package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.intents.Verification;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputVerificationActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_verfication_date) Button btDate;
    @BindView(R.id.laporan_verification_time) Button btTime;

    public static final int REQUEST_CODE = 52;

    private String date = null;
    private int selectedTime = -1;
    private String times[] = null;
    Verification verification = new Verification();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_verification);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_verification));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        // Custom Validation for button Date & Time
        Helpers.addButtonValidation(validation, this, R.id.laporan_verfication_date, ""+btDate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_verification_time, ""+btTime.getText(), R.string.error_required);

        times = getResources().getStringArray(R.array.verification_times);

        Verification data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Verification.class);
        if (data != null && data.getVerificationDate() != null) {
            setData(data);
        }
    }

    private void setData(Verification data) {
        btDate.setText(data.getVerificationDate());

        date = data.getVerificationDate();
        selectedTime = Helpers.getIndexOf(times, data.getVerificationTime());
        btTime.setText(times[selectedTime]);

        btDate.setTextColor(Color.BLACK);
        btTime.setTextColor(Color.BLACK);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            verification.setVerificationDate(""+btDate.getText());
            verification.setVerificationTime(""+btTime.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(verification));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @OnClick(R.id.laporan_verfication_date)
    public void onClickDate(Button button) {
        dialogDatePicker(button);
    }

    @OnClick(R.id.laporan_verification_time)
    public void onClickTime(Button button) {
        showTimeChoiceDialog(button);
    }

    private void dialogDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(date);

        Calendar clMin = Calendar.getInstance();
        clMin.add(Calendar.DATE, 1);

        Calendar clMax = Calendar.getInstance();
        clMax.add(Calendar.MONTH, 6);

        Helpers.dialogDatePicker(this, calendar, clMax, clMin, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                date = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(date);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void showTimeChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(getResources().getStringArray(R.array.verification_times), selectedTime, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(times[which]);
                selectedTime = which;
            }
        });
        builder.show();
    }
}
