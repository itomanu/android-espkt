package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputKonsulActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Konsultasi;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KonsultasiActivity extends BaseActivity {
    @BindView(R.id.lap_box_konsul_details) LinearLayout llMissingDetails;
    @BindView(R.id.lap_data_konsul_title) TextView tvKonsulTitle;
    @BindView(R.id.lap_data_konsul_stuff) TextView tvKonsulStuff;

    private Konsultasi konsultasi = new Konsultasi();
    private boolean[] steps = {false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_konsultasi;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_konsultasi, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_konsultasi_2, APP_NAME);
    }

    @OnClick(R.id.lap_btn_konsul_edit)
    public void onEdit() {
        Intent intent = new Intent(this, InputKonsulActivity.class);
        intent.putExtra("DATA", new Gson().toJson(konsultasi));
        startActivityForResult(intent, InputKonsulActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case InputKonsulActivity.REQUEST_CODE:
                    Konsultasi dKonsultasi = new Gson().fromJson(data.getStringExtra("DATA"), Konsultasi.class);
                    setData(dKonsultasi);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvKonsulTitle);
                break;
            }
        }
    }

    private void setData(Konsultasi data) {
        // Set Global Object Pengaduan for send to API later
        konsultasi.setDesc(data.getDesc());

        // Set View
        llMissingDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvKonsulStuff.setText(konsultasi.getDesc());
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_URAIAN, konsultasi.getDesc());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitKonsultasi(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
