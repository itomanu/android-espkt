package com.baraciptalaksana.espktkedirikota.activities.form.skck;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Kehilangan;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSKCKActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_skck_marital) Button btMarital;
    @BindView(R.id.laporan_skck_passport) AppCompatEditText etPassport;
    @BindView(R.id.laporan_skck_kitas) AppCompatEditText etKitas;

    public static final int REQUEST_CODE = 61;

    SKCK skck = new SKCK();
    AwesomeValidation validation;

    String[] marital;
    private int selectedMarital = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_skck);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_skck));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        Helpers.addButtonValidation(validation, this, R.id.laporan_skck_marital, ""+btMarital.getText(), R.string.error_required);

        marital = getResources().getStringArray(R.array.marital);

        SKCK data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SKCK.class);
        if (data != null && data.getSkckMarital() != null) {
            setData(data);
        }
    }

    private void setData(SKCK data) {
        etPassport.setText(data.getSkckPassport());
        etKitas.setText(data.getSkckKitas());

        selectedMarital = Helpers.getIndexOf(marital, data.getSkckMarital());
        if (selectedMarital > 0) btMarital.setText(marital[selectedMarital]);
        else btMarital.setText(data.getSkckMarital());

        btMarital.setTextColor(Color.BLACK);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_skck_marital)
    public void onMarital(Button button) {
        showMaritalChoiceDialog(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            skck.setSkckPassport(""+etPassport.getText());
            skck.setSkckKitas(""+etKitas.getText());
            skck.setSkckMarital(marital[selectedMarital]);

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(skck));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showMaritalChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(marital, selectedMarital, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(marital[which]);
                selectedMarital = which;
            }
        });
        builder.show();
    }
}
