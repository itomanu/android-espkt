package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Lalin;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputLalinMapsActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    @BindView(R.id.laporan_lalin_maps_location) Button btLocation;
    @BindView(R.id.laporan_lalin_maps_alternative) Button btAlternative;
    @BindView(R.id.laporan_lalin_maps_location_desc) AppCompatEditText etLocation;
    @BindView(R.id.laporan_lalin_maps_alternative_desc) AppCompatEditText etAlternative;

    @BindView(R.id.laporan_data_lalin_maps_location_img) ImageView ivLocation;
    @BindView(R.id.laporan_data_lalin_maps_alternative_img) ImageView ivAlternative;

    public static final int REQUEST_CODE = 53;

    public static final int REQUEST_CODE_LOCATION = 12;
    public static final int REQUEST_CODE_ALTERNATIVE = 13;

    Lalin lalin = new Lalin();
    String[] types, capacities;
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_lalin_maps);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_lalin_maps));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_lalin_maps_location_desc_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addMultilineValidation(validation, this, R.id.laporan_lalin_maps_alternative_desc_layout, Helpers.patternMinLength(3), R.string.error_required);

        types = getResources().getStringArray(R.array.lalin_type);
        capacities = getResources().getStringArray(R.array.lalin_capacities);

        Lalin data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Lalin.class);
        if (data != null && data.getLalinLocationDesc() != null) {
            setData(data);
        }
    }

    private void setData(Lalin data) {
        etLocation.setText(data.getLalinLocationDesc());
        etAlternative.setText(data.getLalinAlternativeDesc());
        if (data.getLalinLocationPath() != null) lalin.setLalinLocationPath(data.getLalinLocationPath());
        if (data.getLalinAlternativePath() != null) lalin.setLalinAlternativePath(data.getLalinAlternativePath());

        setLocationMap();
        setAlternativeMap();
    }

    private void setLocationMap() {
        setImageMap(lalin.getLalinLocationPath(), "red", "ff0000", ivLocation, btLocation,"Ubah Peta Lokasi");
    }

    private void setAlternativeMap() {
        setImageMap(lalin.getLalinAlternativePath(), "green", "00ff00", ivAlternative, btAlternative,"Ubah Peta Jalan Alternatif");
    }

    private void setImageMap(String path, String color, String colorPath, ImageView iv, Button bt, String btText) {
        if (path != null) {
            Picasso.get().load(Helpers.getMAPImageURL(path, color, colorPath)).into(iv);
            iv.setVisibility(View.VISIBLE);

            bt.setText(btText);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_lalin_maps_location)
    public void onClickLocation(Button button) {
        Intent intent = new Intent(this, InputLalinMapsInputActivity.class);
        intent.putExtra("REQUEST_CODE", REQUEST_CODE_LOCATION);
        intent.putExtra("LOCATION_PATH", lalin.getLalinLocationPath());
        intent.putExtra("ALTERNATIVE_PATH", lalin.getLalinAlternativePath());
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    @OnClick(R.id.laporan_lalin_maps_alternative)
    public void onClickAlternative(Button button) {
        Intent intent = new Intent(this, InputLalinMapsInputActivity.class);
        intent.putExtra("REQUEST_CODE", REQUEST_CODE_ALTERNATIVE);
        intent.putExtra("LOCATION_PATH", lalin.getLalinLocationPath());
        intent.putExtra("ALTERNATIVE_PATH", lalin.getLalinAlternativePath());
        startActivityForResult(intent, REQUEST_CODE_ALTERNATIVE);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            lalin.setLalinLocationDesc(""+etLocation.getText());
            lalin.setLalinAlternativeDesc(""+etAlternative.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(lalin));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String path = data.getStringExtra("PATH");

            switch (requestCode) {
                case REQUEST_CODE_LOCATION:
                    lalin.setLalinLocationPath(path);

                    setLocationMap();
                break;
                case REQUEST_CODE_ALTERNATIVE:
                    lalin.setLalinAlternativePath(path);
                    setAlternativeMap();
            }
        }
    }
}
