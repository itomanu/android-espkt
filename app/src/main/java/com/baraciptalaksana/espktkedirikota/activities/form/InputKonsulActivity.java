package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Konsultasi;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputKonsulActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_konsul) AppCompatEditText etKonsul;

    public static final int REQUEST_CODE = 51;

    Konsultasi konsultasi = new Konsultasi();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_konsul);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_konsul));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_konsul_layout, Helpers.patternMinLength(3), R.string.error_required);

        Konsultasi data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Konsultasi.class);
        if (data != null && data.getDesc() != null) {
            setData(data);
        }
    }

    private void setData(Konsultasi data) {
        etKonsul.setText(data.getDesc());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            konsultasi.setDesc(""+etKonsul.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(konsultasi));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
