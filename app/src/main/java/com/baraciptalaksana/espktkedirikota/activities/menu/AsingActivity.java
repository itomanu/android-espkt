package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputAsingActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.InputEventActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Asing;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsingActivity extends BaseActivity {
    @BindView(R.id.lap_box_asing_details) LinearLayout llAsingDetails;
    @BindView(R.id.lap_btn_asing_edit) View vEditAsing;
    @BindView(R.id.lap_data_asing_title) TextView tvAsingTitle;
    @BindView(R.id.lap_data_asing_birth) TextView tvAsingBirth;
    @BindView(R.id.lap_data_asing_nation) TextView tvAsingNation;
    @BindView(R.id.lap_data_asing_job) TextView tvAsingJob;
    @BindView(R.id.lap_data_asing_address) TextView tvAsingAddress;
    @BindView(R.id.lap_data_asing_passport) TextView tvAsingPassport;
    @BindView(R.id.lap_data_asing_passport_by) TextView tvAsingPassportBy;
    @BindView(R.id.lap_data_asing_visa) TextView tvAsingVisa;
    @BindView(R.id.lap_data_asing_arrival_date) TextView tvAsingArrivalDate;
    @BindView(R.id.lap_data_asing_from) TextView tvAsingFrom;
    @BindView(R.id.lap_data_asing_purpose) TextView tvAsingPurpose;
    @BindView(R.id.lap_data_asing_depart_date) TextView tvAsingDepartDate;
    @BindView(R.id.lap_data_asing_next) TextView tvAsingNext;
    @BindView(R.id.lap_data_asing_next_country) TextView tvAsingCountry;
    @BindView(R.id.lap_data_asing_city) TextView tvAsingCity;

    private Asing asing = new Asing();
    private boolean[] steps = {false, false, true, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);

        tvVerificationTitle.setText("KTP");
        rlVerificationDate.setVisibility(View.GONE);
        rlVerificationTime.setVisibility(View.GONE);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_asing;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_asing, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_asing_2);
    }

    @OnClick(R.id.lap_btn_asing_edit)
    public void onEdit() {
        Intent intent = new Intent(this, InputAsingActivity.class);
        intent.putExtra("DATA", new Gson().toJson(asing));
        startActivityForResult(intent, InputAsingActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_ver_edit)
    public void onVerificationEdit() {
        setVerification();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            setFormView(false);
            switch (requestCode) {
                case InputEventActivity.REQUEST_CODE:
                    Asing dAsing = new Gson().fromJson(data.getStringExtra("DATA"), Asing.class);
                    setData(dAsing);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvAsingTitle);
                break;
            }
        }
    }

    private void setData(Asing data) {
        // Set Global Object Pengaduan for send to API later
        asing.setAsingBirthdate(data.getAsingBirthdate());
        asing.setAsingArrivalDate(data.getAsingArrivalDate());
        asing.setAsingDepartDate(data.getAsingDepartDate());
        asing.setAsingName(data.getAsingName());
        asing.setAsingBirthplace(data.getAsingBirthplace());
        asing.setAsingNation(data.getAsingNation());
        asing.setAsingJob(data.getAsingJob());
        asing.setAsingAddress(data.getAsingAddress());
        asing.setAsingPassport(data.getAsingPassport());
        asing.setAsingPassportBy(data.getAsingPassportBy());
        asing.setAsingVisa(data.getAsingVisa());
        asing.setAsingFrom(data.getAsingFrom());
        asing.setAsingPurpose(data.getAsingPurpose());
        asing.setAsingNext(data.getAsingNext());
        asing.setAsingCountry(data.getAsingCountry());
        asing.setAsingCity(data.getAsingCity());

        // Set View
        llAsingDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvAsingTitle.setText(asing.getAsingName());
        tvAsingBirth.setText(asing.getAsingBirthplace() + ", " + asing.getAsingBirthdate());
        tvAsingNation.setText(asing.getAsingNation());
        tvAsingJob.setText(asing.getAsingJob());
        tvAsingAddress.setText(asing.getAsingAddress());
        tvAsingPassport.setText(asing.getAsingPassport());
        tvAsingPassportBy.setText(asing.getAsingPassportBy());
        tvAsingVisa.setText(asing.getAsingVisa());
        tvAsingArrivalDate.setText(asing.getAsingArrivalDate());
        tvAsingFrom.setText(asing.getAsingFrom());
        tvAsingPurpose.setText(asing.getAsingPurpose());
        tvAsingDepartDate.setText(asing.getAsingDepartDate());
        tvAsingNext.setText(asing.getAsingNext());
        tvAsingCountry.setText(asing.getAsingCountry());
        tvAsingCity.setText(asing.getAsingCity());

        setVerification();
    }

    private void setVerification() {
        // Set View
        llVerificationDetails.setVisibility(View.VISIBLE);

        Helpers.dialog(this,
            R.string.dialog_form_id_card_title,
            R.string.dialog_form_id_card_message,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Helpers.scrollToView(scrollView, ivKTP);
                }
            }).show();
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_NAMA_OA, asing.getAsingName());
        builder.addFormDataPart(FORM_TEMPAT_LAHIR_OA, asing.getAsingBirthplace());
        builder.addFormDataPart(FORM_TGL_LAHIR_OA, asing.getAsingBirthdate());
        builder.addFormDataPart(FORM_KEBANGSAAN_OA, asing.getAsingNation());
        builder.addFormDataPart(FORM_PEKERJAAN_OA, asing.getAsingJob());
        builder.addFormDataPart(FORM_ALAMAT_OA, asing.getAsingAddress());
        builder.addFormDataPart(FORM_NO_PASSPORT, asing.getAsingPassport());
        builder.addFormDataPart(FORM_OLEH, asing.getAsingPassportBy());
        builder.addFormDataPart(FORM_VISA, asing.getAsingVisa());
        builder.addFormDataPart(FORM_TGL_KEDATANGAN, Helpers.toServerDate(asing.getAsingArrivalDate()));
        builder.addFormDataPart(FORM_NEGARA, asing.getAsingFrom());
        builder.addFormDataPart(FORM_MAKSUD_DATANG, asing.getAsingPurpose());
        builder.addFormDataPart(FORM_TGL_KEBERANGKATAN, Helpers.toServerDate(asing.getAsingDepartDate()));
        builder.addFormDataPart(FORM_TUJUAN_SELANJUTNYA, asing.getAsingNext());
        builder.addFormDataPart(FORM_NEGARA_SELANJUTNYA, asing.getAsingCountry());
        builder.addFormDataPart(FORM_KOTA_INDONESIA, asing.getAsingCity());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitAsing(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
