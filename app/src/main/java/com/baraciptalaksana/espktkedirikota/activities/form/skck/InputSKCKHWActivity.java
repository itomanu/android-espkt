package com.baraciptalaksana.espktkedirikota.activities.form.skck;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSKCKHWActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_religion) Button btReligion;
    @BindView(R.id.laporan_name_layout) TextInputLayout etNameL;
    @BindView(R.id.laporan_name) AppCompatEditText etName;
    @BindView(R.id.laporan_age) AppCompatEditText etAge;
    @BindView(R.id.laporan_nation) AppCompatEditText etNation;
    @BindView(R.id.laporan_job) AppCompatEditText etJob;
    @BindView(R.id.laporan_job_spnr) Button btJob;
    @BindView(R.id.laporan_address) AppCompatEditText etAddress;

    public static final int REQUEST_CODE = 62;

    SKCK skck = new SKCK();
    AwesomeValidation validation;

    String[] religions, jobs;
    private int selectedReligion = -1, selectedJob = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_skck_fams);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_skck_hw));

        etNameL.setHint("Nama Pasangan");

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_name, Helpers.patternMinLength(3), R.string.error_name);
        validation.addValidation(this, R.id.laporan_age, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_nation, Helpers.patternMinLength(3), R.string.error_required);

        if (Helpers.isEmptyPekerjaan(this)) {
            validation.addValidation(this, R.id.laporan_job, Helpers.patternMinLength(3), R.string.error_required);
        } else {
            btJob.setVisibility(View.VISIBLE);
            etJob.setVisibility(View.GONE);
            Helpers.addButtonValidation(validation, this, R.id.laporan_job_spnr, ""+btJob.getText(), R.string.error_required);
            List<ResponsePekerjaan.Pekerjaan> data = Helpers.getPekerjaan(this).getData();
            jobs = new String[data.size()];
            for (int i = 0; i < jobs.length; i++) {
                jobs[i] = data.get(i).getPekerjaan();
            }
        }

        Helpers.addButtonValidation(validation, this, R.id.laporan_religion, ""+btReligion.getText(), R.string.error_required);
        Helpers.addMultilineValidation(validation, this, R.id.laporan_address_layout, Helpers.patternMinLength(3), R.string.error_required);

        religions = getResources().getStringArray(R.array.religion);

        SKCK data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SKCK.class);
        if (data != null && data.getSkckHWReligion() != null) {
            setData(data);
        }
    }

    private void setData(SKCK data) {
        etName.setText(data.getSkckHWName());
        etAge.setText(data.getSkckHWAge());
        etNation.setText(data.getSkckHWNation());
        etAddress.setText(data.getSkckHWAddress());

        selectedReligion = Helpers.getIndexOf(religions, data.getSkckHWReligion());
        if (selectedReligion > 0) btReligion.setText(religions[selectedReligion]);
        else btReligion.setText(data.getSkckHWReligion());

        btReligion.setTextColor(Color.BLACK);

        if (Helpers.isEmptyPekerjaan(this)) {
            etJob.setText(data.getSkckHWJob());
        } else {
            selectedJob = Helpers.getIndexOf(jobs, data.getSkckHWJob());
            if (selectedJob > 0) btJob.setText(jobs[selectedJob]);
            else btJob.setText(data.getSkckHWJob());
            btJob.setTextColor(Color.BLACK);
        }
    }

    private String getJob() {
        return "" + (Helpers.isEmptyPekerjaan(this) ? etJob.getText() : btJob.getText().toString().equals("Pekerjaan") ? "" : btJob.getText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_religion)
    public void onReligion(Button button) {
        showReligionChoiceDialog(button);
    }

    @OnClick(R.id.laporan_job_spnr)
    public void onClickJob(final Button button) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(jobs, selectedJob, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                button.setTextColor(Color.BLACK);
                button.setText(jobs[which]);
                selectedJob = which;
            }
        });
        builder.show();
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            skck.setSkckHWName(""+etName.getText());
            skck.setSkckHWAge(""+etAge.getText());
            skck.setSkckHWNation(""+etNation.getText());
            skck.setSkckHWJob(getJob());
            skck.setSkckHWAddress(""+etAddress.getText());
            skck.setSkckHWReligion(religions[selectedReligion]);

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(skck));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showReligionChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(religions, selectedReligion, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(religions[which]);
                selectedReligion = which;
            }
        });
        builder.show();
    }
}
