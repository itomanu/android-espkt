package com.baraciptalaksana.espktkedirikota.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BaselineLayout;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.fragments.HomeFragment;
import com.baraciptalaksana.espktkedirikota.fragments.InboxFragment;
import com.baraciptalaksana.espktkedirikota.fragments.PermohonanFragment;
import com.baraciptalaksana.espktkedirikota.fragments.PolresFragment;
import com.baraciptalaksana.espktkedirikota.fragments.ProfilFragment;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseNewsDetail;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.baraciptalaksana.espktkedirikota.utils.Analytics;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.baraciptalaksana.espktkedirikota.utils.Preferences;
import com.baraciptalaksana.espktkedirikota.utils.Updater;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, Constants {
    @BindView(R.id.home_header) RelativeLayout homeHeader;
    @BindView(R.id.home_photo) CircularImageView ivPhoto;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.main_bottom_nav) BottomNavigationView navigation;

    Typeface font;
    int selectedId = 0;

    private HomeFragment homeFragment;
    private PolresFragment polresFragment;
    private PermohonanFragment permohonanFragment;
    private InboxFragment inboxFragment;
    private ProfilFragment profilFragment;

    private Analytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Helpers.fixToolbarPosition(toolbar);

        navigation.setOnNavigationItemSelectedListener(this);
        disableShiftMode(navigation);

        font = Typeface.createFromAsset(getAssets(), "fonts/bariol_regular.otf");

        setToolbar(true);

        initFragment();
        navigation.setSelectedItemId(R.id.navigation_home);

        analyze();
        checkAppUpdate();
        getPekerjaan();
    }

    private void analyze() {
        analytics = new Analytics(this);
        analytics.openMain();
    }

    private void checkAppUpdate() {
        Updater appUpdater = new Updater(this)
            .setTitleOnUpdateAvailable(R.string.dialog_update_title)
            .setContentOnUpdateAvailable(R.string.dialog_update_message)
            .setButtonUpdate(R.string.dialog_update_ok);
        appUpdater.start();
    }

    private void setToolbar(boolean isHome) {
        homeHeader.setVisibility(isHome ? View.VISIBLE : View.GONE);
    }

    private void setLogin() {
        if (Helpers.getLoginData(this) != null) {
            ResponseLogin.Data loginData = Helpers.getLoginData(this);

            if (loginData.getJenisKelamin() == null) {
                Helpers.toastInfo(this, "Lengkapi data Anda");
                Intent intent = new Intent(this, EditProfileActivity.class);
                intent.putExtra("DATA", new Gson().toJson(Helpers.getLoginDataInPerson(this)));
                intent.putExtra("TITLE", "Edit Profile");

                startActivity(intent);
            } else {
                Helpers.setPhotoProfile(loginData.getFoto(), ivPhoto);
            }
        } else {
            Picasso.get().load(R.drawable.no_user_placeholder).into(ivPhoto);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLogin();
    }

    @OnClick(R.id.home_photo)
    public void doLogin() {
        if (Helpers.getLoginData(this) == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra("NO_SKIP", true);
            startActivityForResult(intent, LoginActivity.REQUEST_LOGIN_NO_SKIP);
        } else {
            navigation.setSelectedItemId(R.id.navigation_profil);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        navigation.setSelectedItemId(selectedId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (navigation != null && navigation.getSelectedItemId() == R.id.navigation_home) return false;
        if (Helpers.getLoginData(this) != null) {
            Helpers.fixTitlePosition(title);
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.profile, menu);
            return true;
        } else return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                Helpers.dialog(this, R.string.dialog_logout_title, R.string.dialog_logout_message, R.string.text_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helpers.resetLoginData(MainActivity.this);
                        invalidateOptionsMenu();
                        navigation.setSelectedItemId(R.id.navigation_home);
                        setLogin();
                    }
                }, R.string.text_no, null).show();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (selectedId == R.id.navigation_home) {
            finish();
        } else {
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        invalidateOptionsMenu();
        switch (item.getItemId()) {
            case R.id.navigation_home:
                title.setText(R.string.title_home_fragment);
                setToolbar(true);
                loadFragment(homeFragment);

                if (selectedId == R.id.navigation_home) {
                    homeFragment.scrollToTop();
                }

                homeFragment.getData();

                selectedId = item.getItemId();
                return true;
            case R.id.navigation_polres:
                title.setText(R.string.title_polres_fragment);
                setToolbar(false);
                loadFragment(polresFragment);
                selectedId = item.getItemId();
                return true;
            case R.id.navigation_permohonan:
                if (Helpers.getLoginData(this) != null) {
                    title.setText(R.string.title_permohonan_fragment);
                    setToolbar(false);
                    loadFragment(permohonanFragment);

                    if (selectedId == R.id.navigation_permohonan) {
                        permohonanFragment.scrollToTop();
                    }

                    permohonanFragment.getLoginData();
                    permohonanFragment.getData();
                    selectedId = item.getItemId();
                    return true;
                } else {
                    doLogin();
                    return false;
                }
            case R.id.navigation_inbox:
                title.setText(R.string.title_inbox_fragment);
                setToolbar(false);
                loadFragment(inboxFragment);

                if (selectedId == R.id.navigation_inbox) {
                    inboxFragment.scrollToTop();
                }

                selectedId = item.getItemId();
                return true;
            case R.id.navigation_profil:
                if (Helpers.getLoginData(this) != null) {
                    loadFragment(profilFragment);
                    title.setText(R.string.title_profil_fragment);
                    setToolbar(false);
                    selectedId = item.getItemId();
                    return true;
                } else {
                    doLogin();
                    return false;
                }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case LoginActivity.REQUEST_LOGIN_NO_SKIP:
                    navigation.setSelectedItemId(selectedId);
                    invalidateOptionsMenu();
                    setLogin();
                    break;
                case EditProfileActivity.REQUEST_CODE:
                    setLogin();
                    profilFragment.renewLoginData();
            }
        }
    }

    private void initFragment() {
        homeFragment = new HomeFragment();
        polresFragment = new PolresFragment();
        permohonanFragment = new PermohonanFragment();
        inboxFragment = new InboxFragment();
        profilFragment = new ProfilFragment();
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());

                View itemTitle = item.getChildAt(1);

                TextView smallTitle = (TextView) ((BaselineLayout) itemTitle).getChildAt(0);
                TextView largeTitle = (TextView) ((BaselineLayout) itemTitle).getChildAt(1);

                smallTitle.setTypeface(font);
                smallTitle.setTextSize(10f);
                largeTitle.setTypeface(font);
                largeTitle.setTextSize(11f);
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    private void getPekerjaan() {
        Api.Factory.getInstance().getPekerjaan().enqueue(new Callback<ResponsePekerjaan>() {
            @Override
            public void onResponse(Call<ResponsePekerjaan> call, Response<ResponsePekerjaan> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    List<ResponsePekerjaan.Pekerjaan> data = response.body().getData();
                    if (data != null) {
                        Helpers.setPekerjaan(MainActivity.this, response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponsePekerjaan> call, Throwable t) {

            }
        });
    }
}
