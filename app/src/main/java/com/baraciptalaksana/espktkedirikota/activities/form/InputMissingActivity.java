package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Kehilangan;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputMissingActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_missing) AppCompatEditText btMissing;

    public static final int REQUEST_CODE = 51;

    Kehilangan kehilangan = new Kehilangan();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_missing);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_missing));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_missing_layout, Helpers.patternMinLength(3), R.string.error_required);

        Kehilangan data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Kehilangan.class);
        if (data != null && data.getMissingStuff() != null) {
            setData(data);
        }
    }

    private void setData(Kehilangan data) {
        btMissing.setText(data.getMissingStuff());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            kehilangan.setMissingStuff(""+btMissing.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(kehilangan));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
