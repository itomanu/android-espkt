package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SIM;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSimActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    @BindView(R.id.laporan_sim_kind) Button btKind;
    @BindView(R.id.laporan_sim_type) Button btType;
    @BindView(R.id.laporan_sim_edu) Button btEdu;
    @BindView(R.id.laporan_sim_cert) Button btCert;
    @BindView(R.id.laporan_sim_blood) Button btBlood;
    @BindView(R.id.laporan_sim_height) AppCompatEditText etHeight;
    @BindView(R.id.laporan_sim_glasses) AppCompatEditText etGlasses;
    @BindView(R.id.laporan_sim_disabled) AppCompatEditText etDisabled;
    @BindView(R.id.laporan_sim_prov) AppCompatEditText etProv;
    @BindView(R.id.laporan_sim_nation) AppCompatEditText etNation;
    @BindView(R.id.laporan_sim_country) AppCompatEditText etCountry;

    public static final int REQUEST_CODE = 51;

    private int selectedKind, selectedType, selectedEdu, selectedCert, selectedBlood;

    SIM sim = new SIM();
    String[] kinds, types, edus, certs, bloods;
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_sim);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_sim));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_sim_height, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_sim_prov, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_sim_nation, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_sim_country, Helpers.patternMinLength(1), R.string.error_required);

        Helpers.addButtonValidation(validation, this, R.id.laporan_sim_kind, ""+btKind.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_sim_type, ""+btType.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_sim_edu, ""+btEdu.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_sim_cert, ""+btCert.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_sim_blood, ""+btBlood.getText(), R.string.error_required);

        kinds = getResources().getStringArray(R.array.sim_kind);
        types = getResources().getStringArray(R.array.sim_type);
        edus = getResources().getStringArray(R.array.education);
        certs = getResources().getStringArray(R.array.sim_cert);
        bloods = getResources().getStringArray(R.array.blood_type);

        SIM data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SIM.class);
        if (data != null && data.getSimKind() != null) {
            setData(data);
        } else {
            setAutoField();
        }
    }

    private void setAutoField() {
        etProv.setText(Constants.APP_PROV);
        etCountry.setText(Constants.APP_COUNTRY);
        etNation.setText(Constants.APP_COUNTRY);
    }

    private void setData(SIM data) {
        selectedKind = Helpers.getIndexOf(kinds, data.getSimKind());
        btKind.setTextColor(Color.BLACK);
        if (selectedKind > 0) btKind.setText(kinds[selectedKind]);
        else btKind.setText(data.getSimKind());

        selectedType = Helpers.getIndexOf(types, data.getSimType());
        btType.setTextColor(Color.BLACK);
        if (selectedType > 0) btType.setText(types[selectedType]);
        else btType.setText(data.getSimType());

        selectedEdu = Helpers.getIndexOf(edus, data.getSimEdu());
        btEdu.setTextColor(Color.BLACK);
        if (selectedEdu > 0) btEdu.setText(edus[selectedEdu]);
        else btEdu.setText(data.getSimEdu());

        selectedCert = Helpers.getIndexOf(certs, data.getSimCert());
        btCert.setTextColor(Color.BLACK);
        if (selectedCert > 0) btCert.setText(certs[selectedCert]);
        else btCert.setText(data.getSimCert());

        selectedBlood = Helpers.getIndexOf(bloods, data.getSimBlood());
        btBlood.setTextColor(Color.BLACK);
        if (selectedBlood > 0) btBlood.setText(bloods[selectedBlood]);
        else btBlood.setText(data.getSimBlood());

        etHeight.setText(data.getSimHeight());
        etGlasses.setText(data.getSimGlasses());
        etDisabled.setText(data.getSimDisabled());
        etCountry.setText(data.getSimCountry());
        etNation.setText(data.getSimNational());
        etProv.setText(data.getSimProvince());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_sim_kind)
    public void onClickKind(Button button) {
        showKindChoiceDialog(button);
    }

    @OnClick(R.id.laporan_sim_type)
    public void onClickType(Button button) {
        showTypeChoiceDialog(button);
    }

    @OnClick(R.id.laporan_sim_edu)
    public void onClickEdu(Button button) {
        showEduChoiceDialog(button);
    }

    @OnClick(R.id.laporan_sim_cert)
    public void onClickCert(Button button) {
        showCertChoiceDialog(button);
    }

    @OnClick(R.id.laporan_sim_blood)
    public void onClickBlood(Button button) {
        showBloodChoiceDialog(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            sim.setSimType(types[selectedType]);
            sim.setSimKind(kinds[selectedKind]);
            sim.setSimBlood(bloods[selectedBlood]);
            sim.setSimCert(certs[selectedCert]);
            sim.setSimEdu(edus[selectedEdu]);
            sim.setSimHeight(Helpers.isEmptyString(etHeight) ? "-"
                    : ""+etHeight.getText());
            sim.setSimDisabled(Helpers.isEmptyString(etDisabled) ? "-"
                    : ""+etDisabled.getText());
            sim.setSimGlasses(Helpers.isEmptyString(etGlasses) ? "-"
                    : ""+etGlasses.getText());
            sim.setSimCountry(""+etCountry.getText());
            sim.setSimNational(""+etNation.getText());
            sim.setSimProvince(""+etProv.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(sim));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showKindChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(kinds, selectedKind, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(kinds[which]);
                selectedKind = which;
            }
        });
        builder.show();
    }

    private void showTypeChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(types, selectedType, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(types[which]);
                selectedType = which;
            }
        });
        builder.show();
    }

    private void showEduChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(edus, selectedEdu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(edus[which]);
                selectedEdu = which;
            }
        });
        builder.show();
    }

    private void showCertChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(certs, selectedCert, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(certs[which]);
                selectedCert = which;
            }
        });
        builder.show();
    }

    private void showBloodChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(bloods, selectedBlood, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(bloods[which]);
                selectedBlood = which;
            }
        });
        builder.show();
    }
}
