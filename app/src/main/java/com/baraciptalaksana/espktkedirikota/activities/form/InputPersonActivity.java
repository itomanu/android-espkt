package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputPersonActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_nik) AppCompatEditText tvNIK;
    @BindView(R.id.laporan_name) AppCompatEditText tvName;
    @BindView(R.id.laporan_birth_location) AppCompatEditText tvBirthLocation;
    @BindView(R.id.laporan_birthdate) Button btBirthDate;
    @BindView(R.id.radio_male) AppCompatRadioButton rbMale;
    @BindView(R.id.radio_female) AppCompatRadioButton rbFemale;
    @BindView(R.id.laporan_job) AppCompatEditText tvJob;
    @BindView(R.id.laporan_job_spnr) Button btJob;
    @BindView(R.id.laporan_religion) Button btReligion;
    @BindView(R.id.laporan_address) AppCompatEditText tvAddress;
    @BindView(R.id.laporan_phone) AppCompatEditText tvPhone;
    @BindView(R.id.laporan_email) AppCompatEditText tvEmail;

    public static final int REQUEST_CODE = 50;

    private int selectedReligion = -1, selectedJob = -1;
    private boolean genderMale = true;
    private String birthdate = null;
    Person person = new Person();
    AwesomeValidation validation;

    String[] religions, jobs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_person);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        String mTitle = getIntent().getStringExtra("TITLE");
        if (mTitle == null) title.setText(getString(R.string.title_input_person));
        else title.setText(mTitle);

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_nik_layout, Helpers.patternMinLength(16), R.string.error_nik);
        validation.addValidation(this, R.id.laporan_name_layout, Helpers.patternMinLength(3), R.string.error_name);
        validation.addValidation(this, R.id.laporan_birth_location_layout, Helpers.patternMinLength(3), R.string.error_required);
        validation.addValidation(this, R.id.laporan_phone_layout, Patterns.PHONE, R.string.error_phone);
        validation.addValidation(this, R.id.laporan_email_layout, Patterns.EMAIL_ADDRESS, R.string.error_email);

        if (Helpers.isEmptyPekerjaan(this)) {
            validation.addValidation(this, R.id.laporan_job_layout, Helpers.patternMinLength(3), R.string.error_required);
        } else {
            btJob.setVisibility(View.VISIBLE);
            tvJob.setVisibility(View.GONE);
            Helpers.addButtonValidation(validation, this, R.id.laporan_job_spnr, ""+btJob.getText(), R.string.error_required);
            List<ResponsePekerjaan.Pekerjaan> data = Helpers.getPekerjaan(this).getData();
            jobs = new String[data.size()];
            for (int i = 0; i < jobs.length; i++) {
                jobs[i] = data.get(i).getPekerjaan();
            }
        }

        // Custom Validation for multiline Address & button Birth Date & Religion
        Helpers.addMultilineValidation(validation, this, R.id.laporan_address_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_birthdate, ""+btBirthDate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_religion, ""+btReligion.getText(), R.string.error_required);

        religions = getResources().getStringArray(R.array.religion);

        Person data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Person.class);
        if (data != null && data.getGender() != null) {
            setData(data);
        }
    }

    private void setData(Person data) {
        tvNIK.setText(data.getNik());
        tvName.setText(data.getName());
        tvName.setText(data.getName());
        tvBirthLocation.setText(data.getBirthplace());
        btBirthDate.setText(data.getBirthdate());
        tvAddress.setText(data.getAddress());
        tvPhone.setText(data.getPhone());
        tvEmail.setText(data.getEmail());

        if (data.getGender().equalsIgnoreCase("perempuan")) {
            rbFemale.setChecked(true);
            rbMale.setChecked(false);
            genderMale = false;
        } else {
            rbMale.setChecked(true);
            rbFemale.setChecked(false);
            genderMale = true;
        }

        birthdate = data.getBirthdate();
        selectedReligion = Helpers.getIndexOf(religions, data.getReligion());
        if (selectedReligion > 0) btReligion.setText(religions[selectedReligion]);
        else btReligion.setText(data.getReligion());

        btBirthDate.setTextColor(Color.BLACK);
        btReligion.setTextColor(Color.BLACK);

        if (Helpers.isEmptyPekerjaan(this)) {
            tvJob.setText(data.getJob());
        } else {
            selectedJob = Helpers.getIndexOf(jobs, data.getJob());
            if (selectedJob > 0) btJob.setText(jobs[selectedJob]);
            else btJob.setText(data.getJob());
            btJob.setTextColor(Color.BLACK);
        }
    }

    private String getJob() {
        return "" + (Helpers.isEmptyPekerjaan(this) ? tvJob.getText() : btJob.getText().toString().equals("Pekerjaan") ? "" : btJob.getText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            person.setNik("" + tvNIK.getText());
            person.setName("" + tvName.getText());
            person.setBirthplace("" + tvBirthLocation.getText());
            person.setBirthdate(birthdate);
            person.setGender(genderMale ? "Laki-Laki" : "Perempuan");
            person.setJob(getJob());
            person.setReligion(getResources().getStringArray(R.array.religion)[selectedReligion]);
            person.setAddress("" + tvAddress.getText());
            person.setPhone("" + tvPhone.getText());
            person.setEmail("" + tvEmail.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(person));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @OnClick(R.id.laporan_birthdate)
    public void onClickBirthdate(Button button) {
        dialogDatePicker(button);
    }

    @OnClick(R.id.laporan_religion)
    public void onClickReligion(Button button) {
        showReligionChoiceDialog(button);
    }

    @OnClick({R.id.radio_male, R.id.radio_female})
    public void onClickMale(AppCompatRadioButton radioButton) {

        boolean checked = radioButton.isChecked();
        if (checked) {
            if (radioButton.getId() == R.id.radio_male) {
                genderMale = true;
            } else if (radioButton.getId() == R.id.radio_female) {
                genderMale = false;
            }
        }
    }

    @OnClick(R.id.laporan_job_spnr)
    public void onClickJob(final Button button) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(jobs, selectedJob, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                button.setTextColor(Color.BLACK);
                button.setText(jobs[which]);
                selectedJob = which;
            }
        });
        builder.show();
    }

    private void dialogDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(birthdate);

        Calendar cl = Calendar.getInstance();
        cl.add(Calendar.YEAR, -17);

        Helpers.dialogDatePicker(this, calendar, cl, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                birthdate = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(birthdate);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void showReligionChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(religions, selectedReligion, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(religions[which]);
                selectedReligion = which;
            }
        });
        builder.show();
    }
}
