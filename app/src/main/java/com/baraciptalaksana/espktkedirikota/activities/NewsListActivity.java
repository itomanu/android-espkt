package com.baraciptalaksana.espktkedirikota.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.adapters.NewsListAdapter;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseNews;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsListActivity extends AppCompatActivity implements Constants {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.news_list) RecyclerView rvNews;
    @BindView(R.id.news_list_loading) RotateLoading rlLoading;

    NewsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Helpers.fixToolbarPosition(toolbar);
        title.setText("Berita Humas Polres");

        rvNews.setHasFixedSize(true);
        rvNews.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NewsListAdapter();
        rvNews.setAdapter(adapter);

        rlLoading.setVisibility(View.VISIBLE);
        rvNews.setVisibility(View.GONE);

        rlLoading.start();
        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
            return true;
        }
        return false;
    }

    private void getData() {
        Api.Factory.getInstance().getNews().enqueue(new Callback<ResponseNews>() {
            @Override
            public void onResponse(Call<ResponseNews> call, Response<ResponseNews> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    if (response.body().getData() != null) {
                        adapter.setData(response.body().getData());
                        adapter.notifyDataSetChanged();
                        if (rlLoading != null && adapter.getItemCount() > 0) {
                            rlLoading.setVisibility(View.GONE);
                            rvNews.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    Toasty.error(NewsListActivity.this, "Tidak dapat mengambil data", Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseNews> call, Throwable t) {
                Toasty.error(getApplicationContext(), "Tidak dapat mengambil data, Coba lagi.", Toast.LENGTH_SHORT, true).show();
            }
        });
    }
}
