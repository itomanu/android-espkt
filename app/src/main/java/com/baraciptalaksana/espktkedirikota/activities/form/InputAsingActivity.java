package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Asing;
import com.baraciptalaksana.espktkedirikota.models.Pendapat;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputAsingActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    
    @BindView(R.id.laporan_asing_name) AppCompatEditText etName;
    @BindView(R.id.laporan_asing_birth_location) AppCompatEditText etBirthplace;
    @BindView(R.id.laporan_asing_birthdate) Button btBirthdate;
    @BindView(R.id.laporan_asing_nation) AppCompatEditText etNation;
    @BindView(R.id.laporan_asing_job) AppCompatEditText etJob;
    @BindView(R.id.laporan_asing_address) AppCompatEditText etAddress;
    @BindView(R.id.laporan_asing_passport) AppCompatEditText etPassport;
    @BindView(R.id.laporan_asing_passport_by) AppCompatEditText etPassportBy;
    @BindView(R.id.laporan_asing_visa) AppCompatEditText etVisa;
    @BindView(R.id.laporan_asing_arrival_date) Button btArrivalDate;
    @BindView(R.id.laporan_asing_from) AppCompatEditText etFrom;
    @BindView(R.id.laporan_asing_purpose) AppCompatEditText etPurpose;
    @BindView(R.id.laporan_asing_depart_date) Button btDepartDate;
    @BindView(R.id.laporan_asing_next) AppCompatEditText etNext;
    @BindView(R.id.laporan_asing_next_country) AppCompatEditText etCountry;
    @BindView(R.id.laporan_asing_city) AppCompatEditText etCity;

    public static final int REQUEST_CODE = 51;

    private String bDate = null, dDate = null, aDate = null;

    Asing asing = new Asing();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_asing);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_asing));

        addValidation();

        Asing data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Asing.class);
        if (data != null && data.getAsingBirthdate() != null) {
            setData(data);
        }
    }

    private void addValidation() {
        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_asing_name, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_birth_location, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_nation, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_job, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_passport, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_passport_by, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_visa, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_from, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_purpose, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_next, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_next_country, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_asing_city, Helpers.patternMinLength(1), R.string.error_required);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_asing_address_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_asing_birthdate, ""+btBirthdate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_asing_depart_date, ""+btDepartDate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_asing_arrival_date, ""+btDepartDate.getText(), R.string.error_required);
    }

    private void setData(Asing data) {
        btBirthdate.setText(data.getAsingBirthdate());
        btArrivalDate.setText(data.getAsingArrivalDate());
        btDepartDate.setText(data.getAsingDepartDate());

        btBirthdate.setTextColor(Color.BLACK);
        btArrivalDate.setTextColor(Color.BLACK);
        btDepartDate.setTextColor(Color.BLACK);

        etName.setText(data.getAsingName());
        etBirthplace.setText(data.getAsingBirthplace());
        etNation.setText(data.getAsingNation());
        etJob.setText(data.getAsingJob());
        etPassport.setText(data.getAsingPassport());
        etPassportBy.setText(data.getAsingPassportBy());
        etVisa.setText(data.getAsingVisa());
        etFrom.setText(data.getAsingFrom());
        etPurpose.setText(data.getAsingPurpose());
        etNext.setText(data.getAsingNext());
        etCountry.setText(data.getAsingCountry());
        etCity.setText(data.getAsingCity());
        etAddress.setText(data.getAsingAddress());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_asing_birthdate)
    public void onClickBDate(Button button) {
        dialogBDatePicker(button);
    }

    @OnClick(R.id.laporan_asing_arrival_date)
    public void onClickADate(Button button) {
        dialogADatePicker(button);
    }

    @OnClick(R.id.laporan_asing_depart_date)
    public void onClickDDate(Button button) {
        dialogDDatePicker(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            asing.setAsingBirthdate(""+btBirthdate.getText());
            asing.setAsingArrivalDate(""+btArrivalDate.getText());
            asing.setAsingDepartDate(""+btDepartDate.getText());
            asing.setAsingName(""+etName.getText());
            asing.setAsingBirthplace(""+etBirthplace.getText());
            asing.setAsingNation(""+etNation.getText());
            asing.setAsingJob(""+etJob.getText());
            asing.setAsingPassport(""+etPassport.getText());
            asing.setAsingPassportBy(""+etPassportBy.getText());
            asing.setAsingVisa(""+etVisa.getText());
            asing.setAsingFrom(""+etFrom.getText());
            asing.setAsingPurpose(""+etPurpose.getText());
            asing.setAsingNext(""+etNext.getText());
            asing.setAsingCountry(""+etCountry.getText());
            asing.setAsingCity(""+etCity.getText());
            asing.setAsingAddress(""+etAddress.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(asing));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void dialogBDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(bDate);

        Calendar cl = Calendar.getInstance();
        cl.add(Calendar.YEAR, -17);

        Helpers.dialogDatePicker(this, calendar, cl, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                bDate = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(bDate);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogADatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(aDate);

        Helpers.dialogDatePicker(this, calendar, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                aDate = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(aDate);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogDDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(dDate);

        Helpers.dialogDatePicker(this, calendar, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                dDate = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(dDate);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }
}
