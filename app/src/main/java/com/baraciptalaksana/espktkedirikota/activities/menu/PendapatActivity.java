package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputPendapatActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Pendapat;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendapatActivity extends BaseActivity {
    @BindView(R.id.lap_box_pendapat_details) LinearLayout llPendapatDetails;
    @BindView(R.id.lap_data_pendapat_title) TextView tvPendapatTitle;
    @BindView(R.id.lap_data_pendapat_name) TextView tvPendapatName;
    @BindView(R.id.lap_data_pendapat_phone) TextView tvPendapatPhone;
    @BindView(R.id.lap_data_pendapat_time) TextView tvPendapatTime;
    @BindView(R.id.lap_data_pendapat_type) TextView tvPendapatType;
    @BindView(R.id.lap_data_pendapat_capacity) TextView tvPendapatCapacity;
    @BindView(R.id.lap_data_pendapat_location) TextView tvPendapatLocation;
    @BindView(R.id.lap_data_pendapat_agenda) TextView tvPendapatAgenda;
    @BindView(R.id.lap_data_pendapat_korlap) TextView tvPendapatKorlap;
    @BindView(R.id.lap_data_pendapat_meeting_point) TextView tvPendapatMP;
    @BindView(R.id.lap_data_pendapat_name_p) TextView tvPendapatNameP;
    @BindView(R.id.lap_data_pendapat_props) TextView tvPendapatProps;
    @BindView(R.id.lap_data_pendapat_route) TextView tvPendapatRoute;

    private Pendapat pendapat = new Pendapat();
    private boolean[] steps = {false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_pendapat;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_pendapat, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_pendapat_2, APP_NAME);
    }

    @OnClick(R.id.lap_btn_pendapat_edit)
    public void onEdit() {
        Intent intent = new Intent(this, InputPendapatActivity.class);
        intent.putExtra("DATA", new Gson().toJson(pendapat));
        startActivityForResult(intent, InputPendapatActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case InputPendapatActivity.REQUEST_CODE:
                    Pendapat dPendapat = new Gson().fromJson(data.getStringExtra("DATA"), Pendapat.class);
                    setData(dPendapat);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvPendapatTitle);
                break;
            }
        }
    }

    private void setData(Pendapat data) {
        // Set Global Object Pengaduan for send to API later
        pendapat.setPendapatName(data.getPendapatName());
        pendapat.setPendapatNameP(data.getPendapatNameP());
        pendapat.setPendapatPhone(data.getPendapatPhone());
        pendapat.setPendapatDate(data.getPendapatDate());
        pendapat.setPendapatTimeStart(data.getPendapatTimeStart());
        pendapat.setPendapatTimeEnd(data.getPendapatTimeEnd());
        pendapat.setPendapatType(data.getPendapatType());
        pendapat.setPendapatCapacities(data.getPendapatCapacities());
        pendapat.setPendapatLocation(data.getPendapatLocation());
        pendapat.setPendapatRoute(data.getPendapatRoute());
        pendapat.setPendapatAgenda(data.getPendapatAgenda());
        pendapat.setPendapatProps(data.getPendapatProps());
        pendapat.setPendapatMeetingPoint(data.getPendapatMeetingPoint());
        pendapat.setPendapatKorlap1(data.getPendapatKorlap1());
        pendapat.setPendapatKorlap2(data.getPendapatKorlap2());

        // Set View
        llPendapatDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvPendapatAgenda.setText(pendapat.getPendapatAgenda());
        tvPendapatCapacity.setText(pendapat.getPendapatCapacities());
        if (data.getPendapatKorlap2() == null)
            tvPendapatKorlap.setText(pendapat.getPendapatKorlap1());
        else
            tvPendapatKorlap.setText(pendapat.getPendapatKorlap1() + "\n" + data.getPendapatKorlap2());

        tvPendapatLocation.setText(pendapat.getPendapatLocation());
        tvPendapatMP.setText(pendapat.getPendapatMeetingPoint());
        tvPendapatName.setText(pendapat.getPendapatName());
        tvPendapatNameP.setText(pendapat.getPendapatNameP());
        tvPendapatPhone.setText(pendapat.getPendapatPhone());
        tvPendapatProps.setText(pendapat.getPendapatProps());
        tvPendapatRoute.setText(pendapat.getPendapatRoute());
        tvPendapatType.setText(pendapat.getPendapatType());
        tvPendapatTime.setText(pendapat.getPendapatDate() + ", " + pendapat.getPendapatTimeStart() + " - " + pendapat.getPendapatTimeEnd());
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_NAMA_INSTANSI, pendapat.getPendapatName());
        builder.addFormDataPart(FORM_NAMA_PEN_JAWAB, pendapat.getPendapatNameP());
        builder.addFormDataPart(FORM_TELP_PEN_JAWAB, pendapat.getPendapatPhone());
        builder.addFormDataPart(FORM_KEGIATAN, pendapat.getPendapatType());
        builder.addFormDataPart(FORM_LOKASI, pendapat.getPendapatLocation());
        builder.addFormDataPart(FORM_TGL_KEGIATAN, Helpers.toServerDate(pendapat.getPendapatDate()));
        builder.addFormDataPart(FORM_WAKTU_MULAI, pendapat.getPendapatTimeStart());
        builder.addFormDataPart(FORM_WAKTU_SELESAI, pendapat.getPendapatTimeEnd());
        builder.addFormDataPart(FORM_PESERTA, pendapat.getPendapatCapacities());
        builder.addFormDataPart(FORM_TITIK_KUMPUL, pendapat.getPendapatMeetingPoint());
        builder.addFormDataPart(FORM_KORLAP_1, pendapat.getPendapatKorlap1());
        builder.addFormDataPart(FORM_KORLAP_2, pendapat.getPendapatKorlap2());
        builder.addFormDataPart(FORM_RUTE, pendapat.getPendapatRoute());
        builder.addFormDataPart(FORM_PERAGA, pendapat.getPendapatProps());
        builder.addFormDataPart(FORM_AGENDA, pendapat.getPendapatAgenda());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitPendapat(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
