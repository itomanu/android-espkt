package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SIM;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSimEmerActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    @BindView(R.id.laporan_name) AppCompatEditText tvName;
    @BindView(R.id.laporan_address) AppCompatEditText tvAddress;
    @BindView(R.id.laporan_phone) AppCompatEditText tvPhone;

    public static final int REQUEST_CODE = 53;

    SIM sim = new SIM();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_sim_emer);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_sim_emer));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_name_layout, Helpers.patternMinLength(3), R.string.error_name);
        validation.addValidation(this, R.id.laporan_phone_layout, Patterns.PHONE, R.string.error_phone);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_address_layout, Helpers.patternMinLength(3), R.string.error_required);

        SIM data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SIM.class);
        if (data != null && data.getSimEmerName() != null) {
            setData(data);
        }
    }

    private void setData(SIM data) {
        tvName.setText(data.getSimEmerName());
        tvAddress.setText(data.getSimEmerAddress());
        tvPhone.setText(data.getSimEmerPhone());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            sim.setSimEmerName(""+tvName.getText());
            sim.setSimEmerPhone(""+tvPhone.getText());
            sim.setSimEmerAddress(""+tvAddress.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(sim));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
