package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputKeramaianActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Keramaian;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KeramaianActivity extends BaseActivity {
    @BindView(R.id.lap_box_keramaian_details) LinearLayout llKeramaianDetails;
    @BindView(R.id.lap_data_keramaian_title) TextView tvKeramaianTitle;
    @BindView(R.id.lap_data_keramaian_name) TextView tvKeramaianName;
    @BindView(R.id.lap_data_keramaian_phone) TextView tvKeramaianPhone;
    @BindView(R.id.lap_data_keramaian_duration) TextView tvKeramaianDuration;
    @BindView(R.id.lap_data_keramaian_time) TextView tvKeramaianTime;
    @BindView(R.id.lap_data_keramaian_type) TextView tvKeramaianType;
    @BindView(R.id.lap_data_keramaian_capacity) TextView tvKeramaianCapacity;
    @BindView(R.id.lap_data_keramaian_desc) TextView tvKeramaianDesc;

    private Keramaian keramaian = new Keramaian();
    private boolean[] steps = {false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_keramaian;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_keramaian, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_keramaian_2, APP_NAME);
    }

    @OnClick(R.id.lap_btn_keramaian_edit)
    public void onEdit() {
        Intent intent = new Intent(this, InputKeramaianActivity.class);
        intent.putExtra("DATA", new Gson().toJson(keramaian));
        startActivityForResult(intent, InputKeramaianActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case InputKeramaianActivity.REQUEST_CODE:
                    Keramaian dKeramaian = new Gson().fromJson(data.getStringExtra("DATA"), Keramaian.class);
                    setData(dKeramaian);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvKeramaianTitle);
                break;
            }
        }
    }

    private void setData(Keramaian data) {
        // Set Global Object Pengaduan for send to API later
        keramaian.setKeramaianDate(data.getKeramaianDate());
        keramaian.setKeramaianTime(data.getKeramaianTime());
        keramaian.setKeramaianName(data.getKeramaianName());
        keramaian.setKeramaianPhone(data.getKeramaianPhone());
        keramaian.setKeramaianDuration(data.getKeramaianDuration());
        keramaian.setKeramaianType(data.getKeramaianType());
        keramaian.setKeramaianCapacities(data.getKeramaianCapacities());
        keramaian.setKeramaianDesc(data.getKeramaianDesc());

        // Set View
        llKeramaianDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvKeramaianTime.setText(keramaian.getKeramaianDate() + ", " + keramaian.getKeramaianTime());
        tvKeramaianName.setText(keramaian.getKeramaianName());
        tvKeramaianPhone.setText(keramaian.getKeramaianPhone());
        tvKeramaianDuration.setText(keramaian.getKeramaianDuration() + " jam");
        tvKeramaianType.setText(keramaian.getKeramaianType());
        tvKeramaianCapacity.setText(keramaian.getKeramaianCapacities() + " orang");
        tvKeramaianDesc.setText(keramaian.getKeramaianDesc());
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_NAMA_INSTANSI, keramaian.getKeramaianName());
        builder.addFormDataPart(FORM_TELP_PANITIA, keramaian.getKeramaianPhone());
        builder.addFormDataPart(FORM_TGL_KEGIATAN, Helpers.toServerDate(keramaian.getKeramaianDate()));
        builder.addFormDataPart(FORM_WAKTU_KEGIATAN, keramaian.getKeramaianTime());
        builder.addFormDataPart(FORM_DURASI, keramaian.getKeramaianDuration());
        builder.addFormDataPart(FORM_KEGIATAN, keramaian.getKeramaianType());
        builder.addFormDataPart(FORM_PESERTA, keramaian.getKeramaianCapacities());
        builder.addFormDataPart(FORM_RANGKAIAN, keramaian.getKeramaianDesc());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitKeramaian(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
