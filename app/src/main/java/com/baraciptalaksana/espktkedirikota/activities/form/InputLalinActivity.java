package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Lalin;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputLalinActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    @BindView(R.id.laporan_lalin_type) Button btType;
    @BindView(R.id.laporan_lalin_date) Button btDate;
    @BindView(R.id.laporan_lalin_time) Button btTime;
    @BindView(R.id.laporan_lalin_name) AppCompatEditText etName;
    @BindView(R.id.laporan_lalin_phone) AppCompatEditText etPhone;
    @BindView(R.id.laporan_lalin_duration) AppCompatEditText etDuration;
    @BindView(R.id.laporan_lalin_capacity) AppCompatEditText etCaps;

    public static final int REQUEST_CODE = 51;

    private String date = null, time = null;
    private int selectedType;

    Lalin lalin = new Lalin();
    String[] types;
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_lalin);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_lalin));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_lalin_name, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_lalin_phone, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_lalin_duration, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_lalin_capacity, Helpers.patternMinLength(1), R.string.error_required);

        Helpers.addButtonValidation(validation, this, R.id.laporan_lalin_date, ""+btDate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_lalin_time, ""+btTime.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_lalin_type, ""+btType.getText(), R.string.error_required);

        types = getResources().getStringArray(R.array.lalin_type);

        Lalin data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Lalin.class);
        if (data != null && data.getLalinType() != null) {
            setData(data);
        }
    }

    private void setData(Lalin data) {
        btDate.setText(data.getLalinDate());
        btTime.setText(data.getLalinTime());

        btDate.setTextColor(Color.BLACK);
        btTime.setTextColor(Color.BLACK);

        selectedType = Helpers.getIndexOf(types, data.getLalinType());
        if (selectedType > 0) btType.setText(types[selectedType]);
        else btType.setText(data.getLalinType());
        btType.setTextColor(Color.BLACK);

        etName.setText(data.getLalinName());
        etPhone.setText(data.getLalinPhone());
        etDuration.setText(data.getLalinDuration());
        etCaps.setText(data.getLalinCapacities());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_lalin_type)
    public void onClickType(Button button) {
        showTypeChoiceDialog(button);
    }

    @OnClick(R.id.laporan_lalin_date)
    public void onClickDate(Button button) {
        dialogDatePicker(button);
    }

    @OnClick(R.id.laporan_lalin_time)
    public void onClickTime(Button button) {
        dialogTimePicker(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            lalin.setLalinType(types[selectedType]);
            lalin.setLalinName(""+etName.getText());
            lalin.setLalinPhone(""+etPhone.getText());
            lalin.setLalinCapacities(""+etCaps.getText());
            lalin.setLalinDuration(""+etDuration.getText());
            lalin.setLalinDate(""+btDate.getText());
            lalin.setLalinTime(""+btTime.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(lalin));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showTypeChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(types, selectedType, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(types[which]);
                selectedType = which;
            }
        });
        builder.show();
    }

    private void dialogDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(date);

        Calendar clMin = Calendar.getInstance();
        clMin.add(Calendar.DATE, 9);

        Calendar clMax = Calendar.getInstance();
        clMax.add(Calendar.MONTH, 6);

        Helpers.dialogDatePicker(this, calendar, clMax, clMin, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                date = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(date);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogTimePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(time);

        Helpers.dialogTimePicker(this, calendar, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                time = Helpers.getFormattedTime(hourOfDay, minute, second);

                bt.setText(time);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Timepickerdialog");
    }
}
