package com.baraciptalaksana.espktkedirikota.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.edit_photo) ImageView ivPhoto;
    @BindView(R.id.edit_password) AppCompatEditText tvPassword;
    @BindView(R.id.edit_new_password) AppCompatEditText tvPasswordNew;
    @BindView(R.id.edit_new_password_conf) AppCompatEditText tvPasswordNewConf;
    @BindView(R.id.edit_nik) AppCompatEditText tvNIK;
    @BindView(R.id.edit_name) AppCompatEditText tvName;
    @BindView(R.id.edit_birth_location) AppCompatEditText tvBirthLocation;
    @BindView(R.id.edit_birthdate) Button btBirthDate;
    @BindView(R.id.radio_male) AppCompatRadioButton rbMale;
    @BindView(R.id.radio_female) AppCompatRadioButton rbFemale;
    @BindView(R.id.edit_job) AppCompatEditText tvJob;
    @BindView(R.id.edit_job_spnr) Button btJob;
    @BindView(R.id.edit_religion) Button btReligion;
    @BindView(R.id.edit_address) AppCompatEditText tvAddress;
    @BindView(R.id.edit_phone) AppCompatEditText tvPhone;
    @BindView(R.id.edit_email) AppCompatEditText tvEmail;
    @BindView(R.id.edit_btn_submit) Button btSubmit;

    public static final int REQUEST_CODE = 50;

    private static final int REQUEST_IMAGE_CAPTURE = 20;
    private static final int REQUEST_IMAGE_GALLERY = 30;
    private static final int PERMISSIONS_CAM_REQUEST = 1;
    private static final int PERMISSIONS_RS_REQUEST = 2;
    private static final int PERMISSIONS_WS_REQUEST = 3;
    private String mCurrentPhotoPath;
    private File upload;

    private boolean cameraPermissionGranted = false;
    private boolean readStoragePermissionGranted = false;
    private boolean writeStoragePermissionGranted = false;
    private int selectedReligion = -1, selectedJob = -1;
    private boolean genderMale = true, needChangePassword = false;
    private String birthdate = null;
    Person person = new Person();
    AwesomeValidation validation;

    String[] religions, jobs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        String mTitle = getIntent().getStringExtra("TITLE");
        if (mTitle == null) title.setText(getString(R.string.title_input_person));
        else title.setText(mTitle);

        if (!Helpers.isEmptyPekerjaan(this)) {
            btJob.setVisibility(View.VISIBLE);
            tvJob.setVisibility(View.GONE);

            List<ResponsePekerjaan.Pekerjaan> data = Helpers.getPekerjaan(this).getData();
            jobs = new String[data.size()];
            for (int i = 0; i < jobs.length; i++) {
                jobs[i] = data.get(i).getPekerjaan();
            }
        }

        religions = getResources().getStringArray(R.array.religion);

        checkAllPermission();

        Person data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Person.class);
        if (data != null && data.getEmail() != null) {
            setData(data);
        }
    }

    private void addValidation() {
        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.edit_password, ".{6,}", R.string.error_edit_password);
        validation.addValidation(this, R.id.edit_nik_layout, Helpers.patternMinLength(16), R.string.error_nik);
        validation.addValidation(this, R.id.edit_name_layout, Helpers.patternMinLength(3), R.string.error_name);
        validation.addValidation(this, R.id.edit_birth_location_layout, Helpers.patternMinLength(3), R.string.error_required);
        validation.addValidation(this, R.id.edit_phone_layout, Patterns.PHONE, R.string.error_phone);
        validation.addValidation(this, R.id.edit_email_layout, Patterns.EMAIL_ADDRESS, R.string.error_email);

        if (Helpers.isEmptyPekerjaan(this)) {
            validation.addValidation(this, R.id.edit_job_layout, Helpers.patternMinLength(3), R.string.error_required);
        } else {
            Helpers.addButtonValidation(validation, this, R.id.edit_job_spnr, getString(R.string.text_hint_job), R.string.error_required);
        }

        // Custom Validation for multiline Address & button Birth Date & Religion
        Helpers.addMultilineValidation(validation, this, R.id.edit_address_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addMultilineValidation(validation, this, R.id.laporan_address_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.edit_birthdate, getString(R.string.text_hint_birthdate), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.edit_religion, getString(R.string.text_hint_religion), R.string.error_required);
    }

    private void addNewPasswordValidation() {
        validation.addValidation(this, R.id.edit_new_password, ".{6,}", R.string.error_password);
        validation.addValidation(this, R.id.edit_new_password_conf, R.id.edit_new_password, R.string.error_password_conf);
    }

    private void setData(Person data) {
        if (data.getNik() != null) tvNIK.setText(data.getNik());
        if (data.getName() != null) tvName.setText(data.getName());
        if (data.getEmail() != null) tvEmail.setText(data.getEmail());
        if (data.getBirthplace() != null) tvBirthLocation.setText(data.getBirthplace());
        if (data.getBirthdate() != null) {
            birthdate = data.getBirthdate();
            btBirthDate.setText(data.getBirthdate());
            btBirthDate.setTextColor(Color.BLACK);
        }
        if (data.getAddress() != null) tvAddress.setText(data.getAddress());
        if (data.getPhone() != null) tvPhone.setText(data.getPhone());
        if (data.getEmail() != null) tvEmail.setText(data.getEmail());

        if (!Helpers.isEmptyString(data.getGender())) {
            if (data.getGender().equalsIgnoreCase("P")) {
                rbFemale.setChecked(true);
                rbMale.setChecked(false);
                genderMale = false;
            } else {
                rbMale.setChecked(true);
                rbFemale.setChecked(false);
                genderMale = true;
            }
        }

        if (!Helpers.isEmptyString(data.getReligion())) {
            selectedReligion = Arrays.asList(religions).indexOf(data.getReligion());
            if (selectedReligion > 0) btReligion.setText(religions[selectedReligion]);
            else btReligion.setText(data.getReligion());

            btReligion.setTextColor(Color.BLACK);
        }
        ResponseLogin.Data loginData = Helpers.getLoginData(this);
        Helpers.setPhotoProfile(loginData.getFoto(), ivPhoto);

        if (Helpers.isEmptyPekerjaan(this)) {
            if (data.getJob() != null) tvJob.setText(data.getJob());
        } else if (!Helpers.isEmptyString(data.getJob())) {
            selectedJob = Helpers.getIndexOf(jobs, data.getJob());
            if (selectedJob > 0) btJob.setText(jobs[selectedJob]);
            else btJob.setText(data.getJob());
            btJob.setTextColor(Color.BLACK);
        }

        person = data;
    }

    private String getJob() {
        return "" + (Helpers.isEmptyPekerjaan(this) ? tvJob.getText() : btJob.getText().toString().equals("Pekerjaan") ? "" : btJob.getText());
    }

    private void checkAllPermission() {
        cameraPermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        readStoragePermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        writeStoragePermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestCameraPermission() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            cameraPermissionGranted = true;
        } else {
            cameraPermissionGranted = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_CAM_REQUEST);
        }
    }

    private void requestReadStoragePermission() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            readStoragePermissionGranted = true;
        } else {
            readStoragePermissionGranted = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_RS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        if (requestCode == PERMISSIONS_CAM_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            cameraPermissionGranted = true;
            onOpenCamera();
        }
        if (requestCode == PERMISSIONS_RS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            readStoragePermissionGranted = true;
            if (cameraPermissionGranted) onOpenCamera();
        }
        if (requestCode == PERMISSIONS_WS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            writeStoragePermissionGranted = true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.edit_btn_camera)
    public void onOpenCamera() {
        if (cameraPermissionGranted && readStoragePermissionGranted) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                }

                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this, "com.baraciptalaksana.espktkedirikota.fileprovider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        } else {
            requestCameraPermission();
            requestReadStoragePermission();
        }
    }

    @OnClick(R.id.edit_btn_gallery)
    public void onOpenGallery() {
        if (readStoragePermissionGranted) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
        } else {
            requestReadStoragePermission();
        }
    }

    @OnClick(R.id.edit_cb_new_password)
    public void needChangePassword(CheckBox cb) {
        needChangePassword = cb.isChecked();
        if (needChangePassword) {
            tvPasswordNew.setVisibility(View.VISIBLE);
            tvPasswordNewConf.setVisibility(View.VISIBLE);
        } else {
            tvPasswordNew.setVisibility(View.GONE);
            tvPasswordNewConf.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.edit_job_spnr)
    public void onClickJob(final Button button) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(jobs, selectedJob, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                button.setTextColor(Color.BLACK);
                button.setText(jobs[which]);
                selectedJob = which;
            }
        });
        builder.show();
    }

    @OnClick(R.id.edit_btn_submit)
    public void onSubmit() {
        addValidation();

        if (needChangePassword) addNewPasswordValidation();

        Person nPerson = new Person();
        nPerson.setNik("" + tvNIK.getText());
        nPerson.setName("" + tvName.getText());
        nPerson.setBirthplace("" + tvBirthLocation.getText());
        nPerson.setBirthdate(birthdate);
        nPerson.setGender(genderMale ? "Laki-Laki" : "Perempuan");
        nPerson.setJob(getJob());
        nPerson.setReligion(getResources().getStringArray(R.array.religion)[selectedReligion]);
        nPerson.setAddress("" + tvAddress.getText());
        nPerson.setPhone("" + tvPhone.getText());
        nPerson.setEmail("" + tvEmail.getText());

        if (Helpers.isEmptyString(person.getGender()) || !person.equals(nPerson) || mCurrentPhotoPath != null || needChangePassword) {
            if (validation.validate()) {
                if (!tvPasswordNew.getText().toString().equals(tvPassword.getText().toString()))
                    doUpdateProfile(nPerson);
                else {
                    tvPasswordNew.setError(getString(R.string.error_edit_password_new));
                    tvPasswordNew.requestFocus();
                }
            }
        } else {
            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(person));
            setResult(RESULT_CANCELED, intent);
            finish();
            Toasty.info(this, "Tidak ada data yang berubah").show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE_CAPTURE:
                    setPic();
                    break;
                case REQUEST_IMAGE_GALLERY:
                    mCurrentPhotoPath = getRealPathFromURI(data.getData());
                    Picasso.get().load(data.getData()).into(ivPhoto);
                    break;
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void startLoading() {
        btSubmit.setEnabled(false);
        btSubmit.setText("Loading...");
    }

    private void stopLoading() {
        btSubmit.setEnabled(true);
        btSubmit.setText("Submit");
    }

    private void saveCacheImage(Bitmap mBitmap) {
        try {
            upload = new File(getCacheDir(), "photoprof.jpg");
            upload.createNewFile();

            //Convert bitmap to byte array
            Bitmap bitmap = mBitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] bitmapdata = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(upload);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doUpdateProfile(Person person) {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("id_user", ""+Helpers.getLoginData(this).getIdUser());
        builder.addFormDataPart("no_ktp", person.getNik());
        builder.addFormDataPart("nama", person.getName());
        builder.addFormDataPart("telp", person.getPhone());
        builder.addFormDataPart("email", person.getEmail());
        builder.addFormDataPart("pekerjaan", person.getJob());
        builder.addFormDataPart("tempat_lahir", person.getBirthplace());
        builder.addFormDataPart("tgl_lahir", Helpers.toServerDate(person.getBirthdate()));
        builder.addFormDataPart("agama", person.getReligion());
        builder.addFormDataPart("jenis_kelamin", person.getGender());
        builder.addFormDataPart("alamat", person.getAddress());
        builder.addFormDataPart("password", ""+tvPassword.getText());
        if (needChangePassword)
            builder.addFormDataPart("password_new", ""+tvPasswordNew.getText());

        if (mCurrentPhotoPath != null) {
            saveCacheImage(((BitmapDrawable)ivPhoto.getDrawable()).getBitmap());
            if (upload != null)
                builder.addFormDataPart("foto", upload.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), upload));
        }

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doEditProfile(requestBody).enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                response.body();
                stopLoading();

                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(Constants.SUCCESS)) {
                        ResponseLogin.Data data = response.body().getData();
                        Helpers.setLoginData(EditProfileActivity.this, data);
                        Person nPerson = Person.mappingPerson(data);

                        Intent intent = new Intent();
                        intent.putExtra("DATA", new Gson().toJson(nPerson));
                        Helpers.toastSuccess(EditProfileActivity.this, getString(R.string.text_data_change_success));
                        setResult(RESULT_OK, intent);
                        finish();
                    } else if (!Helpers.isEmptyString(response.body().getMessage())) {
                        Helpers.toastError(EditProfileActivity.this, response.body().getMessage());
                    }
                } else {
                    Toasty.error(EditProfileActivity.this, getString(R.string.dialog_error_unknown_network_message), Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Toasty.error(EditProfileActivity.this, getString(R.string.dialog_error_unknown_network_message), Toast.LENGTH_SHORT, true).show();
                stopLoading();
            }
        });
    }

    @OnClick(R.id.edit_birthdate)
    public void onClickBirthdate(Button button) {
        dialogDatePicker(button);
    }

    @OnClick(R.id.edit_religion)
    public void onClickReligion(Button button) {
        showReligionChoiceDialog(button);
    }

    @OnClick({R.id.radio_male, R.id.radio_female})
    public void onClickMale(AppCompatRadioButton radioButton) {

        boolean checked = radioButton.isChecked();
        if (checked) {
            if (radioButton.getId() == R.id.radio_male) {
                genderMale = true;
            } else if (radioButton.getId() == R.id.radio_female) {
                genderMale = false;
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = ivPhoto.getWidth();
        int targetH = ivPhoto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        ivPhoto.setImageBitmap(bitmap);
    }

    private void dialogDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(birthdate);

        Calendar cl = Calendar.getInstance();
        cl.add(Calendar.YEAR, -17);

        Helpers.dialogDatePicker(this, calendar, cl, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                birthdate = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(birthdate);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void showReligionChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(religions, selectedReligion, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(religions[which]);
                selectedReligion = which;
            }
        });
        builder.show();
    }
}
