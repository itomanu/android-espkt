package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputSimActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.InputSimEmerActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.SIM;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SIMActivity extends BaseActivity {
    @BindView(R.id.lap_box_sim_details) LinearLayout llSimDetails;
    @BindView(R.id.lap_data_sim_title) TextView tvSimTitle;
    @BindView(R.id.lap_data_sim_type) TextView tvSimType;
    @BindView(R.id.lap_data_sim_blood) TextView tvSimBlood;
    @BindView(R.id.lap_data_sim_height) TextView tvSimHeight;
    @BindView(R.id.lap_data_sim_glasses) TextView tvSimGlasses;
    @BindView(R.id.lap_data_sim_certificate) TextView tvSimCert;
    @BindView(R.id.lap_data_sim_disabled) TextView tvSimDisabled;
    @BindView(R.id.lap_data_sim_education) TextView tvSimEdu;
    @BindView(R.id.lap_data_sim_nation) TextView tvSimNation;
    @BindView(R.id.lap_data_sim_country) TextView tvSimCountry;
    @BindView(R.id.lap_data_sim_prov) TextView tvSimProv;

    @BindView(R.id.lap_box_sim_emer_details) LinearLayout llSimEDetails;
    @BindView(R.id.lap_data_sim_emer_title) TextView tvSimETitle;
    @BindView(R.id.lap_data_sim_emer_phone) TextView tvSimEPhone;
    @BindView(R.id.lap_data_sim_emer_address) TextView tvSimEAddress;

    private SIM sim = new SIM();
    private boolean[] steps = {false, false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_sim;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_sim, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return null;
    }

    @OnClick(R.id.lap_btn_sim_edit)
    public void onSimEdit() {
        Intent intent = new Intent(this, InputSimActivity.class);
        intent.putExtra("DATA", new Gson().toJson(sim));
        startActivityForResult(intent, InputSimActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_sim_emer_edit)
    public void onSimEmerEdit() {
        Intent intent = new Intent(this, InputSimEmerActivity.class);
        intent.putExtra("DATA", new Gson().toJson(sim));
        startActivityForResult(intent, InputSimEmerActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            SIM dSim;
            switch (requestCode) {
                case InputSimActivity.REQUEST_CODE:
                    dSim = new Gson().fromJson(data.getStringExtra("DATA"), SIM.class);
                    setDataSim(dSim);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvSimTitle);
                break;
                case InputSimEmerActivity.REQUEST_CODE:
                    dSim = new Gson().fromJson(data.getStringExtra("DATA"), SIM.class);
                    setDataSimEmer(dSim);
                    steps[4] = true;
                    Helpers.scrollToView(scrollView, tvSimETitle);
                break;
            }
        }
    }

    private void setDataSim(SIM data) {
        // Set Global Object Pengaduan for send to API later
        sim.setSimType(data.getSimType());
        sim.setSimKind(data.getSimKind());
        sim.setSimBlood(data.getSimBlood());
        sim.setSimCert(data.getSimCert());
        sim.setSimEdu(data.getSimEdu());
        sim.setSimDisabled(data.getSimDisabled());
        sim.setSimGlasses(data.getSimGlasses());
        sim.setSimHeight(data.getSimHeight());
        sim.setSimCountry(data.getSimCountry());
        sim.setSimNational(data.getSimNational());
        sim.setSimProvince(APP_PROV);

        // Set View
        llSimDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSimTitle.setText(sim.getSimKind());
        tvSimType.setText(sim.getSimType());
        tvSimBlood.setText(sim.getSimBlood());
        tvSimHeight.setText(sim.getSimHeight() + " cm");
        tvSimGlasses.setText(sim.getSimGlasses());
        tvSimDisabled.setText(sim.getSimDisabled());
        tvSimEdu.setText(sim.getSimEdu());
        tvSimCert.setText(sim.getSimCert());
        tvSimNation.setText(sim.getSimNational());
        tvSimCountry.setText(sim.getSimCountry());
        tvSimProv.setText(sim.getSimProvince());
    }

    private void setDataSimEmer(SIM data) {
        // Set Global Object Pengaduan for send to API later
        sim.setSimEmerName(data.getSimEmerName());
        sim.setSimEmerPhone(data.getSimEmerPhone());
        sim.setSimEmerAddress(data.getSimEmerAddress());

        // Set View
        llSimEDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSimETitle.setText(sim.getSimEmerName());
        tvSimEAddress.setText(sim.getSimEmerAddress());
        tvSimEPhone.setText(sim.getSimEmerPhone());
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_JENIS, sim.getSimKind());
        builder.addFormDataPart(FORM_GOL_SIM, sim.getSimType());
        builder.addFormDataPart(FORM_PENDIDIDKAN, sim.getSimEdu());
        builder.addFormDataPart(FORM_SERTIFIKAT, sim.getSimCert());
        builder.addFormDataPart(FORM_TINGGI, sim.getSimHeight());
        builder.addFormDataPart(FORM_GOL_DARAH, sim.getSimBlood());
        builder.addFormDataPart(FORM_KACAMATA, sim.getSimGlasses());
        builder.addFormDataPart(FORM_CACAT, sim.getSimDisabled());
        builder.addFormDataPart(FORM_NAMA_DARURAT, sim.getSimEmerName());
        builder.addFormDataPart(FORM_TELP_DARURAT, sim.getSimEmerPhone());
        builder.addFormDataPart(FORM_ALAMAT_DARURAT, sim.getSimEmerAddress());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitSIM(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
