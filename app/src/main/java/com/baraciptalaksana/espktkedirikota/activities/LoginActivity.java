package com.baraciptalaksana.espktkedirikota.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.RequestLogin;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.baraciptalaksana.espktkedirikota.utils.Preferences;
import com.google.gson.Gson;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.login_email) TextInputEditText inEmail;
    @BindView(R.id.login_password) TextInputEditText inPassword;
    @BindView(R.id.login_btn_skip) View btnSkip;
    @BindView(R.id.loading) RotateLoading rlLoading;

    public static final int REQUEST_LOGIN_NO_SKIP = 10;

    private static final int REQUEST_REGISTER_CODE = 1;
    private static final int REQUEST_FORGOT_PASSWORD_CODE = 2;

    AwesomeValidation validation;
    int type = -1;
    private boolean inProgress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        validation = new AwesomeValidation(ValidationStyle.BASIC);
        validation.addValidation(this, R.id.login_email, Patterns.EMAIL_ADDRESS, R.string.error_email);
        validation.addValidation(this, R.id.login_password, Helpers.patternMinLength(6), R.string.error_password);

        setupToolbar();

        boolean noSkip = getIntent().getBooleanExtra("NO_SKIP", false);
        if (noSkip) {
            btnSkip.setVisibility(View.GONE);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    private void startLoading() {
        inProgress = true;
        rlLoading.setVisibility(View.VISIBLE);
        rlLoading.start();
    }

    private void stopLoading() {
        inProgress = false;
        rlLoading.stop();
        rlLoading.setVisibility(View.GONE);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!inProgress)
            return super.dispatchTouchEvent(ev);
        return true;
    }

    @OnClick(R.id.login_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {

            startLoading();

            RequestLogin request = new RequestLogin(""+inEmail.getText(), ""+inPassword.getText());

            Api.Factory.getInstance().doLogin(request).enqueue(new Callback<ResponseLogin>() {
                @Override
                public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                    stopLoading();

                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus().equals(Constants.SUCCESS)) {
                            ResponseLogin.Data data = response.body().getData();
                            Preferences.getInstance(LoginActivity.this).putString(Preferences.LOGIN_DATA, new Gson().toJson(data));

                            if (type != -1) {
                                if (Helpers.isEmptyString(data.getJenisKelamin()))
                                    startActivity(new Intent(LoginActivity.this, EditProfileActivity.class));
                                else
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            } else {
                                setResult(RESULT_OK);
                            }

                            finish();
                        } else if (!Helpers.isEmptyString(response.body().getMessage())) {
                            Helpers.toastError(LoginActivity.this, "Email dan Password tidak cocok");
                        }
                    } else {
                        Helpers.toastErrorNetwork(LoginActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<ResponseLogin> call, Throwable t) {
                    stopLoading();
                    Helpers.toastErrorNetwork(LoginActivity.this);
                }
            });
        }
    }

    @OnClick(R.id.login_btn_register)
    public void onRegister() {
        startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), REQUEST_REGISTER_CODE);
    }

    @OnClick(R.id.login_btn_forgot_pass)
    public void onForgotPass() {
        startActivityForResult(new Intent(LoginActivity.this, ForgotPasswordActivity.class), REQUEST_FORGOT_PASSWORD_CODE);
    }

    @OnClick(R.id.login_btn_skip)
    public void onSkip() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            inEmail.setText(""+data.getStringExtra("EMAIL"));
        }
    }
}
