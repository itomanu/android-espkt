package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputEventActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Pengaduan;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengaduanActivity extends BaseActivity {
    @BindView(R.id.lap_box_event_details) LinearLayout llEventDetails;
    @BindView(R.id.lap_btn_event_edit) View vEditEvent;
    @BindView(R.id.lap_data_event_title) TextView tvEventTitle;
    @BindView(R.id.lap_data_event_time) TextView tvEventTime;
    @BindView(R.id.lap_data_event_location) TextView tvEventLocation;
    @BindView(R.id.lap_data_event_desc) TextView tvEventDesc;
    @BindView(R.id.lap_data_event_officer_visit) RelativeLayout rlEventOfficerVisit;

    @BindView(R.id.lap_box_details_2) LinearLayout llDetails2;
    @BindView(R.id.lap_data_name_2) TextView tvName2;
    @BindView(R.id.lap_data_birth_2) TextView tvBirth2;
    @BindView(R.id.lap_data_gender_2) TextView tvGender2;
    @BindView(R.id.lap_data_job_2) TextView tvJob2;
    @BindView(R.id.lap_data_religion_2) TextView tvReligion2;
    @BindView(R.id.lap_data_address_2) TextView tvAddress2;
    @BindView(R.id.lap_data_phone_2) TextView tvPhone2;
    @BindView(R.id.lap_data_email_2) TextView tvEmail2;

    private Pengaduan pengaduan = new Pengaduan();
    private boolean[] steps = {false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_pengaduan;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_desc, "Laporan Pengaduan", APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_pengaduan_2, APP_NAME);
    }

    @OnClick(R.id.lap_btn_event_edit)
    public void onEventEdit() {
        Intent intent = new Intent(this, InputEventActivity.class);
        intent.putExtra("DATA", new Gson().toJson(pengaduan));
        startActivityForResult(intent, InputEventActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case InputEventActivity.REQUEST_CODE:
                    Pengaduan dPengaduan = new Gson().fromJson(data.getStringExtra("DATA"), Pengaduan.class);
                    setDataEvent(dPengaduan);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvEventTitle);
                break;
            }
        }
    }

    private void setDataEvent(Pengaduan data) {
        // Set Global Object Pengaduan for send to API later
        pengaduan.setEventDate(data.getEventDate());
        pengaduan.setEventTime(data.getEventTime());
        pengaduan.setEventLocation(data.getEventLocation());
        pengaduan.setEventDesc(data.getEventDesc());
        pengaduan.setEventOfficerVisit(data.getEventOfficerVisit());

        // Set View
        llEventDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvEventTitle.setText(pengaduan.getEventDesc());
        tvEventTime.setText(pengaduan.getEventDate() + ", " + pengaduan.getEventTime());
        tvEventLocation.setText(pengaduan.getEventLocation());
        tvEventDesc.setText(pengaduan.getEventDesc());

        if (pengaduan.getEventOfficerVisit()) rlEventOfficerVisit.setVisibility(View.VISIBLE);
        else rlEventOfficerVisit.setVisibility(View.GONE);

        if (data.getGender2() != null) {
            pengaduan.setName2(data.getName2());
            pengaduan.setBirthplace2(data.getBirthplace2());
            pengaduan.setBirthdate2(data.getBirthdate2());
            pengaduan.setGender2(data.getGender2());
            pengaduan.setJob2(data.getJob2());
            pengaduan.setReligion2(data.getReligion2());
            pengaduan.setAddress2(data.getAddress2());
            pengaduan.setPhone2(data.getPhone2());
            pengaduan.setEmail2(data.getEmail2());

            llDetails2.setVisibility(View.VISIBLE);

            Helpers.setText(tvName2, pengaduan.getName2());
            Helpers.setText(tvBirth2, ", ", pengaduan.getBirthplace2(), pengaduan.getBirthdate2());
            Helpers.setText(tvGender2, pengaduan.getGender2());
            Helpers.setText(tvJob2, pengaduan.getJob2());
            Helpers.setText(tvReligion2, pengaduan.getReligion2());
            Helpers.setText(tvAddress2, pengaduan.getAddress2());
            Helpers.setText(tvPhone2, pengaduan.getPhone2());
            Helpers.setText(tvEmail2, pengaduan.getEmail2());
        } else {
            llDetails2.setVisibility(View.GONE);
        }
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_TEMPAT_KEJADIAN, pengaduan.getEventLocation());
        builder.addFormDataPart(FORM_TGL_KEJADIAN, Helpers.toServerDate(pengaduan.getEventDate()));
        builder.addFormDataPart(FORM_WAKTU_KEJADIAN, Helpers.toServerTime(pengaduan.getEventTime()));
        builder.addFormDataPart(FORM_URAIAN, pengaduan.getEventDesc());
        builder.addFormDataPart(FORM_TINJAU, pengaduan.getEventOfficerVisit() ? "1" : "0");

        builder.addFormDataPart(FORM_NAMA_TERLAPOR, Helpers.saveString(pengaduan.getName2()));
        builder.addFormDataPart(FORM_TELP_TERLAPOR, Helpers.saveString(pengaduan.getPhone2()));
        builder.addFormDataPart(FORM_EMAIL_TERLAPOR, Helpers.saveString(pengaduan.getEmail2()));
        builder.addFormDataPart(FORM_PEKERJAAN_TERLAPOR, Helpers.saveString(pengaduan.getJob2()));
        builder.addFormDataPart(FORM_TEMPAT_LAHIR_TERLAPOR, Helpers.saveString(pengaduan.getBirthplace2()));
        builder.addFormDataPart(FORM_TGL_LAHIR_TERLAPOR, Helpers.saveString(pengaduan.getBirthdate2(),
            Helpers.toServerDate(pengaduan.getBirthdate2())));
        builder.addFormDataPart(FORM_AGAMA_TERLAPOR, Helpers.saveString(pengaduan.getReligion2()));
        builder.addFormDataPart(FORM_JENIS_KELAMIN_TERLAPOR, Helpers.saveString(pengaduan.getGender2(),
            pengaduan.getGender2Fix()));
        builder.addFormDataPart(FORM_ALAMAT_TERLAPOR, Helpers.saveString(pengaduan.getAddress2()));

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitPengaduan(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
