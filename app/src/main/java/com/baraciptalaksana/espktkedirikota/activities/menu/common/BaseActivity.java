package com.baraciptalaksana.espktkedirikota.activities.menu.common;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.LayoutRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.LoginActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.InputPersonActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.InputVerificationActivity;
import com.baraciptalaksana.espktkedirikota.models.FormLaporan;
import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.baraciptalaksana.espktkedirikota.models.intents.Verification;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public abstract class BaseActivity extends AppCompatActivity implements Constants {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.scrollView) protected NestedScrollView scrollView;
    @BindView(R.id.loading_frame) RelativeLayout loadingFrame;
    @BindView(R.id.loading) RotateLoading loading;

    @BindView(R.id.form_desc) LinearLayout llFormDesc;
    @BindView(R.id.form_details) LinearLayout llFormDetails;
    @BindView(R.id.laporan_label) TextView tvFormLabel;
    @BindView(R.id.laporan_label_2) TextView tvFormLabel2;

    @BindView(R.id.lap_btn_login) View vLogin;
    @BindView(R.id.lap_btn_edit) View vEditPerson;
    @BindView(R.id.lap_data_nik) TextView tvNik;
    @BindView(R.id.lap_data_name) TextView tvName;
    @BindView(R.id.lap_box_details) LinearLayout llDetails;
    @BindView(R.id.lap_data_birth) TextView tvBirth;
    @BindView(R.id.lap_data_gender) TextView tvGender;
    @BindView(R.id.lap_data_job) TextView tvJob;
    @BindView(R.id.lap_data_religion) TextView tvReligion;
    @BindView(R.id.lap_data_address) TextView tvAddress;
    @BindView(R.id.lap_data_phone) TextView tvPhone;
    @BindView(R.id.lap_data_email) TextView tvEmail;

    @BindView(R.id.lap_box_ver_details) protected LinearLayout llVerificationDetails;
    @BindView(R.id.lab_box_ver_date) protected View rlVerificationDate;
    @BindView(R.id.lab_box_ver_time) protected View rlVerificationTime;
    @BindView(R.id.lap_data_ver_title) protected TextView tvVerificationTitle;
    @BindView(R.id.lap_btn_ver_edit) View vEditVer;
    @BindView(R.id.lap_data_ver_date) TextView tvVerificationDate;
    @BindView(R.id.lap_data_ver_time) TextView tvVerificationTime;
    @BindView(R.id.lap_data_ver_ktp) protected ImageView ivKTP;

    protected static final int REQUEST_IMAGE_CAPTURE = 20;
    protected static final int REQUEST_IMAGE_GALLERY = 30;

    protected static final int PERMISSIONS_CAM_REQUEST = 1;
    protected static final int PERMISSIONS_RS_REQUEST = 2;
    protected static final int PERMISSIONS_WS_REQUEST = 3;

    protected String mCurrentPhotoPath, idUser = "";

    protected boolean inProgress = false;
    protected boolean cameraPermissionGranted = false;
    protected boolean readStoragePermissionGranted = false;
    protected boolean writeStoragePermissionGranted = false;

    protected FormLaporan formLaporan = new FormLaporan();
    protected Person person = new Person();
    protected Verification verification = new Verification();
    
    protected Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText(getIntent().getStringExtra(KEY_TITLE));

        Helpers.fixToolbarPosition(toolbar);


        Helpers.fixToolbarPosition(toolbar);
        setFormView(true);
        checkAllPermission();

        onCreateBase(savedInstanceState);
    }

    protected abstract void onCreateBase(Bundle savedInstanceState);
    protected abstract @LayoutRes int getLayout();
    protected abstract boolean[] getSteps();
    protected abstract void doSubmitForm();
    protected abstract String getFormLabelText();
    protected abstract String getFormLabel2Text();

    protected void checkAllPermission() {
        cameraPermissionGranted = ContextCompat.checkSelfPermission(
            this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        readStoragePermissionGranted = ContextCompat.checkSelfPermission(
            this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        writeStoragePermissionGranted = ContextCompat.checkSelfPermission(
            this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    protected void requestCameraPermission() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            cameraPermissionGranted = true;
        } else {
            cameraPermissionGranted = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_CAM_REQUEST);
        }
    }

    protected void requestReadStoragePermission() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            readStoragePermissionGranted = true;
        } else {
            readStoragePermissionGranted = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_RS_REQUEST);
        }
    }

    protected void requestWriteStoragePermission() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            writeStoragePermissionGranted = true;
        } else {
            writeStoragePermissionGranted = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_WS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        if (requestCode == PERMISSIONS_CAM_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            cameraPermissionGranted = true;
            onOpenCamera();
        }
        if (requestCode == PERMISSIONS_RS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            readStoragePermissionGranted = true;
            if (cameraPermissionGranted) onOpenCamera();
        }
        if (requestCode == PERMISSIONS_WS_REQUEST && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            writeStoragePermissionGranted = true;
        }
    }

    protected void setFormView(boolean init) {
        tvFormLabel.setText(getFormLabelText());
        if (getFormLabel2Text() != null) tvFormLabel2.setText(getFormLabel2Text());

        if (init) {
            llFormDesc.setVisibility(View.VISIBLE);
            llFormDetails.setVisibility(View.GONE);
        } else {
            scrollView.scrollTo(0,0);
            llFormDesc.setVisibility(View.GONE);
            llFormDetails.setVisibility(View.VISIBLE);
        }
    }

    protected void getLoginData() {
        if (Helpers.getLoginData(this) != null) {
            person = Helpers.getLoginDataInPerson(this);
            idUser = ""+Helpers.getLoginData(this).getIdUser();

            llDetails.setVisibility(View.VISIBLE);
            vLogin.setVisibility(View.GONE);
            getSteps()[0] = true;

            setDataPerson();
        }
    }

    protected void setDataPerson() {
        // Set Global Object Pengaduan for send to API later
        FormLaporan.mappingPerson(formLaporan, person);

        // Set View
        llDetails.setVisibility(View.VISIBLE);
        vLogin.setVisibility(View.GONE);

        // Set Text Value
        tvNik.setText(formLaporan.getNik());
        tvName.setText(formLaporan.getName());
        tvBirth.setText(formLaporan.getBirthplace() + ", " + formLaporan.getBirthdate());
        tvGender.setText(formLaporan.getGender());
        tvJob.setText(formLaporan.getJob());
        tvReligion.setText(formLaporan.getReligion());
        tvAddress.setText(formLaporan.getAddress());
        tvPhone.setText(formLaporan.getPhone());
        tvEmail.setText(formLaporan.getEmail());
    }

    protected void setDataVerification(Verification data) {
        // Set Global Object Pengaduan for send to API later
        formLaporan.setVerificationDate(data.getVerificationDate());
        formLaporan.setVerificationTime(data.getVerificationTime());

        // Set View
        llVerificationDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvVerificationDate.setText(formLaporan.getVerificationDate());
        tvVerificationTime.setText(formLaporan.getVerificationTime());

        Helpers.dialog(this,
                R.string.dialog_form_id_card_title,
                R.string.dialog_form_id_card_message,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helpers.scrollToView(scrollView, ivKTP);
                    }
                }).show();
    }

    protected void showSuccessDialog() {
        Helpers.dialog(this, R.string.dialog_form_success_title,
            R.string.dialog_form_success_message,
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mActivity.finish();
                }
            }).show();
    }

    protected void buildMultipartBody(MultipartBody.Builder builder) {
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart(FORM_ID_USER, idUser);

        if (mCurrentPhotoPath != null) {
            String path = Helpers.resizeAndCompressImage(this, mCurrentPhotoPath, "compress.jpg");
            File upload = new File(path);
            if (upload != null)
                builder.addFormDataPart(FORM_FOTO, upload.getName(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), upload));
        }

        builder.addFormDataPart(FORM_NO_KTP, formLaporan.getNik());
        builder.addFormDataPart(FORM_NAMA, formLaporan.getName());
        builder.addFormDataPart(FORM_TELP, formLaporan.getPhone());
        builder.addFormDataPart(FORM_EMAIL, formLaporan.getEmail());
        builder.addFormDataPart(FORM_PEKERJAAN, formLaporan.getJob());
        builder.addFormDataPart(FORM_TEMPAT_LAHIR, formLaporan.getBirthplace());
        builder.addFormDataPart(FORM_TGL_LAHIR, Helpers.toServerDate(formLaporan.getBirthdate()));
        builder.addFormDataPart(FORM_AGAMA, formLaporan.getReligion());
        builder.addFormDataPart(FORM_JENIS_KELAMIN, formLaporan.getGenderFix());
        builder.addFormDataPart(FORM_ALAMAT, formLaporan.getAddress());

        if (getLayout() != R.layout.activity_menu_asing) {
            builder.addFormDataPart(FORM_TGL_DTG, Helpers.toServerDate(formLaporan.getVerificationDate()));
            builder.addFormDataPart(FORM_WAKTU_DTG, formLaporan.getVerificationTimeFix());
        }
    }

    protected void startLoading() {
        inProgress = true;
        if (loadingFrame != null) {
            scrollView.setEnabled(false);
            loadingFrame.setVisibility(View.VISIBLE);
            loading.start();
        }
    }

    protected void stopLoading() {
        inProgress = false;
        if (loadingFrame != null) {
            loading.stop();
            loadingFrame.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!inProgress)
            return super.dispatchTouchEvent(ev);
        return true;
    }

    @OnClick(R.id.laporan_btn_continue)
    public void onContinue() {
        getLoginData();
        setFormView(false);
    }

    @OnClick(R.id.lap_btn_login)
    public void onLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("NO_SKIP", true);
        startActivityForResult(intent, LoginActivity.REQUEST_LOGIN_NO_SKIP);
    }

    @OnClick(R.id.lap_btn_edit)
    public void onPersonEdit() {
        Intent intent = new Intent(this, InputPersonActivity.class);
        intent.putExtra("DATA", new Gson().toJson(person));
        startActivityForResult(intent, InputPersonActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_ver_edit)
    public void onVerificationEdit() {
        Intent intent = new Intent(this, InputVerificationActivity.class);
        intent.putExtra("DATA", new Gson().toJson(verification));
        startActivityForResult(intent, InputVerificationActivity.REQUEST_CODE);
    }

    @OnClick(R.id.laporan_btn_camera)
    public void onOpenCamera() {
        if (cameraPermissionGranted && readStoragePermissionGranted) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) { }

                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this, "com.baraciptalaksana.espktkedirikota.fileprovider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        } else {
            requestCameraPermission();
            requestReadStoragePermission();
        }
    }

    @OnClick(R.id.laporan_btn_gallery)
    public void onOpenGallery() {
        if (readStoragePermissionGranted) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
        } else {
            requestWriteStoragePermission();
        }
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (Helpers.arrayContains(getSteps(), false)) {
            Helpers.dialog(this, R.string.dialog_form_unfinished_title,
                    R.string.dialog_form_unfinished_message).show();
        } else {
            startLoading();
            doSubmitForm();
        }
    }

    protected String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case LoginActivity.REQUEST_LOGIN_NO_SKIP:
                    getLoginData();
                    break;
                case REQUEST_IMAGE_CAPTURE:
                    setPic();
                    getSteps()[3] = true;
                    Helpers.scrollToView(scrollView, ivKTP);
                    break;
                case REQUEST_IMAGE_GALLERY:
                    mCurrentPhotoPath = getRealPathFromURI(data.getData());
                    Picasso.get().load(data.getData()).into(ivKTP);
                    getSteps()[3] = true;
                    Helpers.scrollToView(scrollView, ivKTP);
                    break;
                case InputPersonActivity.REQUEST_CODE:
                    person = new Gson().fromJson(data.getStringExtra("DATA"), Person.class);
                    setDataPerson();
                    getSteps()[0] = true;
                    break;
                case InputVerificationActivity.REQUEST_CODE:
                    verification = new Gson().fromJson(data.getStringExtra("DATA"), Verification.class);
                    setDataVerification(verification);
                    getSteps()[2] = true;
            }
        }
    }

    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    protected void setPic() {
        // Get the dimensions of the View
        int targetW = ivKTP.getWidth();
        int targetH = ivKTP.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        ivKTP.setImageBitmap(bitmap);
    }

    protected void onBack() {
        if (Helpers.arrayContains(getSteps(), true)) {
            Helpers.dialog(this, R.string.dialog_form_unsaved_title,
                    R.string.dialog_form_unsaved_message,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mActivity.finish();
                        }
                    }, null).show();

        } else {
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        onBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBack();
                return true;
        }
        return false;
    }
}
