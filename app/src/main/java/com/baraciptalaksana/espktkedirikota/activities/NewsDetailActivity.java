package com.baraciptalaksana.espktkedirikota.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.News;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseNewsDetail;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsDetailActivity extends AppCompatActivity implements Constants {
    @BindView(R.id.news_scrollview) NestedScrollView scrollView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.loading_frame) RelativeLayout loadingFrame;
    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.news_photo) ImageView ivPhoto;
    @BindView(R.id.news_title) TextView tvTitle;
    @BindView(R.id.news_date) TextView tvDate;
    @BindView(R.id.news_body) TextView tvBody;

    int[] location = {0,0};
    private boolean inProgress = false;
    News news;
    ResponseNewsDetail.Detail data;
    Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Helpers.fixToolbarPosition(toolbar);

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (location[1] == 0) {
                    tvDate.getLocationOnScreen(location);
                }

                boolean showed = Helpers.dpToPx(getApplicationContext(),scrollY) > location[1];

                if (showed) {
                    title.setVisibility(View.VISIBLE);
                } else {
                    title.setVisibility(View.GONE);
                }
            }
        });

        news = new Gson().fromJson(getIntent().getStringExtra("NEWS"), News.class);

        getData();
    }

    protected void startLoading() {
        inProgress = true;
        if (loadingFrame != null) {
            scrollView.setVisibility(View.GONE);
            loadingFrame.setVisibility(View.VISIBLE);
            loading.start();
        }
    }

    protected void stopLoading() {
        inProgress = false;
        if (loadingFrame != null) {
            loading.stop();
            loadingFrame.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!inProgress)
            return super.dispatchTouchEvent(ev);
        return true;
    }

    private void setData() {
        tvTitle.setText(Helpers.toTitleCase(news.getJudul()));
        title.setText(Helpers.toTitleCase(news.getJudul()));

        String body = Helpers.toCapitalize(data.getKonten());

        tvBody.setText(body);
        tvDate.setText(Helpers.getNewsDate(news.getCreatedAt()));
        Picasso.get().load(Helpers.getNewsImgUrlFoto(news.getFoto())).into(ivPhoto);
    }

    private void getData() {
        startLoading();
        Api.Factory.getInstance().getDetailNews(""+news.getId()).enqueue(new Callback<ResponseNewsDetail>() {
            @Override
            public void onResponse(Call<ResponseNewsDetail> call, Response<ResponseNewsDetail> response) {
                stopLoading();

                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    data = response.body().getData();
                    if (data != null) {
                        setData();
                    } else {
                        Helpers.toastErrorNetwork(mActivity);
                    }
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                }
            }

            @Override
            public void onFailure(Call<ResponseNewsDetail> call, Throwable t) {
                stopLoading();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }
}
