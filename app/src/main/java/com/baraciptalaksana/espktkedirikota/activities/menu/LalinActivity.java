package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputLalinActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.InputLalinMapsActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Lalin;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LalinActivity extends BaseActivity {
    @BindView(R.id.lap_box_lalin_details) LinearLayout llLalinDetails;
    @BindView(R.id.lap_data_lalin_title) TextView tvLalinTitle;
    @BindView(R.id.lap_data_lalin_name) TextView tvLalinName;
    @BindView(R.id.lap_data_lalin_phone) TextView tvLalinPhone;
    @BindView(R.id.lap_data_lalin_duration) TextView tvLalinDuration;
    @BindView(R.id.lap_data_lalin_time) TextView tvLalinTime;
    @BindView(R.id.lap_data_lalin_type) TextView tvLalinType;
    @BindView(R.id.lap_data_lalin_capacity) TextView tvLalinCapacity;

    @BindView(R.id.lap_box_lalin_maps_details) LinearLayout llLalinMapsDetails;
    @BindView(R.id.lap_data_lalin_maps_title) TextView tvLalinMapsTitle;
    @BindView(R.id.lap_data_lalin_maps_location) TextView tvLalinMapsLocation;
    @BindView(R.id.lap_data_lalin_maps_alternatif) TextView tvLalinMapsAlternatif;
    @BindView(R.id.lap_data_lalin_maps_location_img) ImageView ivLalinMapsLocation;
    @BindView(R.id.lap_data_lalin_maps_alternatif_img) ImageView ivLalinMapsAlternative;

    private Lalin lalin = new Lalin();
    private boolean[] steps = {false, false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_lalin;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_lalin, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return null;
    }

    @OnClick(R.id.lap_btn_lalin_edit)
    public void onLalinEdit() {
        Intent intent = new Intent(this, InputLalinActivity.class);
        intent.putExtra("DATA", new Gson().toJson(lalin));
        startActivityForResult(intent, InputLalinActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_lalin_maps_edit)
    public void onLalinMapsEdit() {
        Intent intent = new Intent(this, InputLalinMapsActivity.class);
        intent.putExtra("DATA", new Gson().toJson(lalin));
        startActivityForResult(intent, InputLalinMapsActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Lalin dLalin;
            switch (requestCode) {
                case InputLalinActivity.REQUEST_CODE:
                    dLalin = new Gson().fromJson(data.getStringExtra("DATA"), Lalin.class);
                    setDataLalin(dLalin);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvLalinTitle);
                break;
                case InputLalinMapsActivity.REQUEST_CODE:
                    dLalin = new Gson().fromJson(data.getStringExtra("DATA"), Lalin.class);
                    setDataLalinMaps(dLalin);
                    steps[4] = true;
                    Helpers.scrollToView(scrollView, tvLalinMapsTitle);
                break;
            }
        }
    }

    private void setDataLalin(Lalin data) {
        // Set Global Object Pengaduan for send to API later
        lalin.setLalinDate(data.getLalinDate());
        lalin.setLalinTime(data.getLalinTime());
        lalin.setLalinName(data.getLalinName());
        lalin.setLalinPhone(data.getLalinPhone());
        lalin.setLalinDuration(data.getLalinDuration());
        lalin.setLalinType(data.getLalinType());
        lalin.setLalinCapacities(data.getLalinCapacities());

        // Set View
        llLalinDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvLalinTime.setText(lalin.getLalinDate() + ", " + lalin.getLalinTime());
        tvLalinName.setText(lalin.getLalinName());
        tvLalinPhone.setText(lalin.getLalinPhone());
        tvLalinDuration.setText(lalin.getLalinDuration() + " jam");
        tvLalinType.setText(lalin.getLalinType());
        tvLalinCapacity.setText(lalin.getLalinCapacities() + " orang");
    }

    private void setDataLalinMaps(Lalin data) {
        // Set Global Object Pengaduan for send to API later
        lalin.setLalinLocationDesc(data.getLalinLocationDesc());
        lalin.setLalinAlternativeDesc(data.getLalinAlternativeDesc());
        lalin.setLalinLocationPath(data.getLalinLocationPath());
        lalin.setLalinAlternativePath(data.getLalinAlternativePath());

        // Set View
        llLalinMapsDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvLalinMapsLocation.setText(lalin.getLalinLocationDesc());
        tvLalinMapsAlternatif.setText(lalin.getLalinAlternativeDesc());

        if (!Helpers.isEmptyString(lalin.getLalinLocationPath())) {
            ivLalinMapsLocation.setVisibility(View.VISIBLE);
            Picasso.get().load(Helpers.getMAPImageURL(lalin.getLalinLocationPath(), "red", "ff0000")).into(ivLalinMapsLocation);
        } else ivLalinMapsLocation.setVisibility(View.GONE);

        if (!Helpers.isEmptyString(lalin.getLalinAlternativePath())) {
            ivLalinMapsAlternative.setVisibility(View.VISIBLE);
            Picasso.get().load(Helpers.getMAPImageURL(lalin.getLalinAlternativePath(), "green", "00ff00")).into(ivLalinMapsAlternative);
        } else ivLalinMapsAlternative.setVisibility(View.GONE);
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_NAMA_INSTANSI, lalin.getLalinName());
        builder.addFormDataPart(FORM_TELP_PANITIA, lalin.getLalinPhone());
        builder.addFormDataPart(FORM_TGL_TUTUP, Helpers.toServerDate(lalin.getLalinDate()));
        builder.addFormDataPart(FORM_WAKTU_TUTUP, lalin.getLalinTime());
        builder.addFormDataPart(FORM_DURASI, lalin.getLalinDuration());
        builder.addFormDataPart(FORM_KEGIATAN, lalin.getLalinType());
        builder.addFormDataPart(FORM_PESERTA, lalin.getLalinCapacities());
        builder.addFormDataPart(FORM_JALAN_TUTUP, lalin.getLalinLocationDesc());
        builder.addFormDataPart(FORM_JALAN_TUTUP_PATH, lalin.getLalinLocationPath());
        builder.addFormDataPart(FORM_JALAN_ALTERNATIF, lalin.getLalinAlternativeDesc());
        builder.addFormDataPart(FORM_JALAN_ALTERNATIF_PATH, lalin.getLalinAlternativePath());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitLalin(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
