package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Keramaian;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputKeramaianActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    @BindView(R.id.laporan_keramaian_date) Button btDate;
    @BindView(R.id.laporan_keramaian_time) Button btTime;
    @BindView(R.id.laporan_keramaian_name) AppCompatEditText etName;
    @BindView(R.id.laporan_keramaian_phone) AppCompatEditText etPhone;
    @BindView(R.id.laporan_keramaian_duration) AppCompatEditText etDuration;
    @BindView(R.id.laporan_keramaian_type) AppCompatEditText etType;
    @BindView(R.id.laporan_keramaian_capacity) AppCompatEditText etCaps;
    @BindView(R.id.laporan_keramaian_desc) AppCompatEditText etDesc;

    public static final int REQUEST_CODE = 51;

    private String date = null, time = null;

    Keramaian keramaian = new Keramaian();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_keramaian);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_keramaian));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_keramaian_name, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_keramaian_phone, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_keramaian_duration, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_keramaian_capacity, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_keramaian_type, Helpers.patternMinLength(1), R.string.error_required);


        Helpers.addMultilineValidation(validation, this, R.id.laporan_keramaian_desc_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_keramaian_date, ""+btDate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_keramaian_time, ""+btTime.getText(), R.string.error_required);

        Keramaian data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Keramaian.class);
        if (data != null && data.getKeramaianType() != null) {
            setData(data);
        }
    }

    private void setData(Keramaian data) {
        btDate.setText(data.getKeramaianDate());
        btTime.setText(data.getKeramaianTime());

        btDate.setTextColor(Color.BLACK);
        btTime.setTextColor(Color.BLACK);

        etName.setText(data.getKeramaianName());
        etType.setText(data.getKeramaianType());
        etPhone.setText(data.getKeramaianPhone());
        etDuration.setText(data.getKeramaianDuration());
        etCaps.setText(data.getKeramaianCapacities());
        etDesc.setText(data.getKeramaianDesc());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_keramaian_date)
    public void onClickDate(Button button) {
        dialogDatePicker(button);
    }

    @OnClick(R.id.laporan_keramaian_time)
    public void onClickTime(Button button) {
        dialogTimePicker(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            keramaian.setKeramaianType(""+etType.getText());
            keramaian.setKeramaianName(""+etName.getText());
            keramaian.setKeramaianPhone(""+etPhone.getText());
            keramaian.setKeramaianCapacities(""+etCaps.getText());
            keramaian.setKeramaianDesc(""+etDesc.getText());
            keramaian.setKeramaianDuration(""+etDuration.getText());
            keramaian.setKeramaianDate(""+btDate.getText());
            keramaian.setKeramaianTime(""+btTime.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(keramaian));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void dialogDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(date);

        Calendar clMin = Calendar.getInstance();
        clMin.add(Calendar.DATE, 9);

        Calendar clMax = Calendar.getInstance();
        clMax.add(Calendar.MONTH, 6);

        Helpers.dialogDatePicker(this, calendar, clMax, clMin, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                date = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(date);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogTimePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(time);

        Helpers.dialogTimePicker(this, calendar, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                time = Helpers.getFormattedTime(hourOfDay, minute, second);

                bt.setText(time);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Timepickerdialog");
    }
}
