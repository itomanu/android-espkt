package com.baraciptalaksana.espktkedirikota.activities.form.skck;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSKCKPidanaActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_skck_pidana) Button btPidana;
    @BindView(R.id.laporan_skck_pidana_desc) AppCompatEditText etDesc;
    @BindView(R.id.laporan_skck_pidana_putusan) AppCompatEditText etPutusan;
    @BindView(R.id.laporan_skck_pidana_desc_on) AppCompatEditText etDescOn;
    @BindView(R.id.laporan_skck_pidana_process) AppCompatEditText etProcess;

    public static final int REQUEST_CODE = 66;

    SKCK skck = new SKCK();
    AwesomeValidation validation;

    String[] pidana;
    private int selectedPidana = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_skck_pidana);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_skck_pidana));

        pidana = getResources().getStringArray(R.array.pidana);

        SKCK data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SKCK.class);
        if (data != null && data.getSkckPidana() != null) {
            setData(data);
        }
    }

    private void setValidation() {
        validation = new AwesomeValidation(ValidationStyle.BASIC);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_pidana_desc_on_layout, Helpers.patternMinLength(1), R.string.error_required);
        Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_pidana_process_layout, Helpers.patternMinLength(1), R.string.error_required);

        if (selectedPidana == Helpers.getIndexOf(pidana, "Pernah")) {
            Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_pidana_desc_layout, Helpers.patternMinLength(1), R.string.error_required);
            Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_pidana_putusan_layout, Helpers.patternMinLength(1), R.string.error_required);
        } else if (selectedPidana == Helpers.getIndexOf(pidana, "Tidak Pernah")) {
            etDesc.setError(null);
            etDesc.setText("-");
            etDesc.setEnabled(false);

            etPutusan.setError(null);
            etPutusan.setText("-");
            etPutusan.setEnabled(false);
        } else {
            Helpers.addButtonValidation(validation, this, R.id.laporan_skck_pidana, "" + btPidana.getText(), R.string.error_required);
        }
    }

    private void setData(SKCK data) {
        etDesc.setText(data.getSkckPidanaDesc());
        etPutusan.setText(data.getSkckPidanaPutusan());
        etDescOn.setText(data.getSkckPidanaDescOn());
        etProcess.setText(data.getSkckPidanaProccess());

        selectedPidana = Helpers.getIndexOf(pidana, data.getSkckPidana());
        if (selectedPidana > 0) btPidana.setText(pidana[selectedPidana]);
        else btPidana.setText(data.getSkckPidana());

        btPidana.setTextColor(Color.BLACK);

        checkEditText();
    }

    private void checkEditText() {
        if (selectedPidana == Helpers.getIndexOf(pidana, "Pernah")) {
            etDesc.setEnabled(true);
            etPutusan.setEnabled(true);
        } else if (selectedPidana == Helpers.getIndexOf(pidana, "Tidak Pernah")) {
            etDesc.setError(null);
            etDesc.setText("-");
            etDesc.setEnabled(false);

            etPutusan.setError(null);
            etPutusan.setText("-");
            etPutusan.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_skck_pidana)
    public void onPidana(Button button) {
        showPidanaChoiceDialog(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        setValidation();

        if (validation.validate()) {
            skck.setSkckPidanaDesc(""+etDesc.getText());
            skck.setSkckPidanaDescOn(""+etDescOn.getText());
            skck.setSkckPidanaProccess(""+etProcess.getText());
            skck.setSkckPidanaPutusan(""+etPutusan.getText());
            skck.setSkckPidana(pidana[selectedPidana]);

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(skck));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showPidanaChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(pidana, selectedPidana, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setError(null);
                bt.setTextColor(Color.BLACK);
                bt.setText(pidana[which]);
                selectedPidana = which;

                checkEditText();
            }
        });
        builder.show();
    }
}
