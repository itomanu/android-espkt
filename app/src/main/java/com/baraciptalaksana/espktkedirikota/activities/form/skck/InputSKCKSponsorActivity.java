package com.baraciptalaksana.espktkedirikota.activities.form.skck;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSKCKSponsorActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_skck_sponsor_name) AppCompatEditText etName;
    @BindView(R.id.laporan_skck_sponsor_address) AppCompatEditText etAddress;
    @BindView(R.id.laporan_skck_sponsor_type) AppCompatEditText etType;
    @BindView(R.id.laporan_skck_sponsor_phone) AppCompatEditText etPhone;

    public static final int REQUEST_CODE = 69;

    SKCK skck = new SKCK();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_skck_sponsor);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_skck_sponsor));

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        SKCK data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SKCK.class);
        if (data != null && data.getSkckOthersJob() != null) {
            setData(data);
        }
    }

    private void setValidation() {
        validation = new AwesomeValidation(ValidationStyle.BASIC);
        
        Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_sponsor_address_layout, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_skck_sponsor_phone, Patterns.PHONE, R.string.error_phone);
        validation.addValidation(this, R.id.laporan_skck_sponsor_name, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_skck_sponsor_type, Helpers.patternMinLength(1), R.string.error_required);
    }

    private void setData(SKCK data) {
        etName.setText(data.getSkckSponsorName());
        etAddress.setText(data.getSkckSponsorAddress());
        etType.setText(data.getSkckSponsorType());
        etPhone.setText(data.getSkckSponsorPhone());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            skck.setSkckSponsorName(""+etName.getText());
            skck.setSkckSponsorAddress(""+etAddress.getText());
            skck.setSkckSponsorType(""+etType.getText());
            skck.setSkckSponsorPhone(""+etPhone.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(skck));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
