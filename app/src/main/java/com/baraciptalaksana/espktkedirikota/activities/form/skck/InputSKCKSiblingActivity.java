package com.baraciptalaksana.espktkedirikota.activities.form.skck;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSKCKSiblingActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_name_layout) TextInputLayout etNameL;
    @BindView(R.id.laporan_name) AppCompatEditText etName;
    @BindView(R.id.laporan_age) AppCompatEditText etAge;
    @BindView(R.id.laporan_job) AppCompatEditText etJob;
    @BindView(R.id.laporan_job_spnr) Button btJob;
    @BindView(R.id.laporan_address) AppCompatEditText etAddress;

    public static final int REQUEST_CODE = 65;
    public static final int TYPE_ADD = 0;
    public static final int TYPE_EDIT = 1;

    SKCK skck = new SKCK();
    AwesomeValidation validation;
    int index = 0, type = TYPE_ADD, selectedJob = -1;

    String[] jobs;

    List<String> lName, lAge, lJob, lAddress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_skck_sibling);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_skck_sibling));

        etNameL.setHint("Nama Saudara");

        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_name, Helpers.patternMinLength(3), R.string.error_name);
        validation.addValidation(this, R.id.laporan_age, Helpers.patternMinLength(1), R.string.error_required);

        if (Helpers.isEmptyPekerjaan(this)) {
            validation.addValidation(this, R.id.laporan_job, Helpers.patternMinLength(3), R.string.error_required);
        } else {
            btJob.setVisibility(View.VISIBLE);
            etJob.setVisibility(View.GONE);
            Helpers.addButtonValidation(validation, this, R.id.laporan_job_spnr, ""+btJob.getText(), R.string.error_required);
            List<ResponsePekerjaan.Pekerjaan> data = Helpers.getPekerjaan(this).getData();
            jobs = new String[data.size()];
            for (int i = 0; i < jobs.length; i++) {
                jobs[i] = data.get(i).getPekerjaan();
            }
        }

        Helpers.addMultilineValidation(validation, this, R.id.laporan_address_layout, Helpers.patternMinLength(3), R.string.error_required);

        index = getIntent().getIntExtra("INDEX", 0);
        type = getIntent().getIntExtra("TYPE", TYPE_ADD);

        lName = new ArrayList<>();
        lAge = new ArrayList<>();
        lJob = new ArrayList<>();
        lAddress = new ArrayList<>();

        SKCK data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SKCK.class);
        if (data != null && data.getSkckSiblingName() != null) {
            setData(data);
        }
    }

    private void setData(SKCK data) {
        lName = new ArrayList<>(Arrays.asList(data.getSkckSiblingName().split(Constants.SIBLING_SEPARATOR)));
        lAge = new ArrayList<>(Arrays.asList(data.getSkckSiblingAge().split(Constants.SIBLING_SEPARATOR)));
        lJob = new ArrayList<>(Arrays.asList(data.getSkckSiblingJob().split(Constants.SIBLING_SEPARATOR)));
        lAddress = new ArrayList<>(Arrays.asList(data.getSkckSiblingAddress().split(Constants.SIBLING_SEPARATOR)));

        if (type == TYPE_EDIT) {
            etName.setText(lName.get(index));
            etAge.setText(lAge.get(index));
            etAddress.setText(lAddress.get(index));

            if (Helpers.isEmptyPekerjaan(this)) {
                etJob.setText(lJob.get(index));
            } else {
                selectedJob = Helpers.getIndexOf(jobs, lJob.get(index));
                if (selectedJob > 0) btJob.setText(jobs[selectedJob]);
                else btJob.setText(lJob.get(index));
                btJob.setTextColor(Color.BLACK);
            }
        }
    }

    private String getJob() {
        return "" + (Helpers.isEmptyPekerjaan(this) ? etJob.getText() : btJob.getText().toString().equals("Pekerjaan") ? "" : btJob.getText());
    }

    private String toFinalString(List<String> arr) {
        String out = arr.get(0);
        if (arr.size() > 1) {
            for (int i = 1; i < arr.size(); i++) {
                out += Constants.SIBLING_SEPARATOR + arr.get(i);
            }
        }

        return out;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_job_spnr)
    public void onClickJob(final Button button) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(jobs, selectedJob, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                button.setTextColor(Color.BLACK);
                button.setText(jobs[which]);
                selectedJob = which;
            }
        });
        builder.show();
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            if (type == TYPE_EDIT) {
                lName.set(index, ""+etName.getText());
                lJob.set(index, getJob());
                lAge.set(index, ""+etAge.getText());
                lAddress.set(index, ""+etAddress.getText());
            } else if (type == TYPE_ADD) {
                lName.add(""+etName.getText());
                lJob.add(getJob());
                lAge.add(""+etAge.getText());
                lAddress.add(""+etAddress.getText());
            }

            skck.setSkckSiblingName(toFinalString(lName));
            skck.setSkckSiblingAge(toFinalString(lAge));
            skck.setSkckSiblingJob(toFinalString(lJob));
            skck.setSkckSiblingAddress(toFinalString(lAddress));

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(skck));
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
