package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.InputMissingActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.Kehilangan;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KehilanganActivity extends BaseActivity {
    @BindView(R.id.lap_box_missing_details) LinearLayout llMissingDetails;
    @BindView(R.id.lap_data_missing_title) TextView tvMissingTitle;
    @BindView(R.id.lap_data_missing_stuff) TextView tvMissingStuff;

    private Kehilangan kehilangan = new Kehilangan();
    private boolean[] steps = {false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_kehilangan;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_kehilangan, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_kehilangan_2, APP_NAME);
    }

    @OnClick(R.id.lap_btn_missing_edit)
    public void onEdit() {
        Intent intent = new Intent(this, InputMissingActivity.class);
        intent.putExtra("DATA", new Gson().toJson(kehilangan));
        startActivityForResult(intent, InputMissingActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case InputMissingActivity.REQUEST_CODE:
                    Kehilangan dKehilangan = new Gson().fromJson(data.getStringExtra("DATA"), Kehilangan.class);
                    setData(dKehilangan);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvMissingTitle);
                break;
            }
        }
    }

    private void setData(Kehilangan data) {
        // Set Global Object Pengaduan for send to API later
        kehilangan.setMissingStuff(data.getMissingStuff());

        // Set View
        llMissingDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvMissingTitle.setText(kehilangan.getMissingStuff());
        tvMissingStuff.setText(kehilangan.getMissingStuff());
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_URAIAN, kehilangan.getMissingStuff());

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitKehilangan(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
