package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Lalin;
import com.baraciptalaksana.espktkedirikota.models.Pendapat;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputPendapatActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    @BindView(R.id.laporan_pendapat_date) Button btDate;
    @BindView(R.id.laporan_pendapat_time_start) Button btTimeStart;
    @BindView(R.id.laporan_pendapat_time_end) Button btTimeEnd;
    @BindView(R.id.laporan_pendapat_name) AppCompatEditText etName;
    @BindView(R.id.laporan_pendapat_name_p) AppCompatEditText etNameP;
    @BindView(R.id.laporan_pendapat_phone) AppCompatEditText etPhone;
    @BindView(R.id.laporan_pendapat_location) AppCompatEditText etLocation;
    @BindView(R.id.laporan_pendapat_route) AppCompatEditText etRoute;
    @BindView(R.id.laporan_pendapat_meeting_point) AppCompatEditText etMP;
    @BindView(R.id.laporan_pendapat_agenda) AppCompatEditText etAgenda;
    @BindView(R.id.laporan_pendapat_props) AppCompatEditText etProps;
    @BindView(R.id.laporan_pendapat_korlap_1) AppCompatEditText etKorlap1;
    @BindView(R.id.laporan_pendapat_korlap_2) AppCompatEditText etKorlap2;
    @BindView(R.id.laporan_pendapat_type) AppCompatEditText etType;
    @BindView(R.id.laporan_pendapat_capacity) AppCompatEditText etCaps;

    public static final int REQUEST_CODE = 51;

    private String date = null, timeStart = null, timeEnd = null;
    private Timepoint minEndTime = null;

    Pendapat pendapat = new Pendapat();
    AwesomeValidation validation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_pendapat);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_lalin));

        addValidation();

        Pendapat data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Pendapat.class);
        if (data != null && data.getPendapatDate() != null) {
            setData(data);
        }
    }

    private void addValidation() {
        validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_pendapat_name, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_name_p, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_phone, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_location, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_meeting_point, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_korlap_1, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_type, Helpers.patternMinLength(1), R.string.error_required);
        validation.addValidation(this, R.id.laporan_pendapat_capacity, Helpers.patternMinLength(1), R.string.error_required);

        Helpers.addMultilineValidation(validation, this, R.id.laporan_pendapat_route_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addMultilineValidation(validation, this, R.id.laporan_pendapat_agenda_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addMultilineValidation(validation, this, R.id.laporan_pendapat_props_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_pendapat_date, ""+btDate.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_pendapat_time_start, ""+btTimeStart.getText(), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_pendapat_time_end, ""+btTimeEnd.getText(), R.string.error_required);
    }

    private void setData(Pendapat data) {
        btDate.setText(data.getPendapatDate());
        btTimeEnd.setText(data.getPendapatTimeEnd());
        btTimeStart.setText(data.getPendapatTimeStart());

        btDate.setTextColor(Color.BLACK);
        btTimeEnd.setTextColor(Color.BLACK);
        btTimeStart.setTextColor(Color.BLACK);

        etName.setText(data.getPendapatName());
        etNameP.setText(data.getPendapatNameP());
        etPhone.setText(data.getPendapatPhone());
        etLocation.setText(data.getPendapatLocation());
        etMP.setText(data.getPendapatMeetingPoint());
        etKorlap1.setText(data.getPendapatKorlap1());

        if (!Helpers.isEmptyString(data.getPendapatKorlap2()))
            etKorlap2.setText(data.getPendapatKorlap2());

        etType.setText(data.getPendapatType());
        etCaps.setText(data.getPendapatCapacities());
        etRoute.setText(data.getPendapatRoute());
        etAgenda.setText(data.getPendapatAgenda());
        etProps.setText(data.getPendapatProps());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_pendapat_date)
    public void onClickDate(Button button) {
        dialogDatePicker(button);
    }

    @OnClick(R.id.laporan_pendapat_time_start)
    public void onClickTime(Button button) {
        dialogTimeStartPicker(button);
    }

    @OnClick(R.id.laporan_pendapat_time_end)
    public void onClickTimeEnd(Button button) {
        dialogTimeEndPicker(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {
            pendapat.setPendapatDate(""+btDate.getText());
            pendapat.setPendapatTimeEnd(""+btTimeEnd.getText());
            pendapat.setPendapatTimeStart(""+btTimeStart.getText());
            pendapat.setPendapatName(""+etName.getText());
            pendapat.setPendapatNameP(""+etNameP.getText());
            pendapat.setPendapatPhone(""+etPhone.getText());
            pendapat.setPendapatLocation(""+etLocation.getText());
            pendapat.setPendapatMeetingPoint(""+etMP.getText());
            pendapat.setPendapatKorlap1(""+etKorlap1.getText());

            if (!Helpers.isEmptyString(etKorlap2) && etKorlap2.getText().length() >= 3)
                pendapat.setPendapatKorlap2(""+etKorlap2.getText());

            pendapat.setPendapatType(""+etType.getText());
            pendapat.setPendapatCapacities(""+etCaps.getText());
            pendapat.setPendapatRoute(""+etRoute.getText());
            pendapat.setPendapatAgenda(""+etAgenda.getText());
            pendapat.setPendapatProps(""+etProps.getText());

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(pendapat));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void dialogDatePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(date);

        Calendar clMin = Calendar.getInstance();
        clMin.add(Calendar.DATE, 9);

        Calendar clMax = Calendar.getInstance();
        clMax.add(Calendar.MONTH, 6);

        Helpers.dialogDatePicker(this, calendar, clMax, clMin, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                date = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                bt.setText(date);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogTimeStartPicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(timeStart);

        Helpers.dialogTimePicker(this, calendar, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                timeStart = Helpers.getFormattedTime(hourOfDay, minute, second);
                minEndTime = new Timepoint(hourOfDay, minute);

                bt.setText(timeStart);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Timepickerdialog");
    }

    private void dialogTimeEndPicker(final Button bt) {
        if (minEndTime == null) {
            Helpers.dialog(this, "Waktu Mulai Kosong", "Mohon masukkan waktu mulai sebelum menentukan waktu selesai").show();
        } else {
            Calendar calendar = Helpers.getCalendar(timeEnd);

            Helpers.dialogTimePicker(this, calendar, null, minEndTime, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                    timeEnd = Helpers.getFormattedTime(hourOfDay, minute, second);

                    bt.setText(timeEnd);
                    bt.setTextColor(Color.BLACK);
                }
            }).show(getFragmentManager(), "Timepickerdialog");
        }
    }
}
