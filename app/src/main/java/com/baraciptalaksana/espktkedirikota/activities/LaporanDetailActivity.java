package com.baraciptalaksana.espktkedirikota.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePermohonan;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePermohonanDetail;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.victor.loading.rotate.RotateLoading;

import net.glxn.qrgen.android.QRCode;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaporanDetailActivity extends AppCompatActivity implements Constants {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.scrollView) NestedScrollView scrollView;
    @BindView(R.id.loading_frame) RelativeLayout loadingFrame;
    @BindView(R.id.loading) RotateLoading loading;
    @BindView(R.id.laporan_details_text_1) TextView tvText1;
    @BindView(R.id.laporan_details_text_2) TextView tvText2;
    @BindView(R.id.laporan_details_text_3) TextView tvText3;
    @BindView(R.id.laporan_details_text_4) TextView tvText4;
    @BindView(R.id.laporan_details_desc) TextView tvDesc;
    @BindView(R.id.include) LinearLayout llInclude;
    @BindView(R.id.laporan_details_name) TextView tvName;
    @BindView(R.id.laporan_details_nik) TextView tvNik;
    @BindView(R.id.laporan_details_address) TextView tvAddress;
    @BindView(R.id.laporan_details_phone) TextView tvPhone;
    @BindView(R.id.laporan_details_email) TextView tvEmail;
    @BindView(R.id.laporan_details_date) TextView tvDate;
    @BindView(R.id.laporan_details_time) TextView tvTime;
    @BindView(R.id.laporan_details_reqcode) TextView tvCode;
    @BindView(R.id.laporan_details_qrcode) ImageView ivQRCode;
    @BindView(R.id.laporan_details_barcode) ImageView ivBarCode;

    private Activity mActivity = this;
    private boolean inProgress = false;
    private ResponsePermohonan.Permohonan permohonan;
    private ResponsePermohonanDetail.Detail data;
    private String[] waktuDtg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        waktuDtg = getResources().getStringArray(R.array.verification_times);

        title.setText(getIntent().getStringExtra("TITLE"));
        permohonan = new Gson().fromJson(getIntent().getStringExtra("DATA"), ResponsePermohonan.Permohonan.class);

        getData();
    }

    protected void startLoading() {
        inProgress = true;
        if (loadingFrame != null) {
            scrollView.setVisibility(View.GONE);
            loadingFrame.setVisibility(View.VISIBLE);
            loading.start();
        }
    }

    protected void stopLoading() {
        inProgress = false;
        if (loadingFrame != null) {
            loading.stop();
            loadingFrame.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!inProgress)
            return super.dispatchTouchEvent(ev);
        return true;
    }

    public void getData() {
        startLoading();
        Api.Factory.getInstance().getPermohonanDetail(permohonan.getKode(), ""+permohonan.getRefId()).enqueue(new Callback<ResponsePermohonanDetail>() {
            @Override
            public void onResponse(Call<ResponsePermohonanDetail> call, Response<ResponsePermohonanDetail> response) {
                stopLoading();
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    data = response.body().getData();
                    if (data != null) {
                        setContent();
                    } else {
                        Helpers.toastErrorNetwork(mActivity);
                    }
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                }
            }

            @Override
            public void onFailure(Call<ResponsePermohonanDetail> call, Throwable t) {
                startLoading();
                Helpers.toastErrorNetwork(mActivity);
            }
        });
    }

    private void setContent() {
        String name = data.getNama();
        String text1 = "Permohonan Laporan Pengaduan";
        String text2 = "Melaporkan pengaduan";
        String text3 = "Laporan Pengaduan";
        String desc = !Helpers.isEmptyString(data.getDetail()) ? data.getDetail() : "Pengaduan";
        setInfoView(R.layout.content_detail_info, getString(R.string.text_form_pengaduan_2, APP_NAME));

        switch (permohonan.getKode()) {
            case "HILANG":
                text1 = "Permohonan Laporan Kehilangan";
                text2 = "Melaporkan telah kehilangan";
                text3 = "Laporan Kehilangan";
                desc = !Helpers.isEmptyString(data.getDetail()) ? data.getDetail() : "Kehilangan";
                setInfoView(R.layout.content_detail_info, getString(R.string.text_form_kehilangan_2, APP_NAME));
                break;
            case "SIM":
                text1 = "Permohonan Pembuatan SIM";
                text2 = "Melakukan permohonan";
                text3 = "SIM";
                desc = "Pembuatan SIM";

                ResponsePermohonanDetail.Sim detailSim = data.getDetailSim();
                if (detailSim != null) {
                    desc = "Pembuatan SIM - " + detailSim.getJenis()
                        + ", Gol. " + detailSim.getGolongan()
                        + ", No. " + detailSim.getNo();
                }

                setInfoView(R.layout.content_detail_info_sim, null);
                break;
            case "SKCK":
                text1 = "Permohonan Pembuatan Surat Keterangan Catatan Kepolisian / SKCK";
                text2 = "Melakukan permohonan";
                text3 = "Laporan Kehilangan";
                desc = "Surat Keterangan Catatan Kepolisian / SKCK";
                setInfoView(R.layout.content_detail_info, getString(R.string.text_form_skck_2, APP_NAME));
                break;
            case "TUTUP":
                text1 = "Permohonan Penutupan Jalan";
                text2 = "Melakukan permohonan";
                text3 = "Laporan Penutupan Jalan";
                desc = "Penutupan Jalan";
                setInfoView(R.layout.content_detail_info_lalin, null);
                break;
            case "RAMAI":
                text1 = "Permohonan Ijin Keramaian";
                text2 = "Melakukan permohonan";
                text3 = "Laporan Ijin Keramaian";
                desc = !Helpers.isEmptyString(data.getDetail()) ? data.getDetail() : "Ijin Keramaian";
                setInfoView(R.layout.content_detail_info, getString(R.string.text_form_keramaian_2, APP_NAME));
                break;
            case "DEMO":
                text1 = "Permohonan Ijin Penyampaian Pendapat di Muka Umum";
                text2 = "Melakukan permohonan";
                text3 = "Laporan Ijin Penyampaian Pendapat di Muka Umum";
                desc = "Ijin Penyampaian Pendapat di Muka Umum";

                if (!Helpers.isEmptyString(data.getDetail())) {
                    desc += ", " + Helpers.getPermohonanDetailDate(data.getDetail());
                }

                setInfoView(R.layout.content_detail_info, getString(R.string.text_form_pendapat_2, APP_NAME));
                break;
            case "ASING":
                text1 = "Laporan";
                text2 = "Data WNA";
                text3 = "Laporan";
                desc = "";

                ResponsePermohonanDetail.Asing detailAsing = data.getDetailAsing();
                if (detailAsing != null) {
                    desc += "Nama: " + detailAsing.getNama() + "\n";
                    desc += "Alamat: " + detailAsing.getAlamat() + "\n";
                    desc += "Kebangsaan: " + detailAsing.getKebangsaan() + "\n";
                    desc += "Tempat & Tanggal Lahir: " + detailAsing.getTtl() + "\n";
                    desc += "Pekerjaan: " + detailAsing.getPekerjaan() + "\n";
                    desc += "No. Paspor: " + detailAsing.getNoPassport() + "\n";
                    desc += "Dikeluarkan Oleh: " + detailAsing.getDikeluarkan() + "\n";
                    desc += "Visa: " + detailAsing.getVisa() + "\n";
                    desc += "Tanggal Kedatangan: " + detailAsing.getTglKed() + "\n";
                    desc += "Dari Negara: " + detailAsing.getDari() + "\n";
                    desc += "Maksud: " + detailAsing.getMaksud() + "\n";
                    desc += "Tanggal Keberangkatan: " + detailAsing.getTglKed() + "\n";
                    desc += "Tujuan Selanjutnya: " + detailAsing.getTujuanSelanjutnya() + "\n";
                    desc += "Negara Tujuan Selanjutnya: " + detailAsing.getNegaraSelanjutnya() + "\n";
                    desc += "Kota di Indonesia: " + detailAsing.getKota();
                }

                setInfoView(R.layout.content_detail_info, getString(R.string.text_form_asing_2));
                break;
            case "KONSUL":
                text1 = "Permohonan Konsultasi";
                text2 = "Mengajukan permohonan konsultasi tentang:";
                text3 = "Laporan Permohonan Konsultasi";
                desc = !Helpers.isEmptyString(data.getDetail()) ? data.getDetail() : "-";
                setInfoView(R.layout.content_detail_info, getString(R.string.text_form_konsultasi_2, APP_NAME));
        }

        tvText1.setText(getString(R.string.text_laporan_details_1, name, text1));
        tvText2.setText(getString(R.string.text_laporan_details_2, APP_NAME));
        tvText3.setText(text2);
        tvText4.setText(getString(R.string.text_laporan_details_4, text3, APP_NAME));
        tvDesc.setText(desc);

        if (data.getKode() != null) {
            Bitmap code = QRCode.from(data.getKode()).bitmap();
            ivQRCode.setImageBitmap(code);
            tvCode.setText(data.getKode());
            generateBarCode(data.getKode());
        }

        tvName.setText(data.getNama());
        tvNik.setText(data.getNik());
        tvAddress.setText(data.getAlamat());
        tvPhone.setText(data.getTelp());
        tvEmail.setText(data.getEmail());
        tvDate.setText(Helpers.getPermohonanDetailDate(data.getTglDtg()));

        if (data.getWaktuDtg().contains("pagi"))
            tvTime.setText(waktuDtg[0]);
        else if (data.getWaktuDtg().contains("siang"))
            tvTime.setText(waktuDtg[1]);
    }

    private void setInfoView(@LayoutRes int layout, String text) {
        CardView view = (CardView) View.inflate(mActivity, layout, null);
        if (text != null) {
            TextView textView = view.findViewById(R.id.detail_label);
            textView.setText(text);
        }

        llInclude.addView(view);
    }

    public void generateBarCode(String data){
        com.google.zxing.Writer c9 = new Code128Writer();
        Bitmap mBitmap = null;
        int w = 500;
        int h = 100;

        try {
            BitMatrix bm = c9.encode(data,BarcodeFormat.CODE_128,w, h);
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {

                    mBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        if (mBitmap != null) {
            ivBarCode.setImageBitmap(mBitmap);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }
}
