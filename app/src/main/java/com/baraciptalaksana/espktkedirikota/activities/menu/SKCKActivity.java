package com.baraciptalaksana.espktkedirikota.activities.menu;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKFatherActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKHWActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKMotherActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKOthersActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKPelanggaranActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKPidanaActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKSiblingActivity;
import com.baraciptalaksana.espktkedirikota.activities.form.skck.InputSKCKSponsorActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.common.BaseActivity;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SKCKActivity extends BaseActivity {
    @BindView(R.id.lap_box_skck_details) LinearLayout llSKCKDetails;
    @BindView(R.id.lap_data_skck_title) TextView tvSKCKTitle;
    @BindView(R.id.lap_data_skck_marital) TextView tvSKCKMarital;
    @BindView(R.id.lap_data_skck_passport) TextView tvSKCKPassport;
    @BindView(R.id.lap_data_skck_kitas) TextView tvSKCKKitas;

    @BindView(R.id.lap_box_skck_hw) LinearLayout llSKCKHW;
    @BindView(R.id.lap_box_skck_hw_details) LinearLayout llSKCKHWDetails;
    @BindView(R.id.lap_data_skck_hw_title) TextView tvSKCKHWTitle;
    @BindView(R.id.lap_data_skck_hw_age) TextView tvSKCKHWAge;
    @BindView(R.id.lap_data_skck_hw_job) TextView tvSKCKHWJob;
    @BindView(R.id.lap_data_skck_hw_religion) TextView tvSKCKHWReligion;
    @BindView(R.id.lap_data_skck_hw_address) TextView tvSKCKHWAddress;
    @BindView(R.id.lap_data_skck_hw_nation) TextView tvSKCKHWNation;

    @BindView(R.id.lap_box_skck_father_details) LinearLayout llSKCKFatherDetails;
    @BindView(R.id.lap_data_skck_father_title) TextView tvSKCKFatherTitle;
    @BindView(R.id.lap_data_skck_father_age) TextView tvSKCKFatherAge;
    @BindView(R.id.lap_data_skck_father_job) TextView tvSKCKFatherJob;
    @BindView(R.id.lap_data_skck_father_religion) TextView tvSKCKFatherReligion;
    @BindView(R.id.lap_data_skck_father_address) TextView tvSKCKFatherAddress;
    @BindView(R.id.lap_data_skck_father_nation) TextView tvSKCKFatherNation;

    @BindView(R.id.lap_box_skck_mother_details) LinearLayout llSKCKMotherDetails;
    @BindView(R.id.lap_data_skck_mother_title) TextView tvSKCKMotherTitle;
    @BindView(R.id.lap_data_skck_mother_age) TextView tvSKCKMotherAge;
    @BindView(R.id.lap_data_skck_mother_job) TextView tvSKCKMotherJob;
    @BindView(R.id.lap_data_skck_mother_religion) TextView tvSKCKMotherReligion;
    @BindView(R.id.lap_data_skck_mother_address) TextView tvSKCKMotherAddress;
    @BindView(R.id.lap_data_skck_mother_nation) TextView tvSKCKMotherNation;

    @BindView(R.id.lap_box_skck_sibling_details) LinearLayout llSKCKSiblingDetails;
    @BindView(R.id.lap_data_skck_sibling_title) TextView tvSKCKSiblingTitle;

    @BindView(R.id.lap_box_skck_pidana_details) LinearLayout llSKCKPidanaDetails;
    @BindView(R.id.lap_data_skck_pidana_title) TextView tvSKCKPidanaTitle;
    @BindView(R.id.lap_data_skck_pidana) TextView tvSKCKPidana;
    @BindView(R.id.lap_data_skck_pidana_desc) TextView tvSKCKPidanaDesc;
    @BindView(R.id.lap_data_skck_pidana_putusan) TextView tvSKCKPidanaPutusan;
    @BindView(R.id.lap_data_skck_pidana_desc_on) TextView tvSKCKPidanaDescOn;
    @BindView(R.id.lap_data_skck_pidana_process) TextView tvSKCKPidanaProcess;

    @BindView(R.id.lap_box_skck_pelanggaran_details) LinearLayout llSKCKPelanggaranDetails;
    @BindView(R.id.lap_data_skck_pelanggaran_title) TextView tvSKCKPelanggaranTitle;
    @BindView(R.id.lap_data_skck_pelanggaran) TextView tvSKCKPelanggaran;
    @BindView(R.id.lap_data_skck_pelanggaran_desc) TextView tvSKCKPelanggaranDesc;
    @BindView(R.id.lap_data_skck_pelanggaran_process) TextView tvSKCKPelanggaranProcess;

    @BindView(R.id.lap_box_skck_others_details) LinearLayout llSKCKOthersDetails;
    @BindView(R.id.lap_data_skck_others_title) TextView tvSKCKOthersTitle;
    @BindView(R.id.lap_data_skck_others_job) TextView tvSKCKOthersJob;
    @BindView(R.id.lap_data_skck_others_hobby) TextView tvSKCKOthersHobby;
    @BindView(R.id.lap_data_skck_others_phone) TextView tvSKCKOthersPhone;

    @BindView(R.id.lap_box_skck_sponsor_details) LinearLayout llSKCKSponsorDetails;
    @BindView(R.id.lap_data_skck_sponsor_title) TextView tvSKCKSponsorTitle;
    @BindView(R.id.lap_data_skck_sponsor_address) TextView tvSKCKSponsorAddress;
    @BindView(R.id.lap_data_skck_sponsor_type) TextView tvSKCKSponsorType;
    @BindView(R.id.lap_data_skck_sponsor_phone) TextView tvSKCKSponsorPhone;

    private SKCK skck = new SKCK();
    private boolean[] steps = {false, false, false, false, false, false, false, false, false, false};

    @Override
    protected void onCreateBase(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_menu_skck;
    }

    @Override
    protected boolean[] getSteps() {
        return steps;
    }

    @Override
    protected String getFormLabelText() {
        return getString(R.string.text_form_skck, APP_NAME);
    }

    @Override
    protected String getFormLabel2Text() {
        return getString(R.string.text_form_skck_2, APP_NAME);
    }

    @OnClick(R.id.lap_btn_skck_edit)
    public void onEdit() {
        Intent intent = new Intent(this, InputSKCKActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_hw_edit)
    public void onEditHW() {
        if (Helpers.isEmptyString(skck.getSkckMarital())) {
            Helpers.dialog(this, "Status Pernikahan", "Anda belum memasukan data status pernikahan.").show();
        } else {
            Intent intent = new Intent(this, InputSKCKHWActivity.class);
            intent.putExtra("DATA", new Gson().toJson(skck));
            startActivityForResult(intent, InputSKCKHWActivity.REQUEST_CODE);
        }
    }

    @OnClick(R.id.lap_btn_skck_father_edit)
    public void onEditFather() {
        Intent intent = new Intent(this, InputSKCKFatherActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKFatherActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_mother_edit)
    public void onEditMother() {
        Intent intent = new Intent(this, InputSKCKMotherActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKMotherActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_sibling_edit)
    public void onNewSibling() {
        Intent intent = new Intent(this, InputSKCKSiblingActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        intent.putExtra("TYPE", InputSKCKSiblingActivity.TYPE_ADD);
        startActivityForResult(intent, InputSKCKSiblingActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_pidana_edit)
    public void onEditPidana() {
        Intent intent = new Intent(this, InputSKCKPidanaActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKPidanaActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_pelanggaran_edit)
    public void onEditPelanggaran() {
        Intent intent = new Intent(this, InputSKCKPelanggaranActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKPelanggaranActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_others_edit)
    public void onEditOthers() {
        Intent intent = new Intent(this, InputSKCKOthersActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKOthersActivity.REQUEST_CODE);
    }

    @OnClick(R.id.lap_btn_skck_sponsor_edit)
    public void onEditSponsor() {
        Intent intent = new Intent(this, InputSKCKSponsorActivity.class);
        intent.putExtra("DATA", new Gson().toJson(skck));
        startActivityForResult(intent, InputSKCKSponsorActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            SKCK dSkck;
            switch (requestCode) {
                case InputSKCKActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setData(dSkck);
                    steps[1] = true;
                    Helpers.scrollToView(scrollView, tvSKCKTitle);
                break;
                case InputSKCKHWActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataHW(dSkck);
                    steps[4] = true;
                    Helpers.scrollToView(scrollView, tvSKCKHWTitle);
                break;
                case InputSKCKFatherActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataFather(dSkck);
                    steps[5] = true;
                    Helpers.scrollToView(scrollView, tvSKCKFatherTitle);
                break;
                case InputSKCKMotherActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataMother(dSkck);
                    steps[6] = true;
                    Helpers.scrollToView(scrollView, tvSKCKMotherTitle);
                break;
                case InputSKCKSiblingActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataSibling(dSkck);
                    Helpers.scrollToView(scrollView, tvSKCKSiblingTitle);
                break;
                case InputSKCKPidanaActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataPidana(dSkck);
                    steps[7] = true;
                    Helpers.scrollToView(scrollView, tvSKCKPidanaTitle);
                break;
                case InputSKCKPelanggaranActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataPelanggaran(dSkck);
                    steps[8] = true;
                    Helpers.scrollToView(scrollView, tvSKCKPelanggaranTitle);
                break;
                case InputSKCKOthersActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataOthers(dSkck);
                    steps[9] = true;
                    Helpers.scrollToView(scrollView, tvSKCKOthersTitle);
                break;
                case InputSKCKSponsorActivity.REQUEST_CODE:
                    dSkck = new Gson().fromJson(data.getStringExtra("DATA"), SKCK.class);
                    setDataSponsor(dSkck);
                    Helpers.scrollToView(scrollView, tvSKCKSponsorTitle);
            }
        }
    }

    private void setData(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckMarital(data.getSkckMarital());
        skck.setSkckPassport(data.getSkckPassport());
        skck.setSkckKitas(data.getSkckKitas());

        // Set View
        llSKCKDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKMarital.setText(skck.getSkckMarital());
        tvSKCKKitas.setText(Helpers.isEmptyString(skck.getSkckKitas()) ? "-" : skck.getSkckKitas());
        tvSKCKPassport.setText(Helpers.isEmptyString(skck.getSkckKitas()) ? "-" : skck.getSkckPassport());

        if (skck.getSkckMarital().equals("Kawin")) {
            llSKCKHW.setVisibility(View.VISIBLE);
        } else {
            llSKCKHW.setVisibility(View.GONE);
            steps[4] = true;
        }
    }

    private void setDataHW(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckHWName(data.getSkckHWName());
        skck.setSkckHWAge(data.getSkckHWAge());
        skck.setSkckHWReligion(data.getSkckHWReligion());
        skck.setSkckHWJob(data.getSkckHWJob());
        skck.setSkckHWNation(data.getSkckHWNation());
        skck.setSkckHWAddress(data.getSkckHWAddress());

        // Set View
        llSKCKHWDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKHWTitle.setText("Pasangan: " + skck.getSkckHWName());
        tvSKCKHWAge.setText(skck.getSkckHWAge());
        tvSKCKHWReligion.setText(skck.getSkckHWReligion());
        tvSKCKHWJob.setText(skck.getSkckHWJob());
        tvSKCKHWNation.setText(skck.getSkckHWNation());
        tvSKCKHWAddress.setText(skck.getSkckHWAddress());
    }

    private void setDataFather(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckFatherName(data.getSkckFatherName());
        skck.setSkckFatherAge(data.getSkckFatherAge());
        skck.setSkckFatherReligion(data.getSkckFatherReligion());
        skck.setSkckFatherJob(data.getSkckFatherJob());
        skck.setSkckFatherNation(data.getSkckFatherNation());
        skck.setSkckFatherAddress(data.getSkckFatherAddress());

        // Set View
        llSKCKFatherDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKFatherTitle.setText("Ayah: " + skck.getSkckFatherName());
        tvSKCKFatherAge.setText(skck.getSkckFatherAge());
        tvSKCKFatherReligion.setText(skck.getSkckFatherReligion());
        tvSKCKFatherJob.setText(skck.getSkckFatherJob());
        tvSKCKFatherNation.setText(skck.getSkckFatherNation());
        tvSKCKFatherAddress.setText(skck.getSkckFatherAddress());
    }

    private void setDataMother(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckMotherName(data.getSkckMotherName());
        skck.setSkckMotherAge(data.getSkckMotherAge());
        skck.setSkckMotherReligion(data.getSkckMotherReligion());
        skck.setSkckMotherJob(data.getSkckMotherJob());
        skck.setSkckMotherNation(data.getSkckMotherNation());
        skck.setSkckMotherAddress(data.getSkckMotherAddress());

        // Set View
        llSKCKMotherDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKMotherTitle.setText("Ibu: " + skck.getSkckMotherName());
        tvSKCKMotherAge.setText(skck.getSkckMotherAge());
        tvSKCKMotherReligion.setText(skck.getSkckMotherReligion());
        tvSKCKMotherJob.setText(skck.getSkckMotherJob());
        tvSKCKMotherNation.setText(skck.getSkckMotherNation());
        tvSKCKMotherAddress.setText(skck.getSkckMotherAddress());
    }

    private void setDataSibling(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckSiblingName(data.getSkckSiblingName());
        skck.setSkckSiblingAge(data.getSkckSiblingAge());
        skck.setSkckSiblingJob(data.getSkckSiblingJob());
        skck.setSkckSiblingAddress(data.getSkckSiblingAddress());

        // Set View
        llSKCKSiblingDetails.setVisibility(View.VISIBLE);

        // Set Text Value

        String[] sName = skck.getSkckSiblingName().split(Constants.SIBLING_SEPARATOR);
        String[] sAge = skck.getSkckSiblingAge().split(Constants.SIBLING_SEPARATOR);
        String[] sJob = skck.getSkckSiblingJob().split(Constants.SIBLING_SEPARATOR);
        String[] sAddress = skck.getSkckSiblingAddress().split(Constants.SIBLING_SEPARATOR);

        llSKCKSiblingDetails.removeAllViews();

        for (int i = 0; i < sName.length; i++) {
            generateSiblingView(i, sName[i], sAge[i], sJob[i], sAddress[i]);
        }

    }

    private void setDataPidana(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckPidana(data.getSkckPidana());
        skck.setSkckPidanaDesc(data.getSkckPidanaDesc());
        skck.setSkckPidanaDescOn(data.getSkckPidanaDescOn());
        skck.setSkckPidanaProccess(data.getSkckPidanaProccess());
        skck.setSkckPidanaPutusan(data.getSkckPidanaPutusan());

        // Set View
        llSKCKPidanaDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKPidana.setText(skck.getSkckPidana());
        tvSKCKPidanaDesc.setText(skck.getSkckPidanaDesc());
        tvSKCKPidanaDescOn.setText(skck.getSkckPidanaDescOn());
        tvSKCKPidanaProcess.setText(skck.getSkckPidanaProccess());
        tvSKCKPidanaPutusan.setText(skck.getSkckPidanaPutusan());
    }

    private void setDataPelanggaran(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckPelanggaran(data.getSkckPelanggaran());
        skck.setSkckPelanggaranDesc(data.getSkckPelanggaranDesc());
        skck.setSkckPelanggaranProccess(data.getSkckPelanggaranProccess());

        // Set View
        llSKCKPelanggaranDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKPelanggaran.setText(skck.getSkckPelanggaran());
        tvSKCKPelanggaranDesc.setText(skck.getSkckPelanggaranDesc());
        tvSKCKPelanggaranProcess.setText(skck.getSkckPelanggaranProccess());
    }

    private void setDataOthers(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckOthersJob(data.getSkckOthersJob());
        skck.setSkckOthersHobby(data.getSkckOthersHobby());
        skck.setSkckOthersPhone(data.getSkckOthersPhone());

        // Set View
        llSKCKOthersDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKOthersJob.setText(skck.getSkckOthersJob());
        tvSKCKOthersHobby.setText(skck.getSkckOthersHobby());
        tvSKCKOthersPhone.setText(skck.getSkckOthersPhone());
    }

    private void setDataSponsor(SKCK data) {
        // Set Global Object Pengaduan for send to API later
        skck.setSkckSponsorName(data.getSkckSponsorName());
        skck.setSkckSponsorAddress(data.getSkckSponsorAddress());
        skck.setSkckSponsorType(data.getSkckSponsorType());
        skck.setSkckSponsorPhone(data.getSkckSponsorPhone());

        // Set View
        llSKCKSponsorDetails.setVisibility(View.VISIBLE);

        // Set Text Value
        tvSKCKSponsorTitle.setText(skck.getSkckSponsorName());
        tvSKCKSponsorAddress.setText(skck.getSkckSponsorAddress());
        tvSKCKSponsorType.setText(skck.getSkckSponsorType());
        tvSKCKSponsorPhone.setText(skck.getSkckSponsorPhone());
    }

    private TextView genTVDesc(String label) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/bariol_regular.otf");
        float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 16, getResources().getDisplayMetrics());
        TextView tvDesc = new TextView(this);
        tvDesc.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tvDesc.setText(label);
        tvDesc.setTypeface(font);
        tvDesc.setTextColor(getResources().getColor(R.color.textGray));
        tvDesc.setTextSize(textSize);

        return tvDesc;
    }

    private TextView genTVField(String value) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/bariol_regular.otf");
        float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 16, getResources().getDisplayMetrics());
        TextView tvField = new TextView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
        tvField.setLayoutParams(layoutParams);
        tvField.setText(value);
        tvField.setTypeface(font);
        tvField.setTextColor(getResources().getColor(android.R.color.black));
        tvField.setTextSize(textSize);

        return tvField;
    }

    private RelativeLayout genRLField(String label, String value) {
        int spMiddle = (int) getResources().getDimension(R.dimen.spacing_middle);
        RelativeLayout rl = new RelativeLayout(this);
        rl.setPadding(spMiddle, spMiddle, spMiddle, spMiddle);
        rl.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        rl.addView(genTVDesc(label));
        rl.addView(genTVField(value));

        return rl;
    }

    private View genSeparator() {
        View separator = new View(this);
        separator.setMinimumHeight(1);
        separator.setBackgroundColor(getResources().getColor(R.color.backgroundGray));
        separator.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Helpers.dpToPx(this, 1)));

        return separator;
    }

    private void generateSiblingView(final int index, String name, String age, String job, String address) {
        LinearLayout container = new LinearLayout(this);
        container.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        container.setOrientation(LinearLayout.VERTICAL);

        container.addView(genSeparator());
        container.addView(genRLField("Nama", name));
        container.addView(genRLField("Umur", age));
        container.addView(genRLField("Pekerjaan", job));
        container.addView(genRLField("Alamat", address));

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SKCKActivity.this, InputSKCKSiblingActivity.class);
                intent.putExtra("DATA", new Gson().toJson(skck));
                intent.putExtra("TYPE", InputSKCKSiblingActivity.TYPE_EDIT);
                intent.putExtra("INDEX", index);
                startActivityForResult(intent, InputSKCKSiblingActivity.REQUEST_CODE);
            }
        });

        llSKCKSiblingDetails.addView(container);
    }

    @Override
    protected void doSubmitForm() {
        startLoading();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        buildMultipartBody(builder);

        builder.addFormDataPart(FORM_STATUS_NIKAH, skck.getSkckMarital());
        builder.addFormDataPart(FORM_NO_PASSPORT, skck.getSkckPassport());
        builder.addFormDataPart(FORM_NO_KITAS_KITAP, skck.getSkckKitas());

        builder.addFormDataPart(FORM_NAMA_PASANGAN, skck.getSkckHWName());
        builder.addFormDataPart(FORM_UMUR_PASANGAN, skck.getSkckHWAge());
        builder.addFormDataPart(FORM_AGAMA_PASANGAN, skck.getSkckHWReligion());
        builder.addFormDataPart(FORM_KEBANGSAAN_PASANGAN, skck.getSkckHWNation());
        builder.addFormDataPart(FORM_PEKERJAAN_PASANGAN, skck.getSkckHWJob());
        builder.addFormDataPart(FORM_ALAMAT_PASANGAN, skck.getSkckHWAddress());
        
        builder.addFormDataPart(FORM_NAMA_AYAH, skck.getSkckFatherName());
        builder.addFormDataPart(FORM_UMUR_AYAH, skck.getSkckFatherAge());
        builder.addFormDataPart(FORM_AGAMA_AYAH, skck.getSkckFatherReligion());
        builder.addFormDataPart(FORM_KEBANGSAAN_AYAH, skck.getSkckFatherNation());
        builder.addFormDataPart(FORM_PEKERJAAN_AYAH, skck.getSkckFatherJob());
        builder.addFormDataPart(FORM_ALAMAT_AYAH, skck.getSkckFatherAddress());

        builder.addFormDataPart(FORM_NAMA_IBU, skck.getSkckMotherName());
        builder.addFormDataPart(FORM_UMUR_IBU, skck.getSkckMotherAge());
        builder.addFormDataPart(FORM_AGAMA_IBU, skck.getSkckMotherReligion());
        builder.addFormDataPart(FORM_KEBANGSAAN_IBU, skck.getSkckMotherNation());
        builder.addFormDataPart(FORM_PEKERJAAN_IBU, skck.getSkckMotherJob());
        builder.addFormDataPart(FORM_ALAMAT_IBU, skck.getSkckMotherAddress());

        String[] sName = skck.getSkckSiblingName().split(Constants.SIBLING_SEPARATOR);
        String[] sAge = skck.getSkckSiblingAge().split(Constants.SIBLING_SEPARATOR);
        String[] sJob = skck.getSkckSiblingJob().split(Constants.SIBLING_SEPARATOR);
        String[] sAddress = skck.getSkckSiblingAddress().split(Constants.SIBLING_SEPARATOR);

        for (int i = 0; i < sName.length; i++) {
            builder.addFormDataPart(FORM_NAMA_SAUDARA, sName[i]);
            builder.addFormDataPart(FORM_UMUR_SAUDARA, sAge[i]);
            builder.addFormDataPart(FORM_PEKERJAAN_SAUDARA, sJob[i]);
            builder.addFormDataPart(FORM_ALAMAT_SAUDARA, sAddress[i]);
        }

        builder.addFormDataPart(FORM_PERNAH_DIPIDANA, skck.getSkckPidana());
        builder.addFormDataPart(FORM_PERKARA, skck.getSkckPidanaDesc());
        builder.addFormDataPart(FORM_VONIS, skck.getSkckPidanaPutusan());
        builder.addFormDataPart(FORM_SEDANG_DIPIDANA, skck.getSkckPidanaDescOn());
        builder.addFormDataPart(FORM_PROSES_HUKUM, skck.getSkckPidanaProccess());

        builder.addFormDataPart(FORM_PERNAH_PELANGGARAN, skck.getSkckPelanggaran());
        builder.addFormDataPart(FORM_PELANGGARAN, skck.getSkckPelanggaranDesc());
        builder.addFormDataPart(FORM_PROSES_PELANGGARAN, skck.getSkckPelanggaranProccess());

        builder.addFormDataPart(FORM_RIWAYAT, skck.getSkckOthersJob());
        builder.addFormDataPart(FORM_HOBI, skck.getSkckOthersHobby());
        builder.addFormDataPart(FORM_TELP_DARURAT, skck.getSkckOthersPhone());

        builder.addFormDataPart(FORM_NAMA_SPONSOR, Helpers.saveString(skck.getSkckSponsorName()));
        builder.addFormDataPart(FORM_JENIS_USAHA, Helpers.saveString(skck.getSkckSponsorType()));
        builder.addFormDataPart(FORM_TELP_ORG_ASING, Helpers.saveString(skck.getSkckSponsorPhone()));
        builder.addFormDataPart(FORM_ALAMAT_ORG_ASING, Helpers.saveString(skck.getSkckSponsorAddress()));

        MultipartBody requestBody = builder.build();

        Api.Factory.getInstance().doSubmitSKCK(requestBody).enqueue(new Callback<ResponseBaseForm>() {
            @Override
            public void onResponse(Call<ResponseBaseForm> call, Response<ResponseBaseForm> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    showSuccessDialog();
                    stopLoading();
                } else {
                    Helpers.toastErrorNetwork(mActivity);
                    stopLoading();
                }
            }

            @Override
            public void onFailure(Call<ResponseBaseForm> call, Throwable t) {
                Helpers.toastErrorNetwork(mActivity);
                stopLoading();
            }
        });
    }
}
