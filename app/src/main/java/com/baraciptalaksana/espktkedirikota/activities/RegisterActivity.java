package com.baraciptalaksana.espktkedirikota.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.RequestRegister;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBase;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.register_name) TextInputEditText inName;
    @BindView(R.id.register_email) TextInputEditText inEmail;
    @BindView(R.id.register_password) TextInputEditText inPassword;
    @BindView(R.id.loading) RotateLoading rlLoading;

    AwesomeValidation validation;
    private boolean inProgress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setupToolbar();

        validation = new AwesomeValidation(ValidationStyle.BASIC);
        validation.addValidation(this, R.id.register_name, ".{3,}", R.string.error_name);
        validation.addValidation(this, R.id.register_email, Patterns.EMAIL_ADDRESS, R.string.error_email);
        validation.addValidation(this, R.id.register_password, ".{6,}", R.string.error_password);
        validation.addValidation(this, R.id.register_conf_password, R.id.register_password, R.string.error_password_conf);

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    private void startLoading() {
        inProgress = true;
        rlLoading.setVisibility(View.VISIBLE);
        rlLoading.start();
    }

    private void stopLoading() {
        inProgress = false;
        rlLoading.stop();
        rlLoading.setVisibility(View.GONE);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!inProgress)
            return super.dispatchTouchEvent(ev);
        return true;
    }

    @OnClick(R.id.register_btn_submit)
    public void onSubmit() {
        if (validation.validate()) {

            startLoading();

            doSignUp();
        }
    }

    private void showDialog() {
        Helpers.dialog(this, R.string.dialog_register_title, R.string.dialog_register_message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.putExtra("EMAIL", ""+inEmail.getText());
                setResult(RESULT_OK, intent);
                finish();
            }
        }).show();
    }

    private void doSignUp() {
        RequestRegister request = new RequestRegister(
            ""+inName.getText(), ""+inEmail.getText(), ""+inPassword.getText()
        );

        Api.Factory.getInstance().doSignUp(request).enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                stopLoading();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(Constants.SUCCESS)) {
                        showDialog();
                    } else if (!Helpers.isEmptyString(response.body().getMessage())) {
                        Helpers.toastError(RegisterActivity.this, response.body().getMessage());
                    }
                } else {
                    Helpers.toastError(RegisterActivity.this, "Tidak dapat melakukan pendaftaran, Coba lagi!");
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                stopLoading();
                Helpers.toastErrorNetwork(RegisterActivity.this);
            }
        });
    }
}
