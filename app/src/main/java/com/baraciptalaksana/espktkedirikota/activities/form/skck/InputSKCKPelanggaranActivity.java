package com.baraciptalaksana.espktkedirikota.activities.form.skck;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.SKCK;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputSKCKPelanggaranActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_skck_pelanggaran) Button btPelanggaran;
    @BindView(R.id.laporan_skck_pelanggaran_desc) AppCompatEditText etDesc;
    @BindView(R.id.laporan_skck_pelanggaran_process) AppCompatEditText etProcess;

    public static final int REQUEST_CODE = 67;

    SKCK skck = new SKCK();
    AwesomeValidation validation;

    String[] pelanggaran;
    private int selectedPelanggaran = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_skck_pelanggaran);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_skck_pidana));

        pelanggaran = getResources().getStringArray(R.array.pidana);

        SKCK data = new Gson().fromJson(getIntent().getStringExtra("DATA"), SKCK.class);
        if (data != null && data.getSkckPelanggaran() != null) {
            setData(data);
        }
    }

    private void setValidation() {
        validation = new AwesomeValidation(ValidationStyle.BASIC);

        if (selectedPelanggaran == Helpers.getIndexOf(pelanggaran, "Pernah")) {
            Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_pelanggaran_desc_layout, Helpers.patternMinLength(1), R.string.error_required);
            Helpers.addMultilineValidation(validation, this, R.id.laporan_skck_pelanggaran_process_layout, Helpers.patternMinLength(1), R.string.error_required);
        } else if (selectedPelanggaran == Helpers.getIndexOf(pelanggaran, "Tidak Pernah")) {
            etDesc.setError(null);
            etDesc.setText("-");
            etDesc.setEnabled(false);

            etProcess.setError(null);
            etProcess.setText("-");
            etProcess.setEnabled(false);
        } else {
            Helpers.addButtonValidation(validation, this, R.id.laporan_skck_pelanggaran, "" + btPelanggaran.getText(), R.string.error_required);
        }
    }

    private void setData(SKCK data) {
        etDesc.setText(data.getSkckPelanggaranDesc());
        etProcess.setText(data.getSkckPelanggaranProccess());

        selectedPelanggaran = Helpers.getIndexOf(pelanggaran, data.getSkckPelanggaran());
        if (selectedPelanggaran > 0) btPelanggaran.setText(pelanggaran[selectedPelanggaran]);
        else btPelanggaran.setText(data.getSkckPelanggaran());

        btPelanggaran.setTextColor(Color.BLACK);

        checkEditText();
    }

    private void checkEditText() {
        if (selectedPelanggaran == Helpers.getIndexOf(pelanggaran, "Pernah")) {
            etDesc.setEnabled(true);
            etProcess.setEnabled(true);
        } else if (selectedPelanggaran == Helpers.getIndexOf(pelanggaran, "Tidak Pernah")) {
            etDesc.setError(null);
            etDesc.setText("-");
            etDesc.setEnabled(false);

            etProcess.setError(null);
            etProcess.setText("-");
            etProcess.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_skck_pelanggaran)
    public void onPelanggaran(Button button) {
        showPelanggaranChoiceDialog(button);
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        setValidation();

        if (validation.validate()) {
            skck.setSkckPelanggaranDesc(""+etDesc.getText());
            skck.setSkckPelanggaranProccess(""+etProcess.getText());
            skck.setSkckPelanggaran(pelanggaran[selectedPelanggaran]);

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(skck));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void showPelanggaranChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(pelanggaran, selectedPelanggaran, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setError(null);
                bt.setTextColor(Color.BLACK);
                bt.setText(pelanggaran[which]);
                selectedPelanggaran = which;

                checkEditText();
            }
        });
        builder.show();
    }
}
