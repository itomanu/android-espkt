package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.ColorRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.services.GPSTracker;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputLalinMapsInputActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;

    private static final float DEFAULT_ZOOM = 18;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 99;
    public static final String MARKER_TITLE_LOCATION = "Jalur Yang Ditutup";
    public static final String MARKER_TITLE_ALTERNATIVE = "Jalur Alternatif";

    private GoogleMap mMap;
    private LocationManager locationManager;
    private boolean mLocationPermissionGranted;
    private LatLng mDefaultLocation = new LatLng(-6.888527, 107.621318);

    private String sLocation = null, sAlternatif = null;

    String markerTitle;

    private int requestCode;
    @ColorRes int color;

    GPSTracker tracker;

    Marker marker;
    Polyline polyline;

    List<Marker> path = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_lalin_maps_maps);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_maps));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tracker = new GPSTracker(this);

        requestCode = getIntent().getIntExtra("REQUEST_CODE", 0);
        sLocation = getIntent().getStringExtra("LOCATION_PATH");
        sAlternatif = getIntent().getStringExtra("ALTERNATIVE_PATH");

        if (requestCode == InputLalinMapsActivity.REQUEST_CODE_LOCATION) {
            markerTitle = MARKER_TITLE_LOCATION;
            color = R.color.pathColor;
        } else if (requestCode == InputLalinMapsActivity.REQUEST_CODE_ALTERNATIVE) {
            markerTitle = MARKER_TITLE_ALTERNATIVE;
            color = R.color.pathColor2;
        }
        Helpers.dialog(this, R.string.dialog_help_map_title, R.string.dialog_help_map_2_message).show();

        if (!isLocationEnabled()) {
            showAlert();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                mLocationPermissionGranted = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
        }

        updateLocationUI();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        Helpers.dialog(this, "Enable Location",
            "Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                    "use this app", "Location Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            }, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).show();
    }

    private void getDeviceLocation() {
        try {
            if (mLocationPermissionGranted) {

                LatLng latLng = mDefaultLocation;

                if (tracker.canGetLocation()) {
                    latLng = new LatLng(tracker.getLatitude(), tracker.getLongitude());
                }

                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void updateLocationUI() {
        if (mMap == null) return;

        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else  getLocationPermission();
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @OnClick(R.id.maps_btn_clear_path)
    public void onClickClearPath(Button button) {
        for (Marker m: path) {
            m.remove();
        }
        if (polyline != null) polyline.remove();
        path.clear();
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (path.size() > 1) {
            String outputPath = path.get(0).getPosition().latitude + "," + path.get(0).getPosition().longitude;

            for (int i = 1; i < path.size(); i++) {
                outputPath += "|" + path.get(i).getPosition().latitude + "," + path.get(i).getPosition().longitude;
            }

            Intent intent = new Intent();
            intent.putExtra("PATH", outputPath);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Helpers.dialog(this, R.string.dialog_form_unfinished_title,
                    R.string.dialog_form_unfinished_message).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerDragListener(this);

        updateLocationUI();

        getDeviceLocation();

        if (requestCode == InputLalinMapsActivity.REQUEST_CODE_LOCATION) {
            drawToPath(sLocation, MARKER_TITLE_LOCATION, BitmapDescriptorFactory.HUE_RED, R.color.pathColor);
            drawGivenPath(sAlternatif, MARKER_TITLE_ALTERNATIVE, BitmapDescriptorFactory.HUE_GREEN, R.color.pathColor2);
        }

        if (requestCode == InputLalinMapsActivity.REQUEST_CODE_ALTERNATIVE) {
            drawToPath(sAlternatif, MARKER_TITLE_ALTERNATIVE, BitmapDescriptorFactory.HUE_GREEN, R.color.pathColor2);
            drawGivenPath(sLocation, MARKER_TITLE_LOCATION, BitmapDescriptorFactory.HUE_RED, R.color.pathColor);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (requestCode == InputLalinMapsActivity.REQUEST_CODE_LOCATION) {
            Marker m = mMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    .title(markerTitle).draggable(true));
            m.setTag("path");
            path.add(m);

            drawPath(color);
        }

        if (requestCode == InputLalinMapsActivity.REQUEST_CODE_ALTERNATIVE) {
            Marker m = mMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .title(markerTitle).draggable(true));
            m.setTag("path");
            path.add(m);

            drawPath(color);
        }
    }

    public void drawGivenPath(String sPath, String title, float hue, int color) {
        if (sPath != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            PolylineOptions po = new PolylineOptions().color(getResources().getColor(color));

            String[] path = sPath.split("\\|");
            for (int i = 0; i < path.length; i++) {
                Double lat = Double.parseDouble(path[i].split(",")[0]);
                Double lon = Double.parseDouble(path[i].split(",")[1]);
                LatLng latLng = new LatLng(lat, lon);

                Marker m = mMap.addMarker(new MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(hue))
                        .title(title).draggable(false));

                builder.include(m.getPosition());
                po.add(m.getPosition());
            }

            mMap.addPolyline(po);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(po.getPoints().get(0).latitude, po.getPoints().get(0).longitude), DEFAULT_ZOOM));
        }
    }

    public void drawToPath(String sPath, String title, float hue, int color) {
        if (sPath != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            PolylineOptions po = new PolylineOptions().color(getResources().getColor(color));

            String[] path = sPath.split("\\|");
            this.path.clear();
            for (int i = 0; i < path.length; i++) {
                Double lat = Double.parseDouble(path[i].split(",")[0]);
                Double lon = Double.parseDouble(path[i].split(",")[1]);
                LatLng latLng = new LatLng(lat, lon);

                Marker m = mMap.addMarker(new MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(hue))
                        .title(title).draggable(true));
                m.setTag("path");

                this.path.add(m);

                builder.include(m.getPosition());
                po.add(m.getPosition());
            }

            if (polyline != null) polyline.remove();
            polyline = mMap.addPolyline(po);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(po.getPoints().get(0).latitude, po.getPoints().get(0).longitude), DEFAULT_ZOOM));
        }
    }

    private void drawPath(@ColorRes int color) {
        PolylineOptions po = new PolylineOptions().color(getResources().getColor(color));

        for (Marker ll: path) {
            po.add(ll.getPosition());
        }

        if (polyline != null) polyline.remove();
        polyline = mMap.addPolyline(po);
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        if (marker.getTag().equals("path")) {
            drawPath(color);
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }
}
