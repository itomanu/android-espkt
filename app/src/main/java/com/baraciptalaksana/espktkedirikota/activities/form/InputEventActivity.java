package com.baraciptalaksana.espktkedirikota.activities.form;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.Pengaduan;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InputEventActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.main_title) TextView title;
    @BindView(R.id.laporan_event_date) Button btDate;
    @BindView(R.id.laporan_event_time) Button btTime;
    @BindView(R.id.laporan_event_location) AppCompatEditText tvLocation;
    @BindView(R.id.laporan_event_desc) AppCompatEditText tvDesc;

    @BindView(R.id.laporan_person_details) View vDetails;
    @BindView(R.id.laporan_name) AppCompatEditText tvName;
    @BindView(R.id.laporan_birth_location) AppCompatEditText tvBirthLocation;
    @BindView(R.id.laporan_birthdate) Button btBirthDate;
    @BindView(R.id.radio_male) AppCompatRadioButton rbMale;
    @BindView(R.id.radio_female) AppCompatRadioButton rbFemale;
    @BindView(R.id.laporan_job) AppCompatEditText tvJob;
    @BindView(R.id.laporan_job_spnr) Button btJob;
    @BindView(R.id.laporan_religion) Button btReligion;
    @BindView(R.id.laporan_address) AppCompatEditText tvAddress;
    @BindView(R.id.laporan_phone) AppCompatEditText tvPhone;
    @BindView(R.id.laporan_email) AppCompatEditText tvEmail;

    @BindView(R.id.laporan_officer_visit_check) CheckBox cbOfficerVisit;
    @BindView(R.id.laporan_person_details_check) CheckBox cbPersonDetails;

    public static final int REQUEST_CODE = 51;

    private String date = null, time = null;
    Pengaduan pengaduan = new Pengaduan();
    AwesomeValidation validation;
    private boolean needPersonData = false, isOfficerVisit = false;
    private int selectedReligion = -1, selectedJob = -1;
    private boolean genderMale = true;
    private String birthdate = null;
    String[] religions, jobs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input_event);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Helpers.fixToolbarPosition(toolbar);

        title.setText(getString(R.string.title_input_event));

        addValidation(true);

        if (!Helpers.isEmptyPekerjaan(this)) {
            btJob.setVisibility(View.VISIBLE);
            tvJob.setVisibility(View.GONE);

            List<ResponsePekerjaan.Pekerjaan> data = Helpers.getPekerjaan(this).getData();
            jobs = new String[data.size()];
            for (int i = 0; i < jobs.length; i++) {
                jobs[i] = data.get(i).getPekerjaan();
            }
        }

        religions = getResources().getStringArray(R.array.religion);

        Pengaduan data = new Gson().fromJson(getIntent().getStringExtra("DATA"), Pengaduan.class);
        if (data != null && data.getEventDate() != null) {
            setData(data);

            if (data.getGender2() != null) {
                setPersonData(data);
            }
        }
    }

    private void addValidation(boolean init) {
        if (init) validation = new AwesomeValidation(ValidationStyle.BASIC);

        validation.addValidation(this, R.id.laporan_event_location_layout, Helpers.patternMinLength(3), R.string.error_name);

        // Custom Validation for button Date & Time
        Helpers.addMultilineValidation(validation, this, R.id.laporan_event_desc_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_event_date, getString(R.string.text_hint_date), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_event_time, getString(R.string.text_hint_time), R.string.error_required);
    }

    private void addPersonValidation() {
        validation.addValidation(this, R.id.laporan_name_layout, Helpers.patternMinLength(3), R.string.error_name);
        validation.addValidation(this, R.id.laporan_birth_location_layout, Helpers.patternMinLength(3), R.string.error_required);
        validation.addValidation(this, R.id.laporan_job_layout, Helpers.patternMinLength(3), R.string.error_required);
        validation.addValidation(this, R.id.laporan_phone_layout, Patterns.PHONE, R.string.error_phone);
        validation.addValidation(this, R.id.laporan_email_layout, Patterns.EMAIL_ADDRESS, R.string.error_email);

        // Custom Validation for multiline Address & button Birth Date & Religion
        Helpers.addMultilineValidation(validation, this, R.id.laporan_address_layout, Helpers.patternMinLength(3), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_birthdate, getString(R.string.text_hint_birthdate), R.string.error_required);
        Helpers.addButtonValidation(validation, this, R.id.laporan_religion, getString(R.string.text_hint_religion), R.string.error_required);
    }

    private void setData(Pengaduan data) {
        btDate.setText(data.getEventDate());
        btTime.setText(data.getEventTime());
        tvLocation.setText(data.getEventLocation());
        tvDesc.setText(data.getEventDesc());

        date = data.getEventDate();
        time = data.getEventTime();

        btDate.setTextColor(Color.BLACK);
        btTime.setTextColor(Color.BLACK);

        cbOfficerVisit.setChecked(data.getEventOfficerVisit());
    }

    private void setPersonData(Pengaduan data) {
        cbPersonDetails.setChecked(true);
        vDetails.setVisibility(View.VISIBLE);

        tvName.setText(data.getName2());
        tvBirthLocation.setText(data.getBirthplace2());
        tvAddress.setText(data.getAddress2());
        tvPhone.setText(data.getPhone2());
        tvEmail.setText(data.getEmail2());

        if (data.getGender2().equalsIgnoreCase("perempuan")) {
            rbFemale.setSelected(true);
        } else {
            rbMale.setSelected(true);
        }

        if (!Helpers.isEmptyString(data.getBirthdate2())) {
            btBirthDate.setText(data.getBirthdate2());
            birthdate = data.getBirthdate2();
            btBirthDate.setTextColor(Color.BLACK);
        }

        selectedReligion = Helpers.getIndexOf(religions, data.getReligion2());
        if (selectedReligion > 0) {
            btReligion.setText(religions[selectedReligion]);
            btReligion.setTextColor(Color.BLACK);
        }

        if (Helpers.isEmptyPekerjaan(this)) {
            tvJob.setText(data.getJob2());
        } else {
            selectedJob = Helpers.getIndexOf(jobs, data.getJob2());
            if (selectedJob > 0) btJob.setText(jobs[selectedJob]);
            else btJob.setText(data.getJob2());
            btJob.setTextColor(Color.BLACK);
        }
    }

    private String getJob() {
        return "" + (Helpers.isEmptyPekerjaan(this) ? tvJob.getText() : btJob.getText().toString().equals("Pekerjaan") ? "" : btJob.getText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @OnClick(R.id.laporan_btn_submit)
    public void onSubmit() {
        if (needPersonData) {
//            addPersonValidation();
        } else {
            addValidation(true);
        }

        if (validation.validate()) {
            pengaduan.setEventDate(""+btDate.getText());
            pengaduan.setEventTime(""+btTime.getText());
            pengaduan.setEventLocation(""+tvLocation.getText());
            pengaduan.setEventDesc(""+tvDesc.getText());
            pengaduan.setEventOfficerVisit(cbOfficerVisit.isChecked());

            if (needPersonData) {
                pengaduan.setName2("" + tvName.getText());
                pengaduan.setBirthplace2("" + tvBirthLocation.getText());
                pengaduan.setBirthdate2(birthdate);
                pengaduan.setGender2(genderMale ? "Laki-Laki" : "Perempuan");
                pengaduan.setJob2(getJob());
                if (selectedReligion >= 0) pengaduan.setReligion2(religions[selectedReligion]);
                pengaduan.setAddress2("" + tvAddress.getText());
                pengaduan.setPhone2("" + tvPhone.getText());
                pengaduan.setEmail2("" + tvEmail.getText());
            } else {
                pengaduan.setGender2(null);
            }

            Intent intent = new Intent();
            intent.putExtra("DATA", new Gson().toJson(pengaduan));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @OnClick(R.id.laporan_job_spnr)
    public void onClickJob(final Button button) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(jobs, selectedJob, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                button.setTextColor(Color.BLACK);
                button.setText(jobs[which]);
                selectedJob = which;
            }
        });
        builder.show();
    }

    @OnClick(R.id.laporan_event_date)
    public void onClickDate(Button button) {
        dialogDatePicker(button, date);
    }

    @OnClick(R.id.laporan_event_time)
    public void onClickTime(Button button) {
        dialogTimePicker(button);
    }

    @OnClick(R.id.laporan_person_details_check)
    public void onCheckPersonDetails(CheckBox checkBox) {
        needPersonData = checkBox.isChecked();
        vDetails.setVisibility(needPersonData ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.laporan_birthdate)
    public void onClickBirthdate(Button button) {
        dialogDatePicker(button, birthdate);
    }

    @OnClick(R.id.laporan_religion)
    public void onClickReligion(Button button) {
        showReligionChoiceDialog(button);
    }

    @OnClick({R.id.radio_male, R.id.radio_female})
    public void onClickMale(AppCompatRadioButton radioButton) {

        boolean checked = radioButton.isChecked();
        if (checked) {
            if (radioButton.getId() == R.id.radio_male) {
                genderMale = true;
            } else if (radioButton.getId() == R.id.radio_female) {
                genderMale = false;
            }
        }
    }

    private void dialogDatePicker(final Button bt, String sDate) {
        Calendar calendar = Helpers.getCalendar(sDate);

        Calendar cl = Calendar.getInstance();
        if (bt.getId() == R.id.laporan_birthdate) cl.add(Calendar.YEAR, -17);

        Helpers.dialogDatePicker(this, calendar, cl, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                String fDate = Helpers.getFormattedDate(year, monthOfYear, dayOfMonth);

                if (bt.getId() == R.id.laporan_birthdate) birthdate = fDate;
                if (bt.getId() == R.id.laporan_event_date) date = fDate;

                bt.setText(fDate);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Datepickerdialog");
    }

    private void dialogTimePicker(final Button bt) {
        Calendar calendar = Helpers.getCalendar(time);

        Helpers.dialogTimePicker(this, calendar, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                time = Helpers.getFormattedTime(hourOfDay, minute, second);

                bt.setText(time);
                bt.setTextColor(Color.BLACK);
            }
        }).show(getFragmentManager(), "Timepickerdialog");
    }

    private void showReligionChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(religions, selectedReligion, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(religions[which]);
                selectedReligion = which;
            }
        });
        builder.show();
    }
}
