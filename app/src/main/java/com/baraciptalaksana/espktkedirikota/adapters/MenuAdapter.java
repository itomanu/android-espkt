package com.baraciptalaksana.espktkedirikota.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.menu.AsingActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.KehilanganActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.KeramaianActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.KonsultasiActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.LalinActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.PendapatActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.PengaduanActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.SIMActivity;
import com.baraciptalaksana.espktkedirikota.activities.menu.SKCKActivity;
import com.baraciptalaksana.espktkedirikota.models.Pendapat;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
    public static int[][] menu = new int[][] {
        {R.string.title_menu_pengaduan, R.drawable.ic_menu_pengaduan},
        {R.string.title_menu_kehilangan, R.drawable.ic_menu_kehilangan},
        {R.string.title_menu_sim, R.drawable.ic_menu_sim},
        {R.string.title_menu_skck, R.drawable.ic_menu_skck},
        {R.string.title_menu_keramaian, R.drawable.ic_menu_keramaian},
        {R.string.title_menu_lalin, R.drawable.ic_menu_lalin},
        {R.string.title_menu_fos, R.drawable.ic_menu_pendapat},
        {R.string.title_menu_foreign, R.drawable.ic_menu_asing},
        {R.string.title_menu_konsul, R.drawable.ic_menu_konsul},
    };

    public static final int MENU_PENGADUAN_INDEX = 0;
    public static final int MENU_KEHILANGAN_INDEX = 1;
    public static final int MENU_SIM_INDEX = 2;
    public static final int MENU_SKCK_INDEX = 3;
    public static final int MENU_KERAMAIAN_INDEX = 4;
    public static final int MENU_LALIN_INDEX = 5;
    public static final int MENU_PENDAPAT_INDEX = 6;
    public static final int MENU_ASING_INDEX = 7;
    public static final int MENU_KONSULTASI_INDEX = 8;

    List<MenuItem> mData;

    public MenuAdapter() {
        this.mData = new ArrayList<>();

        for (int i = 0; i < menu.length; i++) {
            mData.add(new MenuItem(i, menu[i][0], menu[i][1]));
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuItem menu = mData.get(position);
        holder.setItem(menu);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.menu_icon) public ImageView ivIcon;
        @BindView(R.id.menu_title) public TextView tvTitle;
        MenuItem menu;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void setItem(MenuItem menu) {
            this.menu = menu;

            tvTitle.setText(menu.getTitle(ivIcon.getContext()));
            Picasso.get().load(menu.getIcon()).into(ivIcon);
        }

        private Intent setIntent(Context context, Class clazz) {
            return new Intent(context, clazz);
        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            Intent intent;

            switch (menu.getIndex()) {
                case MENU_PENGADUAN_INDEX:
                    intent = setIntent(context, PengaduanActivity.class);
                break;
                case MENU_KEHILANGAN_INDEX:
                    intent = setIntent(context, KehilanganActivity.class);
                break;
                case MENU_SIM_INDEX:
                    intent = setIntent(context, SIMActivity.class);
                    break;
                case MENU_KERAMAIAN_INDEX:
                    intent = setIntent(context, KeramaianActivity.class);
                    break;
                case MENU_LALIN_INDEX:
                    intent = setIntent(context, LalinActivity.class);
                    break;
                case MENU_PENDAPAT_INDEX:
                    intent = setIntent(context, PendapatActivity.class);
                    break;
                case MENU_KONSULTASI_INDEX:
                    intent = setIntent(context, KonsultasiActivity.class);
                    break;
                case MENU_SKCK_INDEX:
                    intent = setIntent(context, SKCKActivity.class);
                    break;
                case MENU_ASING_INDEX:
                    intent = setIntent(context, AsingActivity.class);
                    break;
                default:
                    intent = null;
            }

            if (intent != null) {
                intent.putExtra(Constants.KEY_TITLE, menu.getTitle(context));
                context.startActivity(intent);
            } else {
                Helpers.dialog(context, "Belum Tersedia",
                        "Mohon maaf menu belum tersedia").show();
            }
        }
    }

    class MenuItem {
        private int index;
        private @StringRes int title;
        private @DrawableRes int icon;

        public MenuItem(int index, int title, int icon) {
            this.index = index;
            this.title = title;
            this.icon = icon;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getTitle(Context context) {
            return context.getString(title);
        }

        public int getTitle() {
            return title;
        }

        public void setTitle(int title) {
            this.title = title;
        }

        public int getIcon() {
            return icon;
        }

        public void setIcon(int icon) {
            this.icon = icon;
        }
    }
}
