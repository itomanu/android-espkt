package com.baraciptalaksana.espktkedirikota.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.NewsDetailActivity;
import com.baraciptalaksana.espktkedirikota.models.News;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {
    List<News> mData;

    public NewsListAdapter() {
        this.mData = new ArrayList<>();
    }

    public void setData(List<News> mData) {
        this.mData = mData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_newslist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        News news = mData.get(position);
        holder.setItem(news);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.news_title) public TextView tvTitle;
        @BindView(R.id.news_date) public TextView tvDate;
        @BindView(R.id.news_views) public TextView tvViews;
        @BindView(R.id.news_photo) public ImageView ivPhoto;
        News news;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void setItem(News news) {
            this.news = news;
            Picasso.get().load(Helpers.getNewsImgUrlThumb(news.getThumbnail())).into(ivPhoto);
            tvTitle.setText(Helpers.toTitleCase(news.getJudul()));
            tvDate.setText(Helpers.getNewsDate(news.getCreatedAt()));
            tvViews.setText(""+news.getViewer());
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(new Intent(view.getContext(), NewsDetailActivity.class));
            intent.putExtra("NEWS", new Gson().toJson(news));
            view.getContext().startActivity(intent);
        }
    }
}
