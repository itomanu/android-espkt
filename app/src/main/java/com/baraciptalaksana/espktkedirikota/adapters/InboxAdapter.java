package com.baraciptalaksana.espktkedirikota.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseInbox;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {
    List<ResponseInbox.Inbox> mData;

    public InboxAdapter() {
        this.mData = new ArrayList<>();
    }

    public void setData(List<ResponseInbox.Inbox> mData) {
        this.mData = mData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inbox, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ResponseInbox.Inbox inbox = mData.get(position);
        holder.setItem(inbox);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.inbox_row_title) public TextView tvTitle;
        @BindView(R.id.inbox_row_date) public TextView tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void setItem(ResponseInbox.Inbox inbox) {
            tvTitle.setText(Helpers.toTitleCase(inbox.getJudul()));
            tvDate.setText(inbox.getTanggal());
        }

        @Override
        public void onClick(View view) {
            Log.d("ROW INBOX", "Item Clicked : " + getAdapterPosition());
        }
    }
}
