package com.baraciptalaksana.espktkedirikota.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.NewsDetailActivity;
import com.baraciptalaksana.espktkedirikota.activities.NewsListActivity;
import com.baraciptalaksana.espktkedirikota.models.News;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsHomeAdapter extends RecyclerView.Adapter<NewsHomeAdapter.ViewHolder> {
    List<News> mData;
    boolean hasMore = false;

    public NewsHomeAdapter() {
        this.mData = new ArrayList<>();
    }

    public void setData(List<News> mData, Context context) {
        if (mData.size() > 3) {
            hasMore = true;
            mData.add(News.createMore(context));
        }

        this.mData = mData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_newshome, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        News news = mData.get(position);
        if (hasMore)
            holder.setItem(news, position, (position == mData.size() - 1));
        else
            holder.setItem(news, position, false);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.news_title) public TextView tvTitle;
        @BindView(R.id.news_photo) public ImageView ivPhoto;
        @BindView(R.id.news_gradient) public ImageView ivGradient;
        boolean isMore;
        News news;

        int[] gradients = new int[]{
            R.drawable.bg_news_gradient_1,
            R.drawable.bg_news_gradient_2,
            R.drawable.bg_news_gradient_3,
            R.drawable.bg_news_gradient_4,
        };

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void setItem(News news, int pos, boolean isMore) {
            this.news = news;
            this.isMore = isMore;
            int dGr = gradients[pos % gradients.length];
            ivGradient.setImageResource(dGr);
            if (!isMore) {
                Picasso.get().load(Helpers.getNewsImgUrlThumb(news.getThumbnail())).into(ivPhoto);
            }
            tvTitle.setText(Helpers.toTitleCase(news.getJudul()));
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(new Intent(view.getContext(), NewsDetailActivity.class));

            if (isMore) {
                intent = new Intent(view.getContext(), NewsListActivity.class);
            } else {
                intent.putExtra("NEWS", new Gson().toJson(news));
            }

            view.getContext().startActivity(intent);
        }
    }
}
