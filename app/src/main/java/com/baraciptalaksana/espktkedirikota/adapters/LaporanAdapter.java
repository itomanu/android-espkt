package com.baraciptalaksana.espktkedirikota.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.LaporanDetailActivity;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePermohonan;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaporanAdapter extends RecyclerView.Adapter<LaporanAdapter.ViewHolder> {
    List<ResponsePermohonan.Permohonan> mData;

    public LaporanAdapter() {
        this.mData = new ArrayList<>();
    }

    public void setData(List<ResponsePermohonan.Permohonan> mData) {
        this.mData = mData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_laporan, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ResponsePermohonan.Permohonan permohonan = mData.get(position);
        holder.setItem(permohonan);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.laporan_row_title) public TextView tvTitle;
        @BindView(R.id.laporan_row_date) public TextView tvDate;
        ResponsePermohonan.Permohonan permohonan;
        int index = -1;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void setItem(ResponsePermohonan.Permohonan permohonan) {
            this.permohonan = permohonan;
            int i = new Random().nextInt(MenuAdapter.menu.length);
            String title = tvTitle.getContext().getString(MenuAdapter.menu[i][0]);

            index = i;
            tvTitle.setText(permohonan.getLayanan());
            tvDate.setText(Helpers.getPermohonanDate(permohonan.getCreatedAt()));
        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            Intent intent = new Intent(context, LaporanDetailActivity.class);
            intent.putExtra("TITLE", permohonan.getLayanan());
            intent.putExtra("DATA", new Gson().toJson(permohonan));
            context.startActivity(intent);
        }
    }
}
