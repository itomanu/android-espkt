package com.baraciptalaksana.espktkedirikota.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baraciptalaksana.espktkedirikota.R;

import butterknife.ButterKnife;

public class PolresAFragment extends Fragment {

    public PolresAFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_polres_a, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
