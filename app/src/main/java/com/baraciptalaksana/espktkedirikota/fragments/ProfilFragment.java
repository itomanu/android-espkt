package com.baraciptalaksana.espktkedirikota.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraciptalaksana.espktkedirikota.BuildConfig;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.activities.EditProfileActivity;
import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfilFragment extends Fragment {
    @BindView(R.id.profil_photo) CircularImageView ivPhoto;
    @BindView(R.id.profil_nik) TextView tvNik;
    @BindView(R.id.profil_name) TextView tvName;
    @BindView(R.id.profil_btn_edit) ImageView btnEdit;
    @BindView(R.id.profil_address) TextView tvAddress;
    @BindView(R.id.profil_phone) TextView tvPhone;
    @BindView(R.id.profil_email) TextView tvEmail;
    @BindView(R.id.profil_job) TextView tvJob;
    @BindView(R.id.profil_gender) TextView tvGender;
    @BindView(R.id.profil_birth) TextView tvBirth;
    @BindView(R.id.profil_religion) TextView tvReligion;
    @BindView(R.id.profil_version_numb) TextView tvVersionNumb;

    private Person person = new Person();

    public ProfilFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profil, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        getLoginData();
        tvVersionNumb.setText(BuildConfig.VERSION_NAME);
    }

    @OnClick({R.id.profil_btn_edit, R.id.profil_photo})
    public void onEdit() {
        Intent intent = new Intent(getContext(), EditProfileActivity.class);
        intent.putExtra("DATA", new Gson().toJson(person));
        intent.putExtra("TITLE", "Edit Profile");
        getActivity().startActivityForResult(intent, EditProfileActivity.REQUEST_CODE);
    }

    private void getLoginData() {
        if (Helpers.getLoginData(getContext()) != null) {
            ResponseLogin.Data loginData = Helpers.getLoginData(getContext());
            person = Helpers.getLoginDataInPerson(getContext());
            Helpers.mappingPersonView(person, tvNik, tvName, tvBirth, tvGender, tvJob, tvReligion, tvAddress, tvPhone, tvEmail);
            Helpers.setPhotoProfile(loginData.getFoto(), ivPhoto);
        }
    }

    public void renewLoginData() {
        getLoginData();
    }
}
