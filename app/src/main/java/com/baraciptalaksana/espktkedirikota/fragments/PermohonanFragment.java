package com.baraciptalaksana.espktkedirikota.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.adapters.LaporanAdapter;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePermohonan;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PermohonanFragment extends Fragment implements Constants {
    @BindView(R.id.permohonan_list) RecyclerView rvPermohonan;
    @BindView(R.id.permohonan_loading) RotateLoading rlLoading;
    @BindView(R.id.btn_reload) Button btReload;

    LaporanAdapter adapter = new LaporanAdapter();
    ResponseLogin.Data loginData;

    public PermohonanFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_permohonan, container, false);
        ButterKnife.bind(this, view);

        rvPermohonan.setHasFixedSize(true);
        rvPermohonan.setLayoutManager(new LinearLayoutManager(getContext()));
        rvPermohonan.addItemDecoration(new DividerItemDecoration(rvPermohonan.getContext(), DividerItemDecoration.VERTICAL));
        rvPermohonan.setAdapter(adapter);

        getLoginData();

        startLoading();

        return view;
    }

    private void startLoading() {
        if (rlLoading != null) {
            rlLoading.setVisibility(View.VISIBLE);
            rlLoading.start();
        }
        if (btReload != null) btReload.setVisibility(View.GONE);
        if (rvPermohonan != null) rvPermohonan.setVisibility(View.GONE);
    }

    private void stopLoading(boolean error) {
        if (error) showReload();
        else {
            if (rlLoading != null) {
                rlLoading.stop();
                rlLoading.setVisibility(View.GONE);
                rvPermohonan.setVisibility(View.VISIBLE);
                btReload.setVisibility(View.GONE);
            }
        }
    }

    private void showReload() {
        if (btReload != null) btReload.setVisibility(View.VISIBLE);
        if (rlLoading != null) {
            rlLoading.stop();
            rlLoading.setVisibility(View.GONE);
        }
    }

    private void setView() {
        if (rlLoading != null && adapter.getItemCount() > 0) stopLoading(false);
        else showReload();
    }

    public void scrollToTop() {
        rvPermohonan.smoothScrollToPosition(0);
    }

    public void getLoginData() {
        if (Helpers.getLoginData(getContext()) != null) {
            loginData = Helpers.getLoginData(getContext());
        }
    }

    @OnClick(R.id.btn_reload)
    public void getData() {

        if (loginData != null) {
            startLoading();

            Api.Factory.getInstance().getPermohonan(""+loginData.getIdUser()).enqueue(new Callback<ResponsePermohonan>() {
                @Override
                public void onResponse(Call<ResponsePermohonan> call, Response<ResponsePermohonan> response) {
                    stopLoading(true);

                    if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                        if (response.body().getData() != null) {
                            adapter.setData(response.body().getData());
                            adapter.notifyDataSetChanged();
                            setView();
                        } else {
                            Helpers.toastErrorNetwork(getActivity());
                        }
                    } else {
                        Helpers.toastErrorNetwork(getActivity());
                    }
                }

                @Override
                public void onFailure(Call<ResponsePermohonan> call, Throwable t) {
                    Helpers.toastErrorNetwork(getActivity());
                    stopLoading(true);
                }
            });
        }
    }
}
