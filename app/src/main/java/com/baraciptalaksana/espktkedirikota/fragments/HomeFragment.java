package com.baraciptalaksana.espktkedirikota.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.adapters.MenuAdapter;
import com.baraciptalaksana.espktkedirikota.adapters.NewsHomeAdapter;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseNews;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.baraciptalaksana.espktkedirikota.utils.StartSnapHelper;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements Constants {
    @BindView(R.id.home_menulist) RecyclerView rvMenu;
    @BindView(R.id.home_newslist) RecyclerView rvNews;
    @BindView(R.id.home_loading) RotateLoading rlLoading;
    @BindView(R.id.btn_reload) Button btReload;

    MenuAdapter menuAdapter;
    NewsHomeAdapter newsAdapter;

    public HomeFragment() {
        menuAdapter = new MenuAdapter();
        newsAdapter = new NewsHomeAdapter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        rvMenu.setHasFixedSize(true);
        rvMenu.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvMenu.setAdapter(menuAdapter);

        rvNews.setHasFixedSize(true);
        rvNews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false ));
        rvNews.setAdapter(newsAdapter);

        startLoading();

        return view;
    }

    private void startLoading() {
        if (rlLoading != null) {
            rlLoading.setVisibility(View.VISIBLE);
            rlLoading.start();
        }
        if (btReload != null) btReload.setVisibility(View.GONE);
        if (rvNews != null) rvNews.setVisibility(View.GONE);
    }

    private void stopLoading(boolean error) {
        if (error) showReload();
        else {
            if (rlLoading != null) {
                rlLoading.stop();
                rlLoading.setVisibility(View.GONE);
                rvNews.setVisibility(View.VISIBLE);
                btReload.setVisibility(View.GONE);
            }
        }
    }

    private void showReload() {
        if (btReload != null) btReload.setVisibility(View.VISIBLE);
        if (rlLoading != null) {
            rlLoading.stop();
            rlLoading.setVisibility(View.GONE);
        }
    }

    private void setView() {
        if (rlLoading != null && newsAdapter.getItemCount() > 0) {
            stopLoading(false);

            SnapHelper snapHelper = new StartSnapHelper();
            rvNews.setOnFlingListener(null);
            snapHelper.attachToRecyclerView(rvNews);
        } else showReload();
    }

    public void scrollToTop() {
        rvNews.smoothScrollToPosition(0);
    }

    @OnClick(R.id.btn_reload)
    public void getData() {
        startLoading();

        Api.Factory.getInstance().getHomeNews().enqueue(new Callback<ResponseNews>() {
            @Override
            public void onResponse(Call<ResponseNews> call, Response<ResponseNews> response) {
                if (getContext() != null && response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(SUCCESS) && response.body().getData() != null) {
                        newsAdapter.setData(response.body().getData(), getContext());
                        newsAdapter.notifyDataSetChanged();
                        setView();
                    } else if (response.body().getStatus().equals(ERROR) && response.body().getMessage() != null) {
                        Helpers.toastInfo(getActivity(), response.body().getMessage());
                        stopLoading(true);
                    }
                } else {
                    Helpers.toastErrorNetwork(getActivity());
                    stopLoading(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseNews> call, Throwable t) {
                Helpers.toastErrorNetwork(getActivity());
                stopLoading(true);
            }

        });
    }
}
