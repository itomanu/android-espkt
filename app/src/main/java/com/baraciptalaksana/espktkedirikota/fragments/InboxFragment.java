package com.baraciptalaksana.espktkedirikota.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.adapters.InboxAdapter;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseInbox;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.victor.loading.rotate.RotateLoading;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InboxFragment extends Fragment implements Constants {
    @BindView(R.id.inbox_list) RecyclerView rvInbox;
    @BindView(R.id.inbox_loading) RotateLoading rlLoading;
    @BindView(R.id.btn_reload) Button btReload;

    InboxAdapter adapter = new InboxAdapter();

    public InboxFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        ButterKnife.bind(this, view);

        rvInbox.setHasFixedSize(true);
        rvInbox.setLayoutManager(new LinearLayoutManager(getContext()));
        rvInbox.addItemDecoration(new DividerItemDecoration(rvInbox.getContext(), DividerItemDecoration.VERTICAL));
        rvInbox.setAdapter(adapter);

        startLoading();

        setView();

        return view;
    }

    private void startLoading() {
        if (rlLoading != null) {
            rlLoading.setVisibility(View.VISIBLE);
            rlLoading.start();
        }
        if (btReload != null) btReload.setVisibility(View.GONE);
        if (rvInbox != null) rvInbox.setVisibility(View.GONE);
    }

    private void stopLoading(boolean error) {
        if (error) showReload();
        else {
            if (rlLoading != null) {
                rlLoading.stop();
                rlLoading.setVisibility(View.GONE);
                rvInbox.setVisibility(View.VISIBLE);
                btReload.setVisibility(View.GONE);
            }
        }
    }

    private void showReload() {
        if (btReload != null) btReload.setVisibility(View.VISIBLE);
        if (rlLoading != null) {
            rlLoading.stop();
            rlLoading.setVisibility(View.GONE);
        }
    }

    private void setView() {
        if (rlLoading != null && adapter.getItemCount() > 0) stopLoading(false);
        else showReload();
    }

    public void scrollToTop() {
        rvInbox.smoothScrollToPosition(0);
    }

    @OnClick(R.id.btn_reload)
    public void getData() {
        startLoading();

        Api.Factory.getInstance().getInboxes().enqueue(new Callback<ResponseInbox>() {
            @Override
            public void onResponse(Call<ResponseInbox> call, Response<ResponseInbox> response) {
                stopLoading(true);

                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    if (response.body().getData() != null) {
                        adapter.setData(response.body().getData());
                        adapter.notifyDataSetChanged();
                        setView();
                    } else {
                        Helpers.toastErrorNetwork(getActivity());
                    }
                } else {
                    Helpers.toastErrorNetwork(getActivity());
                }
            }

            @Override
            public void onFailure(Call<ResponseInbox> call, Throwable t) {
                Helpers.toastErrorNetwork(getActivity());
                stopLoading(true);
            }
        });
    }
}
