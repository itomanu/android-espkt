package com.baraciptalaksana.espktkedirikota.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baraciptalaksana.espktkedirikota.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PolresFragment extends Fragment {
    @BindView(R.id.polres_container) ViewPager viewPager;
    @BindView(R.id.polres_tab_layout) TabLayout tabLayout;

    public PolresFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_polres, container, false);
        ButterKnife.bind(this, view);

        SectionsPagerAdapter pagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        String[] titles = new String[] { "Hubungi Kami", "Tentang Polres" };

        PolresAFragment fragmentA;
        PolresBFragment fragmentB;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentA = new PolresAFragment();
            fragmentB = new PolresBFragment();
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: return fragmentA;
                case 1: return fragmentB;
            }
            return fragmentB;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
