package com.baraciptalaksana.espktkedirikota.networks.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePermohonan {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Permohonan> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Permohonan> getData() {
        return data;
    }

    public void setData(List<Permohonan> data) {
        this.data = data;
    }

    public class Permohonan {
        @SerializedName("kode")
        @Expose
        private String kode;

        @SerializedName("layanan")
        @Expose
        private String layanan;

        @SerializedName("created_at")
        @Expose
        private String createdAt;

        @SerializedName("ref_id")
        @Expose
        private Integer refId;

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public String getLayanan() {
            return layanan;
        }

        public void setLayanan(String layanan) {
            this.layanan = layanan;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getRefId() {
            return refId;
        }

        public void setRefId(Integer refId) {
            this.refId = refId;
        }
    }
}
