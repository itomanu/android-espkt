package com.baraciptalaksana.espktkedirikota.networks.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseLogin {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Data {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("id_user")
        @Expose
        private int idUser;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("tlp")
        @Expose
        private String tlp;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("agama")
        @Expose
        private String agama;
        @SerializedName("tempat_lahir")
        @Expose
        private String tempatLahir;
        @SerializedName("tgl_lahir")
        @Expose
        private String tglLahir;
        @SerializedName("jenis_kelamin")
        @Expose
        private String jenisKelamin;
        @SerializedName("pekerjaan")
        @Expose
        private String pekerjaan;
        @SerializedName("no_ktp")
        @Expose
        private String noKtp;
        @SerializedName("no_sim")
        @Expose
        private String noSim;
        @SerializedName("scan_ktp")
        @Expose
        private String scanKtp;
        @SerializedName("scan_sim")
        @Expose
        private String scanSim;
        @SerializedName("foto")
        @Expose
        private String foto;
        @SerializedName("valid")
        @Expose
        private int valid;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIdUser() {
            return idUser;
        }

        public void setIdUser(int idUser) {
            this.idUser = idUser;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getTlp() {
            return tlp;
        }

        public void setTlp(String tlp) {
            this.tlp = tlp;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAgama() {
            return agama;
        }

        public void setAgama(String agama) {
            this.agama = agama;
        }

        public String getTempatLahir() {
            return tempatLahir;
        }

        public void setTempatLahir(String tempatLahir) {
            this.tempatLahir = tempatLahir;
        }

        public String getTglLahir() {
            return tglLahir;
        }

        public void setTglLahir(String tglLahir) {
            this.tglLahir = tglLahir;
        }

        public String getJenisKelamin() {
            return jenisKelamin;
        }

        public void setJenisKelamin(String jenisKelamin) {
            this.jenisKelamin = jenisKelamin;
        }

        public String getPekerjaan() {
            return pekerjaan;
        }

        public void setPekerjaan(String pekerjaan) {
            this.pekerjaan = pekerjaan;
        }

        public String getNoKtp() {
            return noKtp;
        }

        public void setNoKtp(String noKtp) {
            this.noKtp = noKtp;
        }

        public String getNoSim() {
            return noSim;
        }

        public void setNoSim(String noSim) {
            this.noSim = noSim;
        }

        public String getScanKtp() {
            return scanKtp;
        }

        public void setScanKtp(String scanKtp) {
            this.scanKtp = scanKtp;
        }

        public String getScanSim() {
            return scanSim;
        }

        public void setScanSim(String scanSim) {
            this.scanSim = scanSim;
        }

        public String getFoto() {
            return foto;
        }

        public void setFoto(String foto) {
            this.foto = foto;
        }

        public int getValid() {
            return valid;
        }

        public void setValid(int valid) {
            this.valid = valid;
        }

    }
}
