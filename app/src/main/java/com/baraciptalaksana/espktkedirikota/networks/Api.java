package com.baraciptalaksana.espktkedirikota.networks;

import com.baraciptalaksana.espktkedirikota.networks.models.RequestLogin;
import com.baraciptalaksana.espktkedirikota.networks.models.RequestRegister;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseAppVersion;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBase;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseBaseForm;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseInbox;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseNews;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseNewsDetail;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePermohonan;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePermohonanDetail;
import com.baraciptalaksana.espktkedirikota.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {

    @GET("/api/get_inbox/1")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseInbox> getInboxes();

    // News ----------------------------------------------------------------------------------------
    @GET("/api/list_berita_all_espkt/1")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseNews> getNews();

    @GET("/api/list_berita_espkt/1")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseNews> getHomeNews();

    @GET("/api/detail_berita_espkt/{id}")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseNewsDetail> getDetailNews(@Path("id") String id);
    // End of News ---------------------------------------------------------------------------------

    // User ----------------------------------------------------------------------------------------
    @POST("/api/auth/user/login")
    Call<ResponseLogin> doLogin(@Body RequestLogin request);

    @POST("/api/auth/user/update")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseLogin> doEditProfile(@Body RequestBody request);

    @POST("/api/auth/user/forgot_password")
    Call<ResponseBase> doResetPassword(@Body RequestBody request);

    @POST("/api/auth/user/signup")
    Call<ResponseBase> doSignUp(@Body RequestRegister request);
    // End of User ---------------------------------------------------------------------------------

    // Form ----------------------------------------------------------------------------------------
    @POST("/api/submit_laporan_pengaduan")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitPengaduan(@Body RequestBody request);

    @POST("/api/submit_laporan_kehilangan")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitKehilangan(@Body RequestBody request);

    @POST("/api/submit_permohonan_sim")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitSIM(@Body RequestBody request);

    @POST("/api/submit_izin_keramaian")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitKeramaian(@Body RequestBody request);

    @POST("/api/submit_izin_demo")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitPendapat(@Body RequestBody request);

    @POST("/api/submit_permohonan_konsultasi")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitKonsultasi(@Body RequestBody request);

    @POST("/api/submit_izin_penutupan")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitLalin(@Body RequestBody request);

    @POST("/api/submit_permohonan_skck")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitSKCK(@Body RequestBody request);

    @POST("/api/submit_orang_asing")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseBaseForm> doSubmitAsing(@Body RequestBody request);
    // End of Form ---------------------------------------------------------------------------------

    // Updater -------------------------------------------------------------------------------------
    @GET("/api/versi_espkt/espkt")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponseAppVersion> getAppVersion();
    // End of Updater ------------------------------------------------------------------------------

    // Permohonan ----------------------------------------------------------------------------------
    @GET("/api/list_permohonan/{user}/1")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponsePermohonan> getPermohonan(@Path("user") String user);

    @GET("/api/detail_permohonan/{kode}/{id}")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponsePermohonanDetail> getPermohonanDetail(@Path("kode") String kode, @Path("id") String id);
    // End of Permohonan  --------------------------------------------------------------------------

    // Permohonan ----------------------------------------------------------------------------------
    @GET("/api/list_pekerjaan")
    @Headers({ "Authorization: Bearer "+ Constants.TOKEN_KEY })
    Call<ResponsePekerjaan> getPekerjaan();
    // End of Permohonan ---------------------------------------------------------------------------


    class Factory implements Constants {
        public static Api service;

        public static Api getInstance() {
            if (service == null) {
                Gson gson = new GsonBuilder().create();

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

                logging.setLevel(HttpLoggingInterceptor.Level.BODY);

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

                httpClient.addInterceptor(logging);

                Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson)).build();

                service = retrofit.create(Api.class);
            }

            return service;
        }
    }
}
