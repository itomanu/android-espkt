package com.baraciptalaksana.espktkedirikota.networks.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePekerjaan {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Pekerjaan> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Pekerjaan> getData() {
        return data;
    }

    public void setData(List<Pekerjaan> data) {
        this.data = data;
    }

    public class Pekerjaan {
        @SerializedName("pekerjaan")
        @Expose
        private String pekerjaan;

        public String getPekerjaan() {
            return pekerjaan;
        }

        public void setPekerjaan(String pekerjaan) {
            this.pekerjaan = pekerjaan;
        }
    }
}
