package com.baraciptalaksana.espktkedirikota.networks.models;

import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MultipartBody;

public class RequestEditProfile {
    @SerializedName("id_user")
    @Expose
    private int idUser;
    @SerializedName("no_ktp")
    @Expose
    private String noKtp;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("password_new")
    @Expose
    private String passwordNew;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("pekerjaan")
    @Expose
    private String pekerjaan;
    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;
    @SerializedName("tgl_lahir")
    @Expose
    private String tglLahir;
    @SerializedName("agama")
    @Expose
    private String agama;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("alamat")
    @Expose
    private String alamat;

    public RequestEditProfile(int idUser, Person person) {
        this.idUser = idUser;
        noKtp = person.getNik();
        nama = person.getName();
        telp = person.getPhone();
        email = person.getEmail();
        pekerjaan = person.getJob();
        tempatLahir = person.getBirthplace();
        tglLahir = Helpers.toServerDate(person.getBirthdate());
        agama = person.getReligion();
        jenisKelamin = person.getGender();
        alamat = person.getAddress();
    }

    public MultipartBody.Builder toMultipartBody() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("id_user", ""+idUser);
        builder.addFormDataPart("no_ktp", noKtp);
        builder.addFormDataPart("nama", nama);
        builder.addFormDataPart("telp", telp);
        builder.addFormDataPart("email", email);
        builder.addFormDataPart("pekerjaan", pekerjaan);
        builder.addFormDataPart("tempat_lahir", tempatLahir);
        builder.addFormDataPart("tgl_lahir", tglLahir);
        builder.addFormDataPart("agama", agama);
        builder.addFormDataPart("jenis_kelamin", jenisKelamin);
        builder.addFormDataPart("alamat", alamat);
        builder.addFormDataPart("password", password);

        return builder;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordNew() {
        return passwordNew;
    }

    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

}
