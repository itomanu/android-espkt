package com.baraciptalaksana.espktkedirikota.networks.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePermohonanDetail {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Detail data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Detail getData() {
        return data;
    }

    public void setData(Detail data) {
        this.data = data;
    }

    public class Detail {
        @SerializedName("tgl_dtg")
        @Expose
        private String tglDtg;

        @SerializedName("waktu_dtg")
        @Expose
        private String waktuDtg;

        @SerializedName("kode")
        @Expose
        private String kode;

        @SerializedName("nama")
        @Expose
        private String nama;

        @SerializedName("nik")
        @Expose
        private String nik;

        @SerializedName("alamat")
        @Expose
        private String alamat;

        @SerializedName("telp")
        @Expose
        private String telp;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("detail")
        @Expose
        private String detail;

        @SerializedName("detail_asing")
        @Expose
        private Asing detailAsing;

        @SerializedName("detail_sim")
        @Expose
        private Sim detailSim;

        public String getTglDtg() {
            return tglDtg;
        }

        public void setTglDtg(String tglDtg) {
            this.tglDtg = tglDtg;
        }

        public String getWaktuDtg() {
            return waktuDtg;
        }

        public void setWaktuDtg(String waktuDtg) {
            this.waktuDtg = waktuDtg;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNik() {
            return nik;
        }

        public void setNik(String nik) {
            this.nik = nik;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getTelp() {
            return telp;
        }

        public void setTelp(String telp) {
            this.telp = telp;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public Asing getDetailAsing() {
            return detailAsing;
        }

        public void setDetailAsing(Asing detailAsing) {
            this.detailAsing = detailAsing;
        }

        public Sim getDetailSim() {
            return detailSim;
        }

        public void setDetailSim(Sim detailSim) {
            this.detailSim = detailSim;
        }
    }

    public class Asing {
        @SerializedName("nama")
        @Expose
        private String nama;


        @SerializedName("alamat")
        @Expose
        private String alamat;


        @SerializedName("kebangsaan")
        @Expose
        private String kebangsaan;

        @SerializedName("ttl")
        @Expose
        private String ttl;

        @SerializedName("pekerjaan")
        @Expose
        private String pekerjaan;

        @SerializedName("no_passport")
        @Expose
        private String noPassport;

        @SerializedName("dikeluarkan")
        @Expose
        private String dikeluarkan;

        @SerializedName("visa")
        @Expose
        private String visa;

        @SerializedName("tgl_ked")
        @Expose
        private String tglKed;

        @SerializedName("dari")
        @Expose
        private String dari;

        @SerializedName("maksud")
        @Expose
        private String maksud;

        @SerializedName("tlg_brgkt")
        @Expose
        private String tlgBerangkat;

        @SerializedName("tujuan_sel")
        @Expose
        private String tujuanSelanjutnya;

        @SerializedName("negara_sel")
        @Expose
        private String negaraSelanjutnya;

        @SerializedName("kota_ind")
        @Expose
        private String kota;

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getKebangsaan() {
            return kebangsaan;
        }

        public void setKebangsaan(String kebangsaan) {
            this.kebangsaan = kebangsaan;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }

        public String getPekerjaan() {
            return pekerjaan;
        }

        public void setPekerjaan(String pekerjaan) {
            this.pekerjaan = pekerjaan;
        }

        public String getNoPassport() {
            return noPassport;
        }

        public void setNoPassport(String noPassport) {
            this.noPassport = noPassport;
        }

        public String getDikeluarkan() {
            return dikeluarkan;
        }

        public void setDikeluarkan(String dikeluarkan) {
            this.dikeluarkan = dikeluarkan;
        }

        public String getVisa() {
            return visa;
        }

        public void setVisa(String visa) {
            this.visa = visa;
        }

        public String getTglKed() {
            return tglKed;
        }

        public void setTglKed(String tglKed) {
            this.tglKed = tglKed;
        }

        public String getDari() {
            return dari;
        }

        public void setDari(String dari) {
            this.dari = dari;
        }

        public String getMaksud() {
            return maksud;
        }

        public void setMaksud(String maksud) {
            this.maksud = maksud;
        }

        public String getTlgBerangkat() {
            return tlgBerangkat;
        }

        public void setTlgBerangkat(String tlgBerangkat) {
            this.tlgBerangkat = tlgBerangkat;
        }

        public String getTujuanSelanjutnya() {
            return tujuanSelanjutnya;
        }

        public void setTujuanSelanjutnya(String tujuanSelanjutnya) {
            this.tujuanSelanjutnya = tujuanSelanjutnya;
        }

        public String getNegaraSelanjutnya() {
            return negaraSelanjutnya;
        }

        public void setNegaraSelanjutnya(String negaraSelanjutnya) {
            this.negaraSelanjutnya = negaraSelanjutnya;
        }

        public String getKota() {
            return kota;
        }

        public void setKota(String kota) {
            this.kota = kota;
        }
    }

    public class Sim {
        @SerializedName("jenis")
        @Expose
        private String jenis;

        @SerializedName("golongan")
        @Expose
        private String golongan;

        @SerializedName("no")
        @Expose
        private String no;

        public String getJenis() {
            return jenis;
        }

        public void setJenis(String jenis) {
            this.jenis = jenis;
        }

        public String getGolongan() {
            return golongan;
        }

        public void setGolongan(String golongan) {
            this.golongan = golongan;
        }

        public String getNo() {
            return no;
        }

        public void setNo(String no) {
            this.no = no;
        }
    }
}
