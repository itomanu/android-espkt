package com.baraciptalaksana.espktkedirikota.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.google.firebase.analytics.FirebaseAnalytics;

public class Analytics {
    private Activity mActivity;
    private FirebaseAnalytics mFirebaseAnalytics;

    public Analytics(Activity activity) {
        this.mActivity = activity;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
    }

    public void openMain() {
        Context context = (Context) mActivity;
        String id = "unregistered", email = "unregistered";
        ResponseLogin.Data loginData = Helpers.getLoginData(context);

        if (loginData != null) {
            if (!Helpers.isEmptyString(""+loginData.getIdUser())) id = ""+loginData.getIdUser();
            if (!Helpers.isEmptyString(""+loginData.getIdUser())) email = ""+loginData.getEmail();
        }

        openMain(id, email);
    }

    public void openMain(String id, String email) {
        Bundle bundle = new Bundle();
        bundle.putString(Param.USER_ID, id);
        bundle.putString(Param.USER_EMAIL, email);
        mFirebaseAnalytics.logEvent(Event.MAIN_OPEN, bundle);
    }

    class Event {
        public static final String MAIN_OPEN = "main_open";
    }

    class Param {
        public static final String USER_ID = "user_id";
        public static final String USER_EMAIL = "user_email";
    }
}
