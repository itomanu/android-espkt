package com.baraciptalaksana.espktkedirikota.utils;

public interface Constants {
    String APP_CITY = "Kediri Kota";
    String APP_NAME = "Polres " + APP_CITY;
    String APP_PROV = "Jawa Timur";
    String APP_COUNTRY = "Indonesia";

    String SUCCESS = "success";
    String ERROR = "error";

    String SIBLING_SEPARATOR = "<#>";
    String GOOGLE_STATIC_MAP_BASE_URL = "https://maps.googleapis.com/maps/api/staticmap";
    String GOOGLE_MAP_API_KEY = "AIzaSyCk0zYRqJ9TPyWRN5ypqedo_8lthB1cgqE";
//    String BASE_URL = "http://icc.membara.net";
//    String BASE_URL_IMAGE = "http://icc.membara.net/file/";
    String BASE_URL = "https://icc.polreskedirikota.net";
    String BASE_URL_IMAGE = "https://icc.polreskedirikota.net/file/";
    String TOKEN_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRoXC9hbmdnb3RhXC9sb2dpbiIsImlhdCI6MTUyNTUzOTAxMCwiZXhwIjoxNTI1NTQyNjEwLCJuYmYiOjE1MjU1MzkwMTAsImp0aSI6IlN5MHVSWUJIM25IUGVSV2ciLCJzdWIiOjI4MjgyODI4LCJwcnYiOiJjNjljMGZhYjk2NGQzMzdjODk1MjA0ZDdlZDJiMTUyZGU2YzBlMzc3In0.DapBdriKUOdoZrSE1ySlqzqzz9NfgZifBFTTXCUwwgE";

    // IMG DIR
    String IMG_DIR_MASYARAKAT = "/masyarakat/";
    String IMG_DIR_BERITA = "/berita/";

    String KEY_TITLE = "TITLE";

    // FORM COMMON
    String FORM_ID_USER = "id_user";
    String FORM_FOTO = "foto";
    String FORM_NO_KTP = "no_ktp";
    String FORM_NAMA = "nama";
    String FORM_TELP = "telp";
    String FORM_EMAIL = "email";
    String FORM_PEKERJAAN = "pekerjaan";
    String FORM_TEMPAT_LAHIR = "tempat_lahir";
    String FORM_TGL_LAHIR = "tgl_lahir";
    String FORM_AGAMA = "agama";
    String FORM_JENIS_KELAMIN = "jenis_kelamin";
    String FORM_ALAMAT = "alamat";

    String FORM_TGL_DTG = "tgl_dtg";
    String FORM_WAKTU_DTG = "waktu_dtg";
    String FORM_TINJAU = "tinjau";

    // FORM PENGADUAN
    String FORM_TEMPAT_KEJADIAN = "tempat_kejadian";
    String FORM_TGL_KEJADIAN = "tgl_kejadian";
    String FORM_WAKTU_KEJADIAN = "waktu_kejadian";
    String FORM_URAIAN = "uraian";

    String FORM_NAMA_TERLAPOR = "nama_terlapor";
    String FORM_TELP_TERLAPOR = "telp_terlapor";
    String FORM_EMAIL_TERLAPOR = "email_terlapor";
    String FORM_PEKERJAAN_TERLAPOR = "pekerjaan_terlapor";
    String FORM_TEMPAT_LAHIR_TERLAPOR = "tempat_lahir_terlapor";
    String FORM_TGL_LAHIR_TERLAPOR = "tgl_lahir_terlapor";
    String FORM_AGAMA_TERLAPOR = "agama_terlapor";
    String FORM_JENIS_KELAMIN_TERLAPOR = "jenis_kelamin_terlapor";
    String FORM_ALAMAT_TERLAPOR = "alamat_terlapor";

    // FORM SIM
    String FORM_JENIS = "jenis_permohonan";
    String FORM_GOL_SIM = "golongan_sim";
    String FORM_PENDIDIDKAN = "pendidikan";
    String FORM_SERTIFIKAT = "sertifikat_mengemudi";
    String FORM_TINGGI = "tinggi_badan";
    String FORM_GOL_DARAH = "golongan_darah";
    String FORM_KACAMATA = "berkacamata";
    String FORM_CACAT = "cacat";
    String FORM_NAMA_DARURAT = "nama_darurat";
    String FORM_TELP_DARURAT = "telp_darurat";
    String FORM_ALAMAT_DARURAT = "alamat_darurat";

    // FORM KERAMAIAN
    String FORM_NAMA_INSTANSI = "nama_instansi";
    String FORM_TELP_PANITIA = "telp_panitia";
    String FORM_TGL_KEGIATAN = "tgl_kegiatan";
    String FORM_WAKTU_KEGIATAN = "waktu_kegiatan";
    String FORM_DURASI = "durasi";
    String FORM_KEGIATAN = "kegiatan";
    String FORM_PESERTA = "peserta";
    String FORM_RANGKAIAN = "rangkaian";

    // FORM PENDAPAT
    String FORM_NAMA_PEN_JAWAB = "nama_penanggung_jwb";
    String FORM_TELP_PEN_JAWAB = "telp_penanggung_jwb";
    String FORM_LOKASI = "lokasi";
    String FORM_WAKTU_MULAI = "waktu_mulai";
    String FORM_WAKTU_SELESAI = "waktu_selesai";
    String FORM_TITIK_KUMPUL = "titik_kumpul";
    String FORM_KORLAP_1 = "korlap_1";
    String FORM_KORLAP_2 = "korlap_2";
    String FORM_RUTE = "rute";
    String FORM_PERAGA = "peraga";
    String FORM_AGENDA = "agenda";

    // FORM LALIN
    String FORM_TGL_TUTUP = "tgl_tutup";
    String FORM_WAKTU_TUTUP = "waktu_tutup";
    String FORM_JALAN_TUTUP = "jalan_tutup";
    String FORM_JALAN_TUTUP_PATH = "jalan_tutup_path";
    String FORM_JALAN_ALTERNATIF = "jalan_alternatif";
    String FORM_JALAN_ALTERNATIF_PATH = "jalan_alternatif_path";

    // FORM SKCK
    String FORM_STATUS_NIKAH = "status_nikah";
    String FORM_NO_PASSPORT = "no_passport";
    String FORM_NO_KITAS_KITAP = "no_kitas_kitap";

    String FORM_NAMA_PASANGAN = "nama_pasangan";
    String FORM_UMUR_PASANGAN = "umur_pasangan";
    String FORM_AGAMA_PASANGAN = "agama_pasangan";
    String FORM_KEBANGSAAN_PASANGAN = "kebangsaan_pasangan";
    String FORM_PEKERJAAN_PASANGAN = "pekerjaan_pasangan";
    String FORM_ALAMAT_PASANGAN = "alamat_pasangan";

    String FORM_NAMA_AYAH = "nama_ayah";
    String FORM_UMUR_AYAH = "umur_ayah";
    String FORM_AGAMA_AYAH = "agama_ayah";
    String FORM_KEBANGSAAN_AYAH = "kebangsaan_ayah";
    String FORM_PEKERJAAN_AYAH = "pekerjaan_ayah";
    String FORM_ALAMAT_AYAH = "alamat_ayah";

    String FORM_NAMA_IBU = "nama_ibu";
    String FORM_UMUR_IBU = "umur_ibu";
    String FORM_AGAMA_IBU = "agama_ibu";
    String FORM_KEBANGSAAN_IBU = "kebangsaan_ibu";
    String FORM_PEKERJAAN_IBU = "pekerjaan_ibu";
    String FORM_ALAMAT_IBU = "alamat_ibu";

    String FORM_NAMA_SAUDARA = "nama_saudara[]";
    String FORM_UMUR_SAUDARA = "umur_saudara[]";
    String FORM_PEKERJAAN_SAUDARA = "pekerjaan_saudara[]";
    String FORM_ALAMAT_SAUDARA = "alamat_saudara[]";
    
    String FORM_PERNAH_DIPIDANA = "pernah_dipidana";
    String FORM_PERKARA = "perkara";
    String FORM_VONIS = "vonis";
    String FORM_SEDANG_DIPIDANA = "sedang_dipidana";
    String FORM_PROSES_HUKUM = "proses_hukum";
    String FORM_PERNAH_PELANGGARAN = "pernah_pelanggaran";
    String FORM_PELANGGARAN = "pelanggaran";
    String FORM_PROSES_PELANGGARAN = "proses_pelanggaran";
    String FORM_RIWAYAT = "riwayat";
    String FORM_HOBI = "hobi";
    String FORM_NAMA_SPONSOR = "nama_sponsor";
    String FORM_JENIS_USAHA = "jenis_usaha";
    String FORM_TELP_ORG_ASING = "telp_org_asing";
    String FORM_ALAMAT_ORG_ASING = "alamat_org_asing";

    // FORM ASING
    String FORM_NAMA_OA = "nama_oa";
    String FORM_TEMPAT_LAHIR_OA = "tempat_lahir_oa";
    String FORM_TGL_LAHIR_OA = "tgl_lahir_oa";
    String FORM_KEBANGSAAN_OA = "kebangsaan_oa";
    String FORM_PEKERJAAN_OA = "pekerjaan_oa";
    String FORM_ALAMAT_OA = "alamat_oa";
    String FORM_OLEH = "oleh";
    String FORM_VISA = "visa";
    String FORM_TGL_KEDATANGAN = "tgl_kedatangan";
    String FORM_NEGARA = "negara";
    String FORM_MAKSUD_DATANG = "maksud_datang";
    String FORM_TGL_KEBERANGKATAN = "tgl_keberangkatan";
    String FORM_TUJUAN_SELANJUTNYA = "tujuan_selanjutnya";
    String FORM_NEGARA_SELANJUTNYA = "negara_selanjutnya";
    String FORM_KOTA_INDONESIA = "kota_indonesia";
}
