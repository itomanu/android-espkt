package com.baraciptalaksana.espktkedirikota.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponsePekerjaan;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationHolder;
import com.basgeekball.awesomevalidation.utility.custom.CustomErrorReset;
import com.basgeekball.awesomevalidation.utility.custom.CustomValidation;
import com.basgeekball.awesomevalidation.utility.custom.CustomValidationCallback;
import com.easyandroidanimations.library.Animation;
import com.easyandroidanimations.library.BounceAnimation;
import com.baraciptalaksana.espktkedirikota.R;
import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class Helpers {
    public static boolean isEmptyString(String input) {
        return input == null || input.equals("") || input.trim().equals("") || input.equals("null");
    }

    public static boolean isEmptyStringSpecial(String input) {
        return isEmptyString(input) || input.trim().equals("-");
    }

    public static boolean isEmptyString(AppCompatEditText editText) {
        return editText == null || isEmptyString(""+editText.getText());
    }

    public static void setText(TextView txView, String text) {
        txView.setText(isEmptyString(text) ? "-" : text);
    }

    public static void setText(TextView txView, String separator, String... text) {
        String out = isEmptyString(text[0]) ? "-" : text[0];

        for (int i = 1; i < text.length; i++) {
            out += separator + (isEmptyString(text[i]) ? "-" : text[i]);
        }

        txView.setText(out);
    }

    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public static String toCapitalize(String input) {
        return Character.toUpperCase(input.charAt(0)) + input.substring(1);
    }

    public static int getIndexOf(String [] arrs, String value) {
        return arrs == null || arrs.length == 0 ? -1 : Arrays.asList(arrs).indexOf(value);
    }

    public static String saveString(String input) {
        return isEmptyString(input) ? "" : input;
    }

    public static String saveString(String input, String output) {
        return isEmptyString(input) ? "" : output;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void fixToolbarPosition(Toolbar toolbar) {
        toolbar.setPadding(0, Helpers.getStatusBarHeight(toolbar.getContext()), 0, 0);
    }

    public static void fixToolbarPosition(Toolbar toolbar, Context context) {
        toolbar.setPadding(0, Helpers.getStatusBarHeight(context), 0, 0);
    }

    public static void fixTitlePosition(TextView textView) {
        textView.setPadding(dpToPx(textView.getContext(), 20), 0, 0, 0);
    }

    public static void fixTitlePosition(TextView textView, int dp) {
        textView.setPadding(0, 0, dpToPx(textView.getContext(), dp), 0);
    }

    public static int dpToPx(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density + 0.5f);
    }

    public static String patternMinLength(int length) {
        return ".{" + length + ",}";
    }

    public static void addValidation(AwesomeValidation validation, Activity activity, int textInputLayoutId, final String regex, int stringErrMesagge) {
        validation.addValidation(activity, textInputLayoutId, new CustomValidation() {
            @Override
            public boolean compare(ValidationHolder validationHolder) {
                TextInputLayout view = (TextInputLayout) validationHolder.getView();
                return Pattern.compile(regex).matcher(view.getEditText().getText()).matches();
            }
        }, new CustomValidationCallback() {
            @Override
            public void execute(ValidationHolder validationHolder) {
                TextInputLayout view = (TextInputLayout) validationHolder.getView();
                view.setError(validationHolder.getErrMsg());
                view.requestFocus();
            }
        }, new CustomErrorReset() {
            @Override
            public void reset(ValidationHolder validationHolder) {
                TextInputLayout view = (TextInputLayout) validationHolder.getView();
                view.setError(null);
            }
        }, stringErrMesagge);
    }

    public static void addMultilineValidation(AwesomeValidation validation, Activity activity, int textInputLayoutId, final String regex, int stringErrMesagge) {
        validation.addValidation(activity, textInputLayoutId, new CustomValidation() {
            @Override
            public boolean compare(ValidationHolder validationHolder) {
                TextInputLayout view = (TextInputLayout) validationHolder.getView();
                EditText editText = view.getEditText();
                String trim = editText.getText().toString().replaceAll("\n", "");
                return Pattern.compile(regex).matcher(trim).matches();
            }
        }, new CustomValidationCallback() {
            @Override
            public void execute(ValidationHolder validationHolder) {
                TextInputLayout view = (TextInputLayout) validationHolder.getView();
                view.getEditText().setError(validationHolder.getErrMsg());
                view.getEditText().requestFocus();
            }
        }, new CustomErrorReset() {
            @Override
            public void reset(ValidationHolder validationHolder) {
                TextInputLayout view = (TextInputLayout) validationHolder.getView();
                view.getEditText().setError(null);
            }
        }, stringErrMesagge);
    }

    public static void addButtonValidation(AwesomeValidation validation, Activity activity, int buttonId, final String compare, int stringErrMesagge) {
        validation.addValidation(activity, buttonId, new CustomValidation() {
            @Override
            public boolean compare(ValidationHolder validationHolder) {
                Button view = (Button) validationHolder.getView();
                return !view.getText().toString().equalsIgnoreCase(compare);
            }
        }, new CustomValidationCallback() {
            @Override
            public void execute(ValidationHolder validationHolder) {
                Button view = (Button) validationHolder.getView();
                view.setError(validationHolder.getErrMsg());
                view.requestFocus();
            }
        }, new CustomErrorReset() {
            @Override
            public void reset(ValidationHolder validationHolder) {
                Button view = (Button) validationHolder.getView();
                view.setError(null);
            }
        }, stringErrMesagge);
    }

    public static void scrollToView(final NestedScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    public static void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    public static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar;
    }

    public static Calendar getCalendar(int year, int monthOfYear, int dayOfMonth, boolean dateType) {
        Calendar calendar = Calendar.getInstance();
        if (dateType) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, year);
            calendar.set(Calendar.MINUTE, monthOfYear);
            calendar.set(Calendar.SECOND, dayOfMonth);
        }

        return calendar;
    }

    public static Calendar getCalendar(String formattedDate) {
        if (formattedDate != null && Helpers.getDateFromFormattedString(formattedDate) != null) {
            return getCalendar(Helpers.getDateFromFormattedString(formattedDate));
        } else {
            return Calendar.getInstance();
        }
    }

    public static String getFormattedDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Helpers.getCalendar(year, monthOfYear, dayOfMonth, true);
        return Helpers.getFormattedDate(calendar.getTimeInMillis());
    }

    public static String getFormattedDate(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
        return newFormat.format(new Date(dateTime));
    }

    public static Date getDateFromFormattedString(String date) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return newFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date getDateFromFormattedString(String date, String pattern) {
        SimpleDateFormat newFormat = new SimpleDateFormat(pattern);
        try {
            return newFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String fullDateFormat(Date date) {
        Format formatter = new SimpleDateFormat("EEEE, d MMMM yyyy", new Locale("in","ID"));
        String day = formatter.format(date);
        return day;
    }

    public static String fullDateFormatNoDay(Date date) {
        Format formatter = new SimpleDateFormat("d MMMM yyyy", new Locale("in","ID"));
        String day = formatter.format(date);
        return day;
    }

    public static String fullDateFormatWithTime(Date date) {
        Format formatter = new SimpleDateFormat("d MMMM yyyy - HH:mm", new Locale("in","ID"));
        String day = formatter.format(date);
        return day;
    }

    public static String getNewsDate(String date) {
        Date d = Helpers.getDateFromFormattedString(date, "dd MMM yyyy - HH:mm:ss");
        return d == null ? "" : Helpers.fullDateFormatNoDay(d);
    }

    public static String getPermohonanDate(String date) {
        Date d = Helpers.getDateFromFormattedString(date, "dd MMM yyyy - HH:mm:ss");
        return d == null ? "" : Helpers.fullDateFormatWithTime(d);
    }

    public static String getPermohonanDetailDate(String date) {
        Date d = Helpers.getDateFromFormattedString(date, "yyyy-MM-dd");
        return d == null ? "" : Helpers.fullDateFormat(d);
    }

    public static String getFormattedTime(int hourOfDay, int minute, int second) {
        Calendar calendar = Helpers.getCalendar(hourOfDay, minute, second, false);
        return Helpers.getFormattedTime(calendar.getTimeInMillis());
    }

    public static String getFormattedTime(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm");
        return newFormat.format(new Date(dateTime));
    }

    public static Date getTimeFromFormattedString(String date) {
        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm");
        try {
            return newFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String fixServerDate(String serverDate) {
        String[] split = serverDate.split("-");

        return split[2] + "/" + split[1] + "/" + split[0];
    }

    public static String toServerDate(String systemDate) {
        if (systemDate == null) return "";
        String[] split = systemDate.split("/");
        return split == null || split.length < 3 ? "" : split[2] + "-" + split[1] + "-" + split[0];
    }

    public static String toServerTime(String systemTime) {
        return systemTime+":00";
    }

    public static void shakeView(View view) {
        new BounceAnimation(view).setBounceDistance(10).setNumOfBounces(3).setDuration(Animation.DURATION_SHORT).animate();
    }

    public static boolean arrayContains(boolean[] arr, boolean check) {
        for (boolean value : arr) {
            if (value == check) {
                return true;
            }
        }
        return false;
    }

    public static void toastError(Activity activity, String message) {
        if (activity != null) Toasty.error(activity, message, Toast.LENGTH_LONG, true).show();
    }

    public static void toastSuccess(Activity activity, String message) {
        if (activity != null) Toasty.success(activity, message, Toast.LENGTH_LONG, true).show();
    }

    public static void toastInfo(Activity activity, String message) {
        if (activity != null) Toasty.info(activity, message, Toast.LENGTH_SHORT, true).show();
    }

    public static void toastErrorNetwork(Activity activity) {
        if (activity != null) Toasty.error(activity, activity.getString(R.string.dialog_error_unknown_network_message), Toast.LENGTH_SHORT, true).show();
    }

    public static AlertDialog dialog(Context context, @StringRes int title, @StringRes int message) {
        return dialog(context, title, message, R.string.text_ok);
    }

    public static AlertDialog dialog(Context context, @StringRes int title, @StringRes int message,
                                     @StringRes int positiveText) {
        return dialog(context, title, message, positiveText, null);
    }

    public static AlertDialog dialog(Context context, @StringRes int title, @StringRes int message,
                                     DialogInterface.OnClickListener positiveListener) {
        return dialog(context, title, message, R.string.text_ok, positiveListener);
    }

    public static AlertDialog dialog(Context context, @StringRes int title, @StringRes int message, @StringRes int positiveText,
                                     DialogInterface.OnClickListener positiveListener) {
        return dialog(context, title, message, positiveText, positiveListener, -1, null);
    }

    public static AlertDialog dialog(Context context, @StringRes int title, @StringRes int message,
                                     DialogInterface.OnClickListener positiveListener,
                                     DialogInterface.OnClickListener negativeListener) {
        return dialog(context, context.getString(title), context.getString(message),
                context.getString(R.string.text_yes), positiveListener,
                context.getString(R.string.text_no), negativeListener);
    }

    public static AlertDialog dialog(Context context, @StringRes int title, @StringRes int message,
                                     @StringRes int positiveText, DialogInterface.OnClickListener positiveListener,
                                     @StringRes int negativeText, DialogInterface.OnClickListener negativeListener) {
        return dialog(context, context.getString(title), context.getString(message),
                context.getString(positiveText), positiveListener,
                negativeText == -1 ? null : context.getString(negativeText), negativeListener);
    }

    public static AlertDialog dialog(Context context, String title, String message) {
        return dialog(context, title, message, context.getString(R.string.text_ok));
    }

    public static AlertDialog dialog(Context context, String title, String message,
                                     String positiveText) {
        return dialog(context, title, message, positiveText, null);
    }

    public static AlertDialog dialog(Context context, String title, String message,
                                     DialogInterface.OnClickListener positiveListener) {
        return dialog(context, title, message, context.getString(R.string.text_ok), positiveListener);
    }

    public static AlertDialog dialog(Context context, String title, String message,
                                     String positiveText, DialogInterface.OnClickListener positiveListener) {
        return dialog(context, title, message, positiveText, positiveListener, null, null);
    }

    public static AlertDialog dialog(Context context, String title, String message,
                                     DialogInterface.OnClickListener positiveListener,
                                     DialogInterface.OnClickListener negativeListener) {
        return dialog(context, title, message, context.getString(R.string.text_yes), positiveListener,
                context.getString(R.string.text_no), negativeListener);
    }

    public static AlertDialog dialog(Context context, String title, String message,
                                     String positiveText, DialogInterface.OnClickListener positiveListener,
                                     String negativeText, DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        return builder.setTitle(title).setMessage(message)
                .setPositiveButton(positiveText, positiveListener)
                .setNegativeButton(negativeText, negativeListener).create();
    }

    public static DatePickerDialog dialogDatePicker(Context context, Calendar calendar, OnDateSetListener onDateSetListener) {
        return dialogDatePicker(context, calendar, null, null, R.color.colorAccent, onDateSetListener);
    }

    public static DatePickerDialog dialogDatePicker(Context context, Calendar calendar, Calendar maxDate, OnDateSetListener onDateSetListener) {
        return dialogDatePicker(context, calendar, maxDate, null, R.color.colorAccent, onDateSetListener);
    }

    public static DatePickerDialog dialogDatePicker(Context context, Calendar calendar,
                                                    Calendar maxDate, Calendar minDate,
                                                    OnDateSetListener onDateSetListener) {
        return dialogDatePicker(context, calendar, maxDate, minDate, R.color.colorAccent, onDateSetListener);
    }

    public static DatePickerDialog dialogDatePicker(Context context, Calendar calendar,
                                                    Calendar maxDate, Calendar minDate,
                                                    @ColorRes int color, OnDateSetListener onDateSetListener) {
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        datePicker.setThemeDark(false);
        if (maxDate != null) datePicker.setMaxDate(maxDate);
        if (minDate != null) datePicker.setMinDate(minDate);
        datePicker.setAccentColor(context.getResources().getColor(color));

        return datePicker;
    }

    public static TimePickerDialog dialogTimePicker(Context context, Calendar calendar, OnTimeSetListener onTimeSetListener) {
        return dialogTimePicker(context, calendar, null, null, onTimeSetListener);
    }

    public static TimePickerDialog dialogTimePicker(Context context, Calendar calendar, Timepoint maxTime, Timepoint minTime, OnTimeSetListener onTimeSetListener) {
        return dialogTimePicker(context, calendar, maxTime, minTime, R.color.colorAccent, onTimeSetListener);
    }

    public static TimePickerDialog dialogTimePicker(Context context, Calendar calendar, Timepoint maxTime, Timepoint minTime,
                                                    @ColorRes int color, OnTimeSetListener onTimeSetListener) {
        TimePickerDialog timePicker = TimePickerDialog.newInstance(
                onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND), true
        );

        if (maxTime != null) timePicker.setMaxTime(maxTime);
        if (minTime != null) timePicker.setMinTime(minTime);

        timePicker.setThemeDark(false);
        timePicker.setAccentColor(context.getResources().getColor(color));

        return timePicker;
    }

    public static String getMAPImageURL(String paths, String color, String colorPath) {
        String url = Constants.GOOGLE_STATIC_MAP_BASE_URL + "?" +
                "&scale=2&size=500x300" +
                "&markers=color:" + color + "|" + paths +
                "&path=color:0x" + colorPath + "80|weight:5|" + paths +
                "&key=" + Constants.GOOGLE_MAP_API_KEY;

        return url;
    }

    public static String getMAPImageURL(String center, String... paths) {
        return getMAPImageURL(center, "16", "300x300", "roadmap", paths);
    }

    public static String getMAPImageURL(String center, String zoom, String size, String mapType, String... paths) {
        String path = "";

        if (paths.length > 0) {
            path = paths[0];
            if (paths.length > 1) {
                for (int i = 1; i < paths.length; i++) {
                    path += "|" + paths[i];
                }
            }
        }

        return Constants.GOOGLE_STATIC_MAP_BASE_URL + "?" +
                "center=" + center +
                "&scale=2&zoom=" + zoom +
                "&size=" + size +
                "&maptype=" + mapType +
                "&path=color:0x0000ff80|weight:5|" + path +
                "&key=" + Constants.GOOGLE_MAP_API_KEY;
    }

    public static String resizeAndCompressImage(Context context, String filePath, String fileName) {
        final int MAX_IMAGE_SIZE = 700 * 1024; // max final file size in kilobytes

        // First decode with inJustDecodeBounds=true to check dimensions of image
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize(First we are going to resize the image to 800x800 image, in order to not have a big but very low quality image.
        //resizing the image will already reduce the file size, but after resizing we will check the file size and start to compress image
        options.inSampleSize = calculateInSampleSize(options, 800, 800);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inPreferredConfig= Bitmap.Config.ARGB_8888;

        Bitmap bmpPic = BitmapFactory.decodeFile(filePath,options);

        int compressQuality = 100; // quality decreasing by 5 every loop.
        int streamLength;
        do{
            ByteArrayOutputStream bmpStream = new ByteArrayOutputStream();
            Log.d("compressBitmap", "Quality: " + compressQuality);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream);
            byte[] bmpPicByteArray = bmpStream.toByteArray();
            streamLength = bmpPicByteArray.length;
            compressQuality -= 5;
            Log.d("compressBitmap", "Size: " + streamLength/1024+" kb");
        }while (streamLength >= MAX_IMAGE_SIZE);

        try {
            //save the resized and compressed file to disk cache
            Log.d("compressBitmap","cacheDir: "+context.getCacheDir());
            FileOutputStream bmpFile = new FileOutputStream(context.getCacheDir()+fileName);
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile);
            bmpFile.flush();
            bmpFile.close();
        } catch (Exception e) {
            Log.e("compressBitmap", "Error on saving file");
        }
        //return the path of resized and compressed file
        return  context.getCacheDir()+fileName;
    }



    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        String debugTag = "MemoryInformation";
        // Image nin islenmeden onceki genislik ve yuksekligi
        final int height = options.outHeight;
        final int width = options.outWidth;
        Log.d(debugTag,"image height: "+height+ "---image width: "+ width);
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        Log.d(debugTag,"inSampleSize: "+inSampleSize);
        return inSampleSize;
    }

    // Preferences
    public static ResponseLogin.Data getLoginData(Context context) {
        String data = Preferences.getInstance(context).getString(Preferences.LOGIN_DATA);
        if (!isEmptyString(data)) {
            return new Gson().fromJson(data, ResponseLogin.Data.class);
        } else return null;
    }

    public static Person getLoginDataInPerson(Context context) {
        ResponseLogin.Data loginData = getLoginData(context);
        return Person.mappingPerson(loginData);
    }

    public static void setLoginData(Context context, ResponseLogin.Data data) {
        Preferences.getInstance(context).putString(Preferences.LOGIN_DATA, new Gson().toJson(data, data.getClass()));
    }

    public static void resetLoginData(Context context) {
        Preferences.getInstance(context).putString(Preferences.LOGIN_DATA, "");
    }

    public static void setPhotoProfile(String link, ImageView ivPhoto) {
        if (!isEmptyString(link)) {
            String url = Helpers.getUserImgUrl(link);
            Picasso.get().load(url).placeholder(R.drawable.no_user_placeholder).into(ivPhoto);
        }
    }

    public static ResponsePekerjaan getPekerjaan(Context context) {
        String data = Preferences.getInstance(context).getString(Preferences.PEKERJAAN_DATA);
        if (!isEmptyString(data)) {
            return new Gson().fromJson(data, ResponsePekerjaan.class);
        } else return null;
    }

    public static void setPekerjaan(Context context, ResponsePekerjaan data) {
        Preferences.getInstance(context).putString(Preferences.PEKERJAAN_DATA, new Gson().toJson(data, data.getClass()));
    }

    public static boolean isEmptyPekerjaan(Context context) {
        return  getPekerjaan(context) == null
                || getPekerjaan(context).getData() == null
                || getPekerjaan(context).getData().isEmpty();
    }

    // URL IMG
    public static String getNewsImgUrl(String file, String type) {
        return Constants.BASE_URL_IMAGE + type + Constants.IMG_DIR_BERITA + file;
    }

    public static String getNewsImgUrlThumb(String file) {
        return getNewsImgUrl(file, "thumbnail");
    }

    public static String getNewsImgUrlFoto(String file) {
        return getNewsImgUrl(file, "foto");
    }

    public static String getUserImgUrl(String file, String type) {
        return Constants.BASE_URL_IMAGE + type + Constants.IMG_DIR_MASYARAKAT + file;
    }

    public static String getUserImgUrlThumb(String file) {
        return getUserImgUrl(file, "thumbnail");
    }

    public static String getUserImgUrl(String file) {
        return getUserImgUrl(file, "foto");
    }

    // Mapping
    public static void mappingPersonView(ResponseLogin.Data data,
             TextView nik,
             TextView name,
             TextView birth,
             TextView gender,
             TextView job,
             TextView religion,
             TextView address,
             TextView phone,
             TextView email) {

        mappingPersonView(Person.mappingPerson(data), nik, name, birth, gender, job, religion, address, phone, email);
    }

    public static void mappingPersonView(Person data,
             TextView nik,
             TextView name,
             TextView birth,
             TextView gender,
             TextView job,
             TextView religion,
             TextView address,
             TextView phone,
             TextView email) {

        nik.setText(data.getNik());
        address.setText(data.getAddress());
        email.setText(data.getEmail());
        job.setText(data.getJob());
        name.setText(data.getName());
        phone.setText(data.getPhone());
        gender.setText(data.getGender());
        birth.setText(data.getBirthplace() + ", "+ data.getBirthdate());
        religion.setText(data.getReligion());
    }
}
