package com.baraciptalaksana.espktkedirikota.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.StringRes;

import com.baraciptalaksana.espktkedirikota.BuildConfig;
import com.baraciptalaksana.espktkedirikota.networks.Api;
import com.baraciptalaksana.espktkedirikota.networks.models.ResponseAppVersion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Updater implements Constants {
    private static final String APP_LINK = "https://play.google.com/store/apps/details?id=com.baraciptalaksana.icckedirikota";

    private Activity mActivity;
    private AlertDialog.Builder dialog;
    private String link;
    private int button;

    public Updater(Activity mActivity) {
        this.mActivity = mActivity;
        this.dialog = new AlertDialog.Builder(mActivity);
    }

    public Updater setTitleOnUpdateAvailable(@StringRes int title) {
        dialog.setTitle(title);
        return this;
    }

    public Updater setContentOnUpdateAvailable(@StringRes int content) {
        dialog.setMessage(content);
        return this;
    }

    public Updater setButtonUpdate(@StringRes int button) {
        this.button = button;
        dialog.setPositiveButton(this.button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent browse = new Intent(Intent.ACTION_VIEW, Uri.parse(getLink()) );
                mActivity.startActivity(browse);
            }
        });

        return this;
    }

    public String getLink() {
        return Helpers.isEmptyString(link) ? APP_LINK : link;
    }

    public void setLink(String link) {
        this.link = link;
        setButtonUpdate(this.button);
    }

    private void getVersion() {
        Api.Factory.getInstance().getAppVersion().enqueue(new Callback<ResponseAppVersion>() {
            @Override
            public void onResponse(Call<ResponseAppVersion> call, Response<ResponseAppVersion> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus().equals(SUCCESS)) {
                    if (response.body().getData().getCode() > BuildConfig.VERSION_CODE) {
                        setLink(response.body().getData().getLink());
                        dialog.show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAppVersion> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void start() {
        getVersion();
    }
}
