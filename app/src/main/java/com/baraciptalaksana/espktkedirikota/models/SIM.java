package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SIM extends FormLaporan {

    // Event
    @SerializedName("simKind") @Expose private String simKind;
    @SerializedName("simType") @Expose private String simType;
    @SerializedName("simBlood") @Expose private String simBlood;
    @SerializedName("simCert") @Expose private String simCert;
    @SerializedName("simDisabled") @Expose private String simDisabled;
    @SerializedName("simGlasses") @Expose private String simGlasses;
    @SerializedName("simEdu") @Expose private String simEdu;
    @SerializedName("simHeight") @Expose private String simHeight;
    @SerializedName("simCountry") @Expose private String simCountry;
    @SerializedName("simNational") @Expose private String simNational;
    @SerializedName("simProvince") @Expose private String simProvince;
    @SerializedName("simEmerName") @Expose private String simEmerName;
    @SerializedName("simEmerPhone") @Expose private String simEmerPhone;
    @SerializedName("simEmerAddress") @Expose private String simEmerAddress;

    public String getSimKind() {
        return simKind;
    }

    public void setSimKind(String simKind) {
        this.simKind = simKind;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    public String getSimBlood() {
        return simBlood;
    }

    public void setSimBlood(String simBlood) {
        this.simBlood = simBlood;
    }

    public String getSimCert() {
        return simCert;
    }

    public void setSimCert(String simCert) {
        this.simCert = simCert;
    }

    public String getSimDisabled() {
        return simDisabled;
    }

    public void setSimDisabled(String simDisabled) {
        this.simDisabled = simDisabled;
    }

    public String getSimEdu() {
        return simEdu;
    }

    public void setSimEdu(String simEdu) {
        this.simEdu = simEdu;
    }

    public String getSimCountry() {
        return simCountry;
    }

    public void setSimCountry(String simCountry) {
        this.simCountry = simCountry;
    }

    public String getSimNational() {
        return simNational;
    }

    public void setSimNational(String simNational) {
        this.simNational = simNational;
    }

    public String getSimProvince() {
        return simProvince;
    }

    public void setSimProvince(String simProvince) {
        this.simProvince = simProvince;
    }

    public String getSimEmerName() {
        return simEmerName;
    }

    public void setSimEmerName(String simEmerName) {
        this.simEmerName = simEmerName;
    }

    public String getSimEmerPhone() {
        return simEmerPhone;
    }

    public void setSimEmerPhone(String simEmerPhone) {
        this.simEmerPhone = simEmerPhone;
    }

    public String getSimEmerAddress() {
        return simEmerAddress;
    }

    public void setSimEmerAddress(String simEmerAddress) {
        this.simEmerAddress = simEmerAddress;
    }

    public String getSimGlasses() {
        return simGlasses;
    }

    public void setSimGlasses(String simGlasses) {
        this.simGlasses = simGlasses;
    }

    public String getSimHeight() {
        return simHeight;
    }

    public void setSimHeight(String simHeight) {
        this.simHeight = simHeight;
    }
}
