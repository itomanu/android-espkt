package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pendapat extends FormLaporan {

    // Event
    @SerializedName("pendapatName") @Expose private String pendapatName;
    @SerializedName("pendapatPhone") @Expose private String pendapatPhone;
    @SerializedName("pendapatDate") @Expose private String pendapatDate;
    @SerializedName("pendapatTimeStart") @Expose private String pendapatTimeStart;
    @SerializedName("pendapatTimeEnd") @Expose private String pendapatTimeEnd;
    @SerializedName("pendapatType") @Expose private String pendapatType;
    @SerializedName("pendapatCapacities") @Expose private String pendapatCapacities;
    @SerializedName("pendapatLocation") @Expose private String pendapatLocation;
    @SerializedName("pendapatRoute") @Expose private String pendapatRoute;
    @SerializedName("pendapatAgenda") @Expose private String pendapatAgenda;
    @SerializedName("pendapatProps") @Expose private String pendapatProps;
    @SerializedName("pendapatNameP") @Expose private String pendapatNameP;
    @SerializedName("pendapatMeetingP") @Expose private String pendapatMeetingPoint;
    @SerializedName("pendapatKorlap1") @Expose private String pendapatKorlap1;
    @SerializedName("pendapatKorlap2") @Expose private String pendapatKorlap2;

    public String getPendapatName() {
        return pendapatName;
    }

    public void setPendapatName(String pendapatName) {
        this.pendapatName = pendapatName;
    }

    public String getPendapatPhone() {
        return pendapatPhone;
    }

    public void setPendapatPhone(String pendapatPhone) {
        this.pendapatPhone = pendapatPhone;
    }

    public String getPendapatDate() {
        return pendapatDate;
    }

    public void setPendapatDate(String pendapatDate) {
        this.pendapatDate = pendapatDate;
    }

    public String getPendapatTimeStart() {
        return pendapatTimeStart;
    }

    public void setPendapatTimeStart(String pendapatTimeStart) {
        this.pendapatTimeStart = pendapatTimeStart;
    }

    public String getPendapatTimeEnd() {
        return pendapatTimeEnd;
    }

    public void setPendapatTimeEnd(String pendapatTimeEnd) {
        this.pendapatTimeEnd = pendapatTimeEnd;
    }

    public String getPendapatType() {
        return pendapatType;
    }

    public void setPendapatType(String pendapatType) {
        this.pendapatType = pendapatType;
    }

    public String getPendapatCapacities() {
        return pendapatCapacities;
    }

    public void setPendapatCapacities(String pendapatCapacities) {
        this.pendapatCapacities = pendapatCapacities;
    }

    public String getPendapatLocation() {
        return pendapatLocation;
    }

    public void setPendapatLocation(String pendapatLocation) {
        this.pendapatLocation = pendapatLocation;
    }

    public String getPendapatRoute() {
        return pendapatRoute;
    }

    public void setPendapatRoute(String pendapatRoute) {
        this.pendapatRoute = pendapatRoute;
    }

    public String getPendapatAgenda() {
        return pendapatAgenda;
    }

    public void setPendapatAgenda(String pendapatAgenda) {
        this.pendapatAgenda = pendapatAgenda;
    }

    public String getPendapatProps() {
        return pendapatProps;
    }

    public void setPendapatProps(String pendapatProps) {
        this.pendapatProps = pendapatProps;
    }

    public String getPendapatNameP() {
        return pendapatNameP;
    }

    public void setPendapatNameP(String pendapatNameP) {
        this.pendapatNameP = pendapatNameP;
    }

    public String getPendapatMeetingPoint() {
        return pendapatMeetingPoint;
    }

    public void setPendapatMeetingPoint(String pendapatMeetingPoint) {
        this.pendapatMeetingPoint = pendapatMeetingPoint;
    }

    public String getPendapatKorlap1() {
        return pendapatKorlap1;
    }

    public void setPendapatKorlap1(String pendapatKorlap1) {
        this.pendapatKorlap1 = pendapatKorlap1;
    }

    public String getPendapatKorlap2() {
        return pendapatKorlap2;
    }

    public void setPendapatKorlap2(String pendapatKorlap2) {
        this.pendapatKorlap2 = pendapatKorlap2;
    }
}
