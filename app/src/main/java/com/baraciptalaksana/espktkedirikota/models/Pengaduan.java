package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pengaduan extends FormLaporan {

    // Event
    @SerializedName("eventDate") @Expose private String eventDate;
    @SerializedName("eventTime") @Expose private String eventTime;
    @SerializedName("eventLocation") @Expose private String eventLocation;
    @SerializedName("eventDesc") @Expose private String eventDesc;
    @SerializedName("eventOfficerVisit") @Expose private boolean eventOfficerVisit;

    // Person 2
    @SerializedName("name_2") @Expose private String name2;
    @SerializedName("birthplace_2") @Expose private String birthplace2;
    @SerializedName("birthdate_2") @Expose private String birthdate2;
    @SerializedName("gender_2") @Expose private String gender2;
    @SerializedName("job_2") @Expose private String job2;
    @SerializedName("religion_2") @Expose private String religion2;
    @SerializedName("address_2") @Expose private String address2;
    @SerializedName("phone_2") @Expose private String phone2;
    @SerializedName("email_2") @Expose private String email2;

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public boolean getEventOfficerVisit() {
        return eventOfficerVisit;
    }

    public void setEventOfficerVisit(boolean eventOfficerVisit) {
        this.eventOfficerVisit = eventOfficerVisit;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getBirthplace2() {
        return birthplace2;
    }

    public void setBirthplace2(String birthplace2) {
        this.birthplace2 = birthplace2;
    }

    public String getBirthdate2() {
        return birthdate2;
    }

    public void setBirthdate2(String birthdate2) {
        this.birthdate2 = birthdate2;
    }

    public String getGender2() {
        return gender2;
    }

    public String getGender2Fix() {
        return ""+gender2.charAt(0);
    }

    public void setGender2(String gender2) {
        this.gender2 = gender2;
    }

    public String getJob2() {
        return job2;
    }

    public void setJob2(String job2) {
        this.job2 = job2;
    }

    public String getReligion2() {
        return religion2;
    }

    public void setReligion2(String religion2) {
        this.religion2 = religion2;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }
}
