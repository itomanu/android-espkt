package com.baraciptalaksana.espktkedirikota.models.intents;

import com.baraciptalaksana.espktkedirikota.networks.models.ResponseLogin;
import com.baraciptalaksana.espktkedirikota.utils.Helpers;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Person {
    // Person
    @SerializedName("nik") @Expose private String nik;
    @SerializedName("name") @Expose private String name;
    @SerializedName("birthplace") @Expose private String birthplace;
    @SerializedName("birthdate") @Expose private String birthdate;
    @SerializedName("gender") @Expose private String gender;
    @SerializedName("job") @Expose private String job;
    @SerializedName("religion") @Expose private String religion;
    @SerializedName("address") @Expose private String address;
    @SerializedName("phone") @Expose private String phone;
    @SerializedName("email") @Expose private String email;

    public static Person mappingPerson(ResponseLogin.Data data) {
        if (data == null) return null;

        Person person = new Person();

        person.setNik(data.getNoKtp());
        person.setAddress(data.getAlamat());
        person.setEmail(data.getEmail());
        person.setJob(data.getPekerjaan());
        person.setName(data.getNama());
        person.setPhone(data.getTlp());
        person.setGender(data.getJenisKelamin());
        if (data.getTglLahir() != null) person.setBirthdate(Helpers.fixServerDate(data.getTglLahir()));
        person.setBirthplace(data.getTempatLahir());
        person.setReligion(data.getAgama());

        return person;
    }

    public ResponseLogin.Data toData() {
        ResponseLogin.Data data = new ResponseLogin.Data();

        data.setNoKtp(getNik());
        data.setAlamat(getAddress());
        data.setEmail(getEmail());
        data.setPekerjaan(getJob());
        data.setNama(getName());
        data.setTlp(getPhone());
        data.setJenisKelamin(getGender());
        data.setTempatLahir(getBirthplace());
        data.setTglLahir(getBirthdate());
        data.setAgama(getReligion());

        return data;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Person) {
            Person p = (Person) obj;
            return nik.equals(p.nik)
                    && address.equals(p.address)
                    && email.equals(p.email)
                    && job.equals(p.job)
                    && name.equals(p.name)
                    && phone.equals(p.phone)
                    && gender.equals(p.gender)
                    && birthplace.equals(p.birthplace)
                    && birthdate.equals(p.birthdate)
                    && religion.equals(p.religion);
        } else return false;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
