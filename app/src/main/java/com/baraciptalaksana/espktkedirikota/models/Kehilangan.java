package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kehilangan extends FormLaporan {

    // Event
    @SerializedName("missingStuff") @Expose private String missingStuff;

    public String getMissingStuff() {
        return missingStuff;
    }

    public void setMissingStuff(String missingStuff) {
        this.missingStuff = missingStuff;
    }
}
