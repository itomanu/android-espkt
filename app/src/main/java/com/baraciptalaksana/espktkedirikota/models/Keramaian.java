package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Keramaian extends FormLaporan {

    // Event
    @SerializedName("keramaianName") @Expose private String keramaianName;
    @SerializedName("keramaianPhone") @Expose private String keramaianPhone;
    @SerializedName("keramaianDate") @Expose private String keramaianDate;
    @SerializedName("keramaianTime") @Expose private String keramaianTime;
    @SerializedName("keramaianDuration") @Expose private String keramaianDuration;
    @SerializedName("keramaianType") @Expose private String keramaianType;
    @SerializedName("keramaianCapacities") @Expose private String keramaianCapacities;
    @SerializedName("keramaianDesc") @Expose private String keramaianDesc;

    public String getKeramaianName() {
        return keramaianName;
    }

    public void setKeramaianName(String keramaianName) {
        this.keramaianName = keramaianName;
    }

    public String getKeramaianPhone() {
        return keramaianPhone;
    }

    public void setKeramaianPhone(String keramaianPhone) {
        this.keramaianPhone = keramaianPhone;
    }

    public String getKeramaianDate() {
        return keramaianDate;
    }

    public void setKeramaianDate(String keramaianDate) {
        this.keramaianDate = keramaianDate;
    }

    public String getKeramaianTime() {
        return keramaianTime;
    }

    public void setKeramaianTime(String keramaianTime) {
        this.keramaianTime = keramaianTime;
    }

    public String getKeramaianDuration() {
        return keramaianDuration;
    }

    public void setKeramaianDuration(String keramaianDuration) {
        this.keramaianDuration = keramaianDuration;
    }

    public String getKeramaianType() {
        return keramaianType;
    }

    public void setKeramaianType(String keramaianType) {
        this.keramaianType = keramaianType;
    }

    public String getKeramaianCapacities() {
        return keramaianCapacities;
    }

    public void setKeramaianCapacities(String keramaianCapacities) {
        this.keramaianCapacities = keramaianCapacities;
    }

    public String getKeramaianDesc() {
        return keramaianDesc;
    }

    public void setKeramaianDesc(String keramaianDesc) {
        this.keramaianDesc = keramaianDesc;
    }
}
