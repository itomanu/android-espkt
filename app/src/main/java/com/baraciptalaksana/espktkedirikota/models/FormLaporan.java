package com.baraciptalaksana.espktkedirikota.models;

import com.baraciptalaksana.espktkedirikota.models.intents.Person;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FormLaporan {
    // Person
    @SerializedName("nik") @Expose protected String nik;
    @SerializedName("name") @Expose protected String name;
    @SerializedName("birthplace") @Expose protected String birthplace;
    @SerializedName("birthdate") @Expose protected String birthdate;
    @SerializedName("gender") @Expose protected String gender;
    @SerializedName("job") @Expose protected String job;
    @SerializedName("religion") @Expose protected String religion;
    @SerializedName("address") @Expose protected String address;
    @SerializedName("phone") @Expose protected String phone;
    @SerializedName("email") @Expose protected String email;

    // Verification
    @SerializedName("verificationDate") @Expose private String verificationDate;
    @SerializedName("verificationTime") @Expose private String verificationTime;

    public static FormLaporan mappingPerson(FormLaporan output, Person data) {
        if (data == null) return output;

        output.setNik(data.getNik());
        output.setName(data.getName());
        output.setBirthplace(data.getBirthplace());
        output.setBirthdate(data.getBirthdate());
        output.setGender(data.getGender());
        output.setJob(data.getJob());
        output.setReligion(data.getReligion());
        output.setAddress(data.getAddress());
        output.setPhone(data.getPhone());
        output.setEmail(data.getEmail());

        return output;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public String getGenderFix() {
        return ""+gender.charAt(0);
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(String verificationDate) {
        this.verificationDate = verificationDate;
    }

    public String getVerificationTimeFix() {
        if (verificationTime.contains("Pagi")) {
            return "pagi";
        } else if (verificationTime.contains("Siang")) {
            return "siang";
        } else {
            return "";
        }
    }

    public String getVerificationTime() {
        return verificationTime;
    }

    public void setVerificationTime(String verificationTime) {
        this.verificationTime = verificationTime;
    }
}
