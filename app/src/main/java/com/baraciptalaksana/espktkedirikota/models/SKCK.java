package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SKCK extends FormLaporan {

    // Event
    @SerializedName("skckMarital") @Expose private String skckMarital;
    @SerializedName("skckPassport") @Expose private String skckPassport;
    @SerializedName("skckKitas") @Expose private String skckKitas;

    @SerializedName("skckHWName") @Expose private String skckHWName;
    @SerializedName("skckHWAge") @Expose private String skckHWAge;
    @SerializedName("skckHWReligion") @Expose private String skckHWReligion;
    @SerializedName("skckHWNation") @Expose private String skckHWNation;
    @SerializedName("skckHWJob") @Expose private String skckHWJob;
    @SerializedName("skckHWAddress") @Expose private String skckHWAddress;

    @SerializedName("skckFatherName") @Expose private String skckFatherName;
    @SerializedName("skckFatherAge") @Expose private String skckFatherAge;
    @SerializedName("skckFatherReligion") @Expose private String skckFatherReligion;
    @SerializedName("skckFatherNation") @Expose private String skckFatherNation;
    @SerializedName("skckFatherJob") @Expose private String skckFatherJob;
    @SerializedName("skckFatherAddress") @Expose private String skckFatherAddress;

    @SerializedName("skckMotherName") @Expose private String skckMotherName;
    @SerializedName("skckMotherAge") @Expose private String skckMotherAge;
    @SerializedName("skckMotherReligion") @Expose private String skckMotherReligion;
    @SerializedName("skckMotherNation") @Expose private String skckMotherNation;
    @SerializedName("skckMotherJob") @Expose private String skckMotherJob;
    @SerializedName("skckMotherAddress") @Expose private String skckMotherAddress;

    @SerializedName("skckSiblingName") @Expose private String skckSiblingName;
    @SerializedName("skckSiblingAge") @Expose private String skckSiblingAge;
    @SerializedName("skckSiblingJob") @Expose private String skckSiblingJob;
    @SerializedName("skckSiblingAddress") @Expose private String skckSiblingAddress;

    @SerializedName("skckPidana") @Expose private String skckPidana;
    @SerializedName("skckPidanaDesc") @Expose private String skckPidanaDesc;
    @SerializedName("skckPidanaPutusan") @Expose private String skckPidanaPutusan;
    @SerializedName("skckPidanaDescOn") @Expose private String skckPidanaDescOn;
    @SerializedName("skckPidanaProccess") @Expose private String skckPidanaProccess;

    @SerializedName("skckPelanggaran") @Expose private String skckPelanggaran;
    @SerializedName("skckPelanggaranDesc") @Expose private String skckPelanggaranDesc;
    @SerializedName("skckPelanggaranProccess") @Expose private String skckPelanggaranProccess;

    @SerializedName("skckOthersJob") @Expose private String skckOthersJob;
    @SerializedName("skckOthersHobby") @Expose private String skckOthersHobby;
    @SerializedName("skckOthersPhone") @Expose private String skckOthersPhone;

    @SerializedName("skckSponsorName") @Expose private String skckSponsorName;
    @SerializedName("skckSponsorAddress") @Expose private String skckSponsorAddress;
    @SerializedName("skckSponsorType") @Expose private String skckSponsorType;
    @SerializedName("skckSponsorPhone") @Expose private String skckSponsorPhone;

    public String getSkckMarital() {
        return skckMarital;
    }

    public void setSkckMarital(String skckMarital) {
        this.skckMarital = skckMarital;
    }

    public String getSkckPassport() {
        return skckPassport;
    }

    public void setSkckPassport(String skckPassport) {
        this.skckPassport = skckPassport;
    }

    public String getSkckKitas() {
        return skckKitas;
    }

    public void setSkckKitas(String skckKitas) {
        this.skckKitas = skckKitas;
    }

    public String getSkckHWName() {
        return skckHWName;
    }

    public void setSkckHWName(String skckHWName) {
        this.skckHWName = skckHWName;
    }

    public String getSkckHWAge() {
        return skckHWAge;
    }

    public void setSkckHWAge(String skckHWAge) {
        this.skckHWAge = skckHWAge;
    }

    public String getSkckHWReligion() {
        return skckHWReligion;
    }

    public void setSkckHWReligion(String skckHWReligion) {
        this.skckHWReligion = skckHWReligion;
    }

    public String getSkckHWNation() {
        return skckHWNation;
    }

    public void setSkckHWNation(String skckHWNation) {
        this.skckHWNation = skckHWNation;
    }

    public String getSkckHWJob() {
        return skckHWJob;
    }

    public void setSkckHWJob(String skckHWJob) {
        this.skckHWJob = skckHWJob;
    }

    public String getSkckHWAddress() {
        return skckHWAddress;
    }

    public void setSkckHWAddress(String skckHWAddress) {
        this.skckHWAddress = skckHWAddress;
    }

    public String getSkckFatherName() {
        return skckFatherName;
    }

    public void setSkckFatherName(String skckFatherName) {
        this.skckFatherName = skckFatherName;
    }

    public String getSkckFatherAge() {
        return skckFatherAge;
    }

    public void setSkckFatherAge(String skckFatherAge) {
        this.skckFatherAge = skckFatherAge;
    }

    public String getSkckFatherReligion() {
        return skckFatherReligion;
    }

    public void setSkckFatherReligion(String skckFatherReligion) {
        this.skckFatherReligion = skckFatherReligion;
    }

    public String getSkckFatherNation() {
        return skckFatherNation;
    }

    public void setSkckFatherNation(String skckFatherNation) {
        this.skckFatherNation = skckFatherNation;
    }

    public String getSkckFatherJob() {
        return skckFatherJob;
    }

    public void setSkckFatherJob(String skckFatherJob) {
        this.skckFatherJob = skckFatherJob;
    }

    public String getSkckFatherAddress() {
        return skckFatherAddress;
    }

    public void setSkckFatherAddress(String skckFatherAddress) {
        this.skckFatherAddress = skckFatherAddress;
    }

    public String getSkckMotherName() {
        return skckMotherName;
    }

    public void setSkckMotherName(String skckMotherName) {
        this.skckMotherName = skckMotherName;
    }

    public String getSkckMotherAge() {
        return skckMotherAge;
    }

    public void setSkckMotherAge(String skckMotherAge) {
        this.skckMotherAge = skckMotherAge;
    }

    public String getSkckMotherReligion() {
        return skckMotherReligion;
    }

    public void setSkckMotherReligion(String skckMotherReligion) {
        this.skckMotherReligion = skckMotherReligion;
    }

    public String getSkckMotherNation() {
        return skckMotherNation;
    }

    public void setSkckMotherNation(String skckMotherNation) {
        this.skckMotherNation = skckMotherNation;
    }

    public String getSkckMotherJob() {
        return skckMotherJob;
    }

    public void setSkckMotherJob(String skckMotherJob) {
        this.skckMotherJob = skckMotherJob;
    }

    public String getSkckMotherAddress() {
        return skckMotherAddress;
    }

    public void setSkckMotherAddress(String skckMotherAddress) {
        this.skckMotherAddress = skckMotherAddress;
    }

    public String getSkckSiblingName() {
        return skckSiblingName;
    }

    public void setSkckSiblingName(String skckSiblingName) {
        this.skckSiblingName = skckSiblingName;
    }

    public String getSkckSiblingAge() {
        return skckSiblingAge;
    }

    public void setSkckSiblingAge(String skckSiblingAge) {
        this.skckSiblingAge = skckSiblingAge;
    }

    public String getSkckSiblingJob() {
        return skckSiblingJob;
    }

    public void setSkckSiblingJob(String skckSiblingJob) {
        this.skckSiblingJob = skckSiblingJob;
    }

    public String getSkckSiblingAddress() {
        return skckSiblingAddress;
    }

    public void setSkckSiblingAddress(String skckSiblingAddress) {
        this.skckSiblingAddress = skckSiblingAddress;
    }

    public String getSkckPidana() {
        return skckPidana;
    }

    public void setSkckPidana(String skckPidana) {
        this.skckPidana = skckPidana;
    }

    public String getSkckPidanaDesc() {
        return skckPidanaDesc;
    }

    public void setSkckPidanaDesc(String skckPidanaDesc) {
        this.skckPidanaDesc = skckPidanaDesc;
    }

    public String getSkckPidanaPutusan() {
        return skckPidanaPutusan;
    }

    public void setSkckPidanaPutusan(String skckPidanaPutusan) {
        this.skckPidanaPutusan = skckPidanaPutusan;
    }

    public String getSkckPidanaDescOn() {
        return skckPidanaDescOn;
    }

    public void setSkckPidanaDescOn(String skckPidanaDescOn) {
        this.skckPidanaDescOn = skckPidanaDescOn;
    }

    public String getSkckPidanaProccess() {
        return skckPidanaProccess;
    }

    public void setSkckPidanaProccess(String skckPidanaProccess) {
        this.skckPidanaProccess = skckPidanaProccess;
    }

    public String getSkckPelanggaran() {
        return skckPelanggaran;
    }

    public void setSkckPelanggaran(String skckPelanggaran) {
        this.skckPelanggaran = skckPelanggaran;
    }

    public String getSkckPelanggaranDesc() {
        return skckPelanggaranDesc;
    }

    public void setSkckPelanggaranDesc(String skckPelanggaranDesc) {
        this.skckPelanggaranDesc = skckPelanggaranDesc;
    }

    public String getSkckPelanggaranProccess() {
        return skckPelanggaranProccess;
    }

    public void setSkckPelanggaranProccess(String skckPelanggaranProccess) {
        this.skckPelanggaranProccess = skckPelanggaranProccess;
    }

    public String getSkckOthersJob() {
        return skckOthersJob;
    }

    public void setSkckOthersJob(String skckOthersJob) {
        this.skckOthersJob = skckOthersJob;
    }

    public String getSkckOthersHobby() {
        return skckOthersHobby;
    }

    public void setSkckOthersHobby(String skckOthersHobby) {
        this.skckOthersHobby = skckOthersHobby;
    }

    public String getSkckOthersPhone() {
        return skckOthersPhone;
    }

    public void setSkckOthersPhone(String skckOthersPhone) {
        this.skckOthersPhone = skckOthersPhone;
    }

    public String getSkckSponsorName() {
        return skckSponsorName;
    }

    public void setSkckSponsorName(String skckSponsorName) {
        this.skckSponsorName = skckSponsorName;
    }

    public String getSkckSponsorAddress() {
        return skckSponsorAddress;
    }

    public void setSkckSponsorAddress(String skckSponsorAddress) {
        this.skckSponsorAddress = skckSponsorAddress;
    }

    public String getSkckSponsorType() {
        return skckSponsorType;
    }

    public void setSkckSponsorType(String skckSponsorType) {
        this.skckSponsorType = skckSponsorType;
    }

    public String getSkckSponsorPhone() {
        return skckSponsorPhone;
    }

    public void setSkckSponsorPhone(String skckSponsorPhone) {
        this.skckSponsorPhone = skckSponsorPhone;
    }
}
