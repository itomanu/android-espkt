package com.baraciptalaksana.espktkedirikota.models.intents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Verification {
    // Verification
    @SerializedName("verificationDate") @Expose private String verificationDate;
    @SerializedName("verificationTime") @Expose private String verificationTime;

    public String getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(String verificationDate) {
        this.verificationDate = verificationDate;
    }

    public String getVerificationTime() {
        return verificationTime;
    }

    public void setVerificationTime(String verificationTime) {
        this.verificationTime = verificationTime;
    }
}
