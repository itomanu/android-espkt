package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lalin extends FormLaporan{

    // Event
    @SerializedName("lalinName") @Expose private String lalinName;
    @SerializedName("lalinPhone") @Expose private String lalinPhone;
    @SerializedName("lalinDate") @Expose private String lalinDate;
    @SerializedName("lalinTime") @Expose private String lalinTime;
    @SerializedName("lalinDuration") @Expose private String lalinDuration;
    @SerializedName("lalinType") @Expose private String lalinType;
    @SerializedName("lalinCapacities") @Expose private String lalinCapacities;
    @SerializedName("lalinLocationDesc") @Expose private String lalinLocationDesc;
    @SerializedName("lalinAlternativeDesc") @Expose private String lalinAlternativeDesc;

    @SerializedName("lalinLocationMarker") @Expose private String lalinLocationMarker;
    @SerializedName("lalinLocationPath") @Expose private String lalinLocationPath;
    @SerializedName("lalinAlternativePath") @Expose private String lalinAlternativePath;

    public String getLalinName() {
        return lalinName;
    }

    public void setLalinName(String lalinName) {
        this.lalinName = lalinName;
    }

    public String getLalinPhone() {
        return lalinPhone;
    }

    public void setLalinPhone(String lalinPhone) {
        this.lalinPhone = lalinPhone;
    }

    public String getLalinDate() {
        return lalinDate;
    }

    public void setLalinDate(String lalinDate) {
        this.lalinDate = lalinDate;
    }

    public String getLalinTime() {
        return lalinTime;
    }

    public void setLalinTime(String lalinTime) {
        this.lalinTime = lalinTime;
    }

    public String getLalinDuration() {
        return lalinDuration;
    }

    public void setLalinDuration(String lalinDuration) {
        this.lalinDuration = lalinDuration;
    }

    public String getLalinType() {
        return lalinType;
    }

    public void setLalinType(String lalinType) {
        this.lalinType = lalinType;
    }

    public String getLalinCapacities() {
        return lalinCapacities;
    }

    public void setLalinCapacities(String lalinCapacities) {
        this.lalinCapacities = lalinCapacities;
    }

    public String getLalinLocationDesc() {
        return lalinLocationDesc;
    }

    public void setLalinLocationDesc(String lalinLocationDesc) {
        this.lalinLocationDesc = lalinLocationDesc;
    }

    public String getLalinAlternativeDesc() {
        return lalinAlternativeDesc;
    }

    public void setLalinAlternativeDesc(String lalinAlternativeDesc) {
        this.lalinAlternativeDesc = lalinAlternativeDesc;
    }

    public String getLalinLocationPath() {
        return lalinLocationPath;
    }

    public void setLalinLocationPath(String lalinLocationPath) {
        this.lalinLocationPath = lalinLocationPath;
    }

    public String getLalinAlternativePath() {
        return lalinAlternativePath;
    }

    public void setLalinAlternativePath(String lalinAlternativePath) {
        this.lalinAlternativePath = lalinAlternativePath;
    }
}
