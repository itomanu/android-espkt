package com.baraciptalaksana.espktkedirikota.models;

import android.content.Context;

import com.baraciptalaksana.espktkedirikota.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("viewer")
    @Expose
    private Integer viewer;

    public static News createMore(Context context) {
        News inbox = new News();
        inbox.setJudul(context.getString(R.string.text_see_more));
        return inbox;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getViewer() {
        return viewer;
    }

    public void setViewer(Integer viewer) {
        this.viewer = viewer;
    }
}
