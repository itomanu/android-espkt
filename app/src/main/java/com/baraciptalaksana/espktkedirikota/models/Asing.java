package com.baraciptalaksana.espktkedirikota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Asing extends FormLaporan {

    // Event
    @SerializedName("asingName") @Expose private String asingName;
    @SerializedName("asingBirthplace") @Expose private String asingBirthplace;
    @SerializedName("asingBirthdate") @Expose private String asingBirthdate;
    @SerializedName("asingNation") @Expose private String asingNation;
    @SerializedName("asingJob") @Expose private String asingJob;
    @SerializedName("asingAddress") @Expose private String asingAddress;
    @SerializedName("asingPassport") @Expose private String asingPassport;
    @SerializedName("asingPassportBy") @Expose private String asingPassportBy;
    @SerializedName("asingVisa") @Expose private String asingVisa;
    @SerializedName("asingArrivalDate") @Expose private String asingArrivalDate;
    @SerializedName("asingFrom") @Expose private String asingFrom;
    @SerializedName("asingPurpose") @Expose private String asingPurpose;
    @SerializedName("asingDepartDate") @Expose private String asingDepartDate;
    @SerializedName("asingNext") @Expose private String asingNext;
    @SerializedName("asingCountry") @Expose private String asingCountry;
    @SerializedName("asingCity") @Expose private String asingCity;

    public String getAsingName() {
        return asingName;
    }

    public void setAsingName(String asingName) {
        this.asingName = asingName;
    }

    public String getAsingBirthplace() {
        return asingBirthplace;
    }

    public void setAsingBirthplace(String asingBirthplace) {
        this.asingBirthplace = asingBirthplace;
    }

    public String getAsingBirthdate() {
        return asingBirthdate;
    }

    public void setAsingBirthdate(String asingBirthdate) {
        this.asingBirthdate = asingBirthdate;
    }

    public String getAsingNation() {
        return asingNation;
    }

    public void setAsingNation(String asingNation) {
        this.asingNation = asingNation;
    }

    public String getAsingJob() {
        return asingJob;
    }

    public void setAsingJob(String asingJob) {
        this.asingJob = asingJob;
    }

    public String getAsingAddress() {
        return asingAddress;
    }

    public void setAsingAddress(String asingAddress) {
        this.asingAddress = asingAddress;
    }

    public String getAsingPassport() {
        return asingPassport;
    }

    public void setAsingPassport(String asingPassport) {
        this.asingPassport = asingPassport;
    }

    public String getAsingPassportBy() {
        return asingPassportBy;
    }

    public void setAsingPassportBy(String asingPassportBy) {
        this.asingPassportBy = asingPassportBy;
    }

    public String getAsingVisa() {
        return asingVisa;
    }

    public void setAsingVisa(String asingVisa) {
        this.asingVisa = asingVisa;
    }

    public String getAsingArrivalDate() {
        return asingArrivalDate;
    }

    public void setAsingArrivalDate(String asingArrivalDate) {
        this.asingArrivalDate = asingArrivalDate;
    }

    public String getAsingFrom() {
        return asingFrom;
    }

    public void setAsingFrom(String asingFrom) {
        this.asingFrom = asingFrom;
    }

    public String getAsingPurpose() {
        return asingPurpose;
    }

    public void setAsingPurpose(String asingPurpose) {
        this.asingPurpose = asingPurpose;
    }

    public String getAsingDepartDate() {
        return asingDepartDate;
    }

    public void setAsingDepartDate(String asingDepartDate) {
        this.asingDepartDate = asingDepartDate;
    }

    public String getAsingNext() {
        return asingNext;
    }

    public void setAsingNext(String asingNext) {
        this.asingNext = asingNext;
    }

    public String getAsingCountry() {
        return asingCountry;
    }

    public void setAsingCountry(String asingCountry) {
        this.asingCountry = asingCountry;
    }

    public String getAsingCity() {
        return asingCity;
    }

    public void setAsingCity(String asingCity) {
        this.asingCity = asingCity;
    }
}
